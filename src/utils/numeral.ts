import BigNumber from 'bignumber.js'
import numeral from 'numeral'

class Numeral {
    // 科学计数
    static localString(v: any = 0){
        const bn = new BigNumber(v)
        return bn.toNumber() >= 0.000001 ? numeral(bn.toNumber()).format('$0.[00000000]') : `${numeral.localeData().currency.symbol}${bn.toFixed(8)}`
    }
    // 百分比
    static percentage(v: any = 0){
        const bn = new BigNumber(v)
        return `${numeral(bn.toNumber()).format('0.00')}%`
    }
    // 市值计算
    static value(v: any = 0){
        const bn = new BigNumber(v)
        return bn.toNumber() >= 0.000001 ? numeral(bn.toNumber()).format('($0.00a)') : `${numeral.localeData().currency.symbol}${bn.toFixed(8)}`
    }
    // 小数点
    static decimal(v: any = 0, decimal = 8){
        const bn = new BigNumber(v)
        return bn.toNumber() >= 0.000001 ? numeral(bn.toNumber()).format('0.[00000000]') : `${bn.toFixed(8)}`
    }
    // total
    static total(v: any = 0){
        const bn = new BigNumber(v)
        return bn.toNumber() >= 0.000001 ? numeral(bn.toNumber()).format('$0,0.00[000000]') : `${numeral.localeData().currency.symbol}${bn.toFixed(8)}`
    }
}

export default Numeral