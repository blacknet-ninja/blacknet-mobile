import Blacknetjs from 'blacknetjs';


const decrypt = (mnemonic: string, address: string, message:string)=>{
    try {
        return Blacknetjs.Decrypt(mnemonic, address, message);
    } catch (error) {
        return null   
    }
}


export default {
    decrypt
}