import md5 from 'md5';
import Blacknetjs from 'blacknetjs';
import { blnnodeAPI } from '@app/config'
import { IBlnScanMessage } from '@app/types/bln';
import MSG from './msg';
import { accountStore } from '@app/stores/account';
import thread from '@app/services/thread';
const devFundAddress = 'blacknet1d3wnqk7h0fvq5j8mtvs4zr2crecavlsx6fyfma87lncgnah8vnesge8m3e'
const blacknetjs = new Blacknetjs()
blacknetjs.jsonrpc.endpoint = blnnodeAPI;

const newMessageText = (to: string, isGroup: boolean, isPrivate: boolean, text: string, quote?: any)=>{
    const msg = new BLNChatData(to)
    msg.setIsGroup(isGroup)
    if(isGroup){
        msg.setPrefix(ChatDataPrefix.Group)
    }
    msg.setIsPrivate(isPrivate)
    msg.setType(ChatDataType.Text)
    let data: {[key: string]: any} = {
        content: text
    }
    if(quote){
        data.quote = quote
    }
    msg.setData(data)
    return msg
}

const newMessageImage = (to: string, isGroup: boolean, isPrivate: boolean, text: string, quote?: any)=>{
    const msg = new BLNChatData(to)
    msg.setIsGroup(isGroup)
    if(isGroup){
        msg.setPrefix(ChatDataPrefix.Group)
    }
    msg.setIsPrivate(isPrivate)
    msg.setType(ChatDataType.Image)
    let data: {[key: string]: any} = {
        uri: text
    }
    if(quote){
        data.quote = quote
    }
    msg.setData(data)
    return msg
}

const newMessageVideo = (to: string, isGroup: boolean, isPrivate: boolean, text: string, quote?: any)=>{
    const msg = new BLNChatData(to)
    msg.setIsGroup(isGroup)
    if(isGroup){
        msg.setPrefix(ChatDataPrefix.Group)
    }
    msg.setIsPrivate(isPrivate)
    msg.setType(ChatDataType.Video)
    let data: {[key: string]: any} = {
        uri: text
    }
    if(quote){
        data.quote = quote
    }
    msg.setData(data)
    return msg
}

const newMessageAudio = (to: string, isGroup: boolean, isPrivate: boolean, text: string, quote?: any)=>{
    const msg = new BLNChatData(to)
    msg.setIsGroup(isGroup)
    if(isGroup){
        msg.setPrefix(ChatDataPrefix.Group)
    }
    msg.setIsPrivate(isPrivate)
    msg.setType(ChatDataType.Audio)
    let data: {[key: string]: any} = {
        uri: text
    }
    if(quote){
        data.quote = quote
    }
    msg.setData(data)
    return msg
}

const newMessageSource = (to: string, isGroup: boolean, isPrivate: boolean, text: string, quote?: any)=>{
    const msg = new BLNChatData(to)
    msg.setIsGroup(isGroup)
    if(isGroup){
        msg.setPrefix(ChatDataPrefix.Group)
    }
    msg.setIsPrivate(isPrivate)
    let data: {[key: string]: any} = {
        uri: text
    }
    if(quote){
        data.quote = quote
    }
    msg.setData(data)
    return msg
}

const format = (message: string)=>{
    try {
        return JSON.parse(message) as IBLNChatData
    } catch (error) {
        // console.log("chat.format", error, message)
    }
    return {
        type: ChatDataType.Unknow
    } as IBLNChatData
}

const timeNowStr = ()=>{
    return Math.floor(new Date().getTime() / 1000).toString()
}

export default {
    // pack,
    // unpack,
    timeNowStr,
    format,
    newMessageSource,
    newMessageText,
    newMessageImage,
    newMessageVideo,
    newMessageAudio,
    address: Blacknetjs.Address
}

export enum ChatDataType {
    Image = "image",
    Video = "video",
    Text = "text",
    Audio = "audio",
    Unknow = "unknow",
    Empty = "empty"
}

export enum ChatFee {
    Chat = 0.005,
    Group = 0.01
}

export enum ChatDataPrefix {
    Chat = "chat",
    Group = "groupMessage"
}

export interface IBLNChatData {
    id: string, //消息id
    to: string, //to地址
    prefix: ChatDataPrefix,
    version: number,
    isGroup: number,
    isPrivate: number,
    type: ChatDataType,
    time?: string,
    data: IBLNChatDataText | IBLNChatDataImage | IBLNChatDataAudio | IBLNChatDataVideo | IBLNChatDataAny,
    [key: string]: any
}

export class BLNChatData {
    id?: string //消息id
    to?: string //发送地址/txid
    version: number = 1
    prefix: ChatDataPrefix = ChatDataPrefix.Chat
    type: ChatDataType = ChatDataType.Text
    isGroup: boolean = false
    isPrivate: boolean = false
    data: any
    time: string

    static formart(data: IBLNChatData){
        const message = new BLNChatData(data.to, data.prefix, data.version)
        message.setID(data.id)
        message.setPrefix(data.prefix)
        message.setType(data.type)
        message.setIsGroup(!!data.isGroup)
        message.setIsPrivate(!!data.isPrivate)
        message.setVersion(data.version)
        message.setTo(data.to)
        message.setTime(data.time)
        message.setData(data.data)
        return message
    }

    static unpack(data: IBlnScanMessage, currentMnemonic?: string, currentAddress?: string){
        let msg: IBLNChatData = {
            type: ChatDataType.Unknow
        } as IBLNChatData
        if(!data.text){
            msg = {
                type: ChatDataType.Empty
            } as IBLNChatData
        } else {
            const reg = data.text.match(/(\w+):\s?([\s\S]*)/i)
            if(reg){
                const prefix = reg[1]
                try {
                    const data = JSON.parse(reg[2])
                    if(data.v && data.t){
                        msg = {
                            prefix: prefix,
                            to: data.id,
                            version: data.v,
                            type: data.t,
                            isGroup: data.g,
                            isPrivate: data.p,
                            data: data.d
                        } as IBLNChatData
                    }
                } catch (error) {
                    // console.log("chat.unpack", error, message)
                    // old version
                    if(data){
                        // let text
                        msg.to = data.to
                        msg.version = 0
                        if(data.group){
                            msg.isGroup = 1
                            msg.isPrivate = 0
                            msg.prefix = ChatDataPrefix.Group
                            // text = MSG.decodeGroupChatContent(data.text)
                        } else {
                            msg.prefix = ChatDataPrefix.Chat
                            msg.isPrivate = 1
                            // try {
                            //     if(data.from === currentAddress){
                            //         text = MSG.decodeChatContent(currentMnemonic || "", data.to, data.text)
                            //     } else {
                            //         text = MSG.decodeChatContent(currentMnemonic || "", data.from, data.text)
                            //     }
                            // } catch (error) {
                            // }
                        }
                        msg.data = {
                            text: data.text,
                            from: data.from
                        }
                        // if(text){
                        //     const obj = MSG.decodeContent(text)
                        //     if(MSG.isImage(text)) {
                        //         msg.type = ChatDataType.Image
                        //         msg.data = {
                        //             uri: MSG.getCID(text)
                        //         }
                        //     }else if(MSG.isVideo(text)) {
                        //         msg.type = ChatDataType.Video
                        //         msg.data = {
                        //             uri: MSG.getCID(text)
                        //         }
                        //     }else if(MSG.isAudio(text)) {
                        //         msg.type = ChatDataType.Audio
                        //         msg.data = {
                        //             uri: MSG.getCID(text)
                        //         }
                        //     } else {
                        //         msg.type = ChatDataType.Text
                        //         msg.data = {
                        //             content: text
                        //         }
                        //     }
                        //     if(obj['txid']){
                        //         msg.data.quote = obj['txid']
                        //     }
                        // }
                    }
                }
            }
        }
        return BLNChatData.formart(msg)
    }

    constructor(to?: string, prefix?: ChatDataPrefix, version?: number){
        if(to){
            this.to = to
        }
        if(prefix){
            this.prefix = prefix
        }
        if(version){
            this.version = version
        }
        this.time = Math.floor(new Date().getTime() / 1000).toString()
    }
    formart(){
        return {
            id: this.getID() || null,
            to: this.to || null,
            version: this.version || 0,
            type: this.type || null,
            isGroup: this.isGroup ? 1 : 0,
            isPrivate: this.isPrivate ? 1 : 0,
            prefix: this.prefix || null,
            time: this.time || null,
            data: this.data || null
        } as IBLNChatData
    }
    pack(){
        return `${this.prefix}: ${JSON.stringify({
            v: this.version,
            t: this.type,
            g: this.isGroup ? 1 : 0,
            p: this.isPrivate ? 1 : 0,
            id: this.isGroup ? this.to : undefined,
            d: this.data
        })}`
    }
    encrypt(sk?: string, pk?: string){
        let text = JSON.stringify(this.getData())
        if(this.isPrivate && sk && pk){
            text = Blacknetjs.Encrypt(sk, pk, text)
        } else {
            text = Buffer.from(text, 'utf8').toString('base64')
        }
        this.setData(text)
    }
    decrypt(sk?: string, pk?: string, currentMnemonic?: string, currentAddress?: string){
        let text = this.getData()
        try {
            if(this.isPrivate){
                text = Blacknetjs.Decrypt(sk, pk, text)
                // thread.decrypt(sk, pk, text).then((t)=>{
                //     console.log("t", t)
                // })
            } else {
                text = Buffer.from(text, 'base64').toString('utf-8')
            }
            text = JSON.parse(text)
            this.setData(text)
        } catch (error) {
            // console.log("chat.decrypt", error)
            if(text){
                let data
                if(this.isGroup){
                    data = MSG.decodeGroupChatContent(text.text)
                } else {
                    try {
                        if(text.from === currentAddress){
                            data = MSG.decodeChatContent(currentMnemonic || "", this.getTo(), text.text)
                        } else {
                            data = MSG.decodeChatContent(currentMnemonic || "", text.from, text.text)
                        }
                    } catch (error) {
                        data = text.text
                    }
                }
                if(data){
                    if(MSG.isImage(data)) {
                        this.setType(ChatDataType.Image)
                        this.setData({
                            uri: MSG.getCID(data)
                        })
                    }else if(MSG.isVideo(data)) {
                        this.setType(ChatDataType.Video)
                        this.setData({
                            uri: MSG.getCID(data)
                        })
                    }else if(MSG.isAudio(data)) {
                        this.setType(ChatDataType.Audio)
                        this.setData({
                            uri: MSG.getCID(data)
                        })
                    } else {
                        this.setType(ChatDataType.Text)
                        this.setData({
                            content: data
                        })
                    }
                    const obj = MSG.decodeContent(data)
                    if(obj['txid']){
                        this.updateData({quote: obj['txid']})
                    }
                }
            }
        }
    }
    decryptAsync(sk?: string, pk?: string, currentMnemonic?: string, currentAddress?: string){
        let text = this.getData()
        let promise: Promise<string>;
        try {
            if(this.isPrivate){
                promise = thread.decrypt(sk, pk, text)
            } else {
                promise = Promise.resolve(Buffer.from(text, 'base64').toString('utf-8'))
            }
            return promise.then((res: string)=>{
                text = JSON.parse(res)
                this.setData(text)
                return this
            })
        } catch (error) {
            if(text){
                if(this.isGroup){
                    promise = MSG.decodeGroupChatContentAsync(text.text)
                } else {
                    try {
                        if(text.from === currentAddress){
                            promise = MSG.decodeChatContentAsync(currentMnemonic || "", this.getTo(), text.text)
                        } else {
                            promise = MSG.decodeChatContentAsync(currentMnemonic || "", text.from, text.text)
                        }
                    } catch (error) {
                        promise = Promise.resolve(text.text)
                    }
                }
                return promise.then((data: string)=>{
                    if(data){
                        if(MSG.isImage(data)) {
                            this.setType(ChatDataType.Image)
                            this.setData({
                                uri: MSG.getCID(data)
                            })
                        }else if(MSG.isVideo(data)) {
                            this.setType(ChatDataType.Video)
                            this.setData({
                                uri: MSG.getCID(data)
                            })
                        }else if(MSG.isAudio(data)) {
                            this.setType(ChatDataType.Audio)
                            this.setData({
                                uri: MSG.getCID(data)
                            })
                        } else {
                            this.setType(ChatDataType.Text)
                            this.setData({
                                content: data
                            })
                        }
                        const obj = MSG.decodeContent(data)
                        if(obj['txid']){
                            this.updateData({quote: obj['txid']})
                        }
                    }
                    return this
                })
            }
            return Promise.resolve(this)
        }
    }
    getEncryptData(){
        return this.data
    }
    getDecryptData(){
        return this.data
    }
    getText(){
        return JSON.stringify(this.formart())
    }
    getData(){
        return this.data
    }
    getID(){
        if(!this.id){
            let text
            switch (this.type) {
                case ChatDataType.Text:
                    text = this.data.content || this.getData()
                    break;
                case ChatDataType.Image:
                    text = this.data.uri || this.getData()
                    break;
                case ChatDataType.Audio:
                    text = this.data.uri || this.getData()
                    break;
                case ChatDataType.Video:
                    text = this.data.uri || this.getData()
                    break;
                // case ChatDataType.Unknow:
                //     text = this.getData()
                //     break;
                // case ChatDataType.Empty:
                //     text = this.getData()
                //     break;
                default:
                    text = this.getData()
                    break;
            }
            return `${text===undefined ? text : md5(text)}:${this.isGroup}:${this.to}:${this.isGroup}:${this.time}`
        }
        return this.id
    }
    getSourceUri(){
        let text
        switch (this.type) {
            case ChatDataType.Image:
                text = this.data.uri
                break;
            case ChatDataType.Audio:
                text = this.data.uri
                break;
            case ChatDataType.Video:
                text = this.data.uri
                break;
            default:
                text = ""
                break;
        }
        return `https://ipfs.loqunbai.com/ipfs/${text}`;
    }
    getContent(){
        if(this.isEmpty()){
            return undefined
        }
        return this.data.content
    }
    isImage(){
        return this.type === ChatDataType.Image
    }
    isText(){
        return this.type === ChatDataType.Text
    }
    isVideo(){
        return this.type === ChatDataType.Video
    }
    isAudio(){
        return this.type === ChatDataType.Audio
    }
    isUnknow(){
        return this.type === ChatDataType.Unknow
    }
    isEmpty(){
        return this.type === ChatDataType.Empty
    }
    hasQuote(){
        if(!this.data){
            return false
        }
        return !!this.data.quote
    }
    getQuote(){
        return this.data.quote
    }
    getDate(){
        return new Date(parseInt(this.time)*1000)
    }
    getToAddress(){
        return this.isGroup ? devFundAddress : this.to || ""
    }
    getTo(){
        return this.to || ""
    }
    getFee(){
        return this.isGroup ? ChatFee.Group : ChatFee.Chat
    }
    setID(data: string){
        this.id = data
    }
    setTo(data: string){
        this.to = data
    }
    setData(data: any){
        this.data = data
    }
    updateData(data: any){
        this.data = { ...this.data, ...data }
    }
    setPrefix(data: ChatDataPrefix){
        this.prefix = data
    }
    setType(data: ChatDataType){
        this.type = data
    }
    setIsGroup(data: boolean){
        this.isGroup = data
    }
    setIsPrivate(data: boolean){
        this.isPrivate = data
    }
    setVersion(data: number){
        this.version = data
    }
    setTime(data?: string){
        this.time = data || Math.floor(new Date().getTime() / 1000).toString()
    }
}

export class BLNChatDataText extends BLNChatData{
    constructor(){
        super()
        super.setType(ChatDataType.Text)
    }
}
export class BLNChatDataImage extends BLNChatData{
    constructor(){
        super()
        super.setType(ChatDataType.Image)
    }
}
export class BLNChatDataVideo extends BLNChatData{
    constructor(){
        super()
        super.setType(ChatDataType.Video)
    }
}
export class BLNChatDataAudio extends BLNChatData{
    constructor(){
        super()
        super.setType(ChatDataType.Audio)
    }
}

export interface IBLNChatDataText {
    content: string,
    quote?: string
}

export interface IBLNChatDataImage {
    uri: string,
    mime?: string,
    quote?: string
}

export interface IBLNChatDataVideo {
    uri: string,
    mime?: string,
    quote?: string
}

export interface IBLNChatDataAudio {
    uri: string,
    mime?: string,
    quote?: string
}

export interface IBLNChatDataAny {
    text: string
    from: string
}