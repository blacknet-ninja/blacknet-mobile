/**
 * stash store
 * @file stash store
 * @module app/stores/stash
 * @author Pony Ma <https://gitlab.com/blacknet-ninja>
 */

import { observable, computed, action, reaction } from 'mobx'
import { boundMethod } from 'autobind-decorator'
import storage from '@app/services/storage'
import { IBlnTransaction } from '@app/types/bln'
import StoreAPI from '@app/services/storeapi'
import { txIsCancelLease, txIsPost, txIsReplyPost } from '@app/components/common/asset-type';
import { verifyAccount } from '@app/utils/bln';
import api from '@app/services/api';

class StashStore {
    
    @observable.shallow lists: Array<IBlnTransaction> = []
    @observable address: string = ""
    private timer: any
    private display: boolean = true

    @computed get length() {
        return this.lists.length
    }

    setDisplay(hide: boolean): void {
        this.display = hide;
    }

    @computed get isDisplay() {
        return this.display;
    }

    @computed get pendingLength() {
        let len = 0;
        this.lists.forEach((tx)=>{
            if(this.status(tx) === "pending"){
                len++
            }
        })
        return len
    }

    @computed get list() {
        return this.lists.slice()
    }

   
    @computed get postLength() {
        let len = 0;
        this.lists.forEach((tx)=>{
            if(this.status(tx) === "pending"){
                if(txIsPost(tx) || txIsReplyPost(tx)){
                    len++
                }
            }
        })
        return len
    }

    @computed get cancelLeaseLength() {
        let len = 0;
        this.lists.forEach((tx)=>{
            if(this.status(tx) === "pending" && txIsCancelLease(tx)){
                len++
            }
        })
        return len
    }

    @computed get pendingList(): Array<IBlnTransaction>{
        const list: Array<IBlnTransaction> = [];
        this.lists.forEach((tx)=>{
            if(this.status(tx) === "pending"){
                list.push(tx)
            }
        })
        return list
    }

    

    @boundMethod
    leaseIsPending(from: string, to: string, height: number): boolean{
        let pending = false;
        this.lists.forEach((tx)=>{
            const leaseHeight = tx.blockHeight || tx.leaseHeight
            if(this.status(tx) === "pending" && tx.from === from && tx.to === to && txIsCancelLease(tx) && leaseHeight === height){
                pending = true
            }
        })
        return pending
    }

    @boundMethod
    leaseIsSuccess(from: string, to: string, height: number): boolean{
        let pending = false;
        this.lists.forEach((tx)=>{
            if(txIsCancelLease(tx) && this.status(tx) === "success" && tx.from === from && tx.to === to && tx.leaseHeight === height){
                pending = true
            }
        })
        return pending
    }

    @boundMethod
    private key(address: string):string{
        return `stash:${address}`;
    }

    @boundMethod
    status(tx: IBlnTransaction): string{
        if(tx.blockHeight){
            return "success"
        }
        if(tx.reject){
            return "fail"
        }
        if(tx.time){

            let sub = Date.now() - (+tx.time) * 1000;
            sub = sub / 1000;

            let minutes = sub/60;

            if(minutes > 10) return 'fail';
        }
        return "pending"
    }


    @boundMethod
    get(address: string): Promise<IBlnTransaction[]>{
        return storage.get<Array<IBlnTransaction>>(this.key(address)).then((txns)=>{
            if(!txns){
                return []
            }
            return txns
        }).then((txns)=>{
            this.lists = txns;
            return txns
        }) 
    }

    @action
    set(address: string, txns: Array<IBlnTransaction>) {
        this.lists = txns
        return storage.set(this.key(address), txns)
    }

    @boundMethod
    add(address: string, tx: IBlnTransaction) {
        return this.get(address).then((txns)=>{
            if(!txns){
                return this.set(address, [tx]) 
            }
            let index = -1;
            txns.forEach((t, i)=>{
                if(t.txid === tx.txid){
                    index = i
                }
            })
            if(index>-1){
                txns.splice(index, 1, {...txns[index], ...tx})
            }else{
                txns.unshift(tx)
            }
            txns.sort((a, b)=>{
                return parseInt(b.time) - parseInt(a.time)
            })
            return this.set(address, txns)
        })
    }

    @boundMethod
    del(address: string, txid: string) {
        return this.get(address).then((txns)=>{
            if(txns && txns.length){
                let index = -1;
                txns.forEach((t, i)=>{
                    if(t.txid === txid){
                        index = i
                    }
                })
                if(index>-1){
                    txns.splice(index, 1)
                    this.set(address, txns)
                }
            }
        })
    }

    @boundMethod
    initStash(address: string) {
        // storage.remove(this.key(address))
        this.address = address
        return this.get(address)
        .then((txns)=>{
            this.lists = txns;
            return txns
        })
    }

    @boundMethod
    reset() {
        this.lists = [];
        this.address = ''
    }

    @boundMethod
    refresh(address?: string){
        address = address ?? this.address
        if(this.pendingLength && verifyAccount(address)){
            let txs = this.lists.filter((tx)=> this.status(tx) != 'success');

            return Promise.all(txs.map((tx)=>{
                return StoreAPI.fetchTX(tx.txid)
                .then((ntx)=>{
                    if(ntx){
                        return this.add(address??this.address, ntx)
                    }
                })
            }))
        }
        return Promise.resolve()
    }
}

export const stashStore = new StashStore()



export interface StashData {
    from: string,
    to: string,
    amount: any,
    txid?: string,
    message?: string | {[key: string]: any},
    encrypted?: boolean
}


import { blnnodeAPI } from '@app/config';
import { accountStore } from './account';
import Blacknetjs from 'blacknetjs';
const blacknetjs = new Blacknetjs()
blacknetjs.jsonrpc.endpoint = blnnodeAPI;

class StashStore2 {

   @observable private seq: number = 0   // transfer seq index
   @observable private address?: string // current address
   @observable private datas: StashData[] = []

   constructor(){
       reaction(
           ()=>this.datas.length, 
           this.running
       )
   }

   // add
   @action.bound
   add(data: StashData){
       this.datas.push(data)
   }

   // start
   start(){
       this.address = accountStore.currentAddress
   }
   // stop
   stop(){
       this.address = undefined
   }

   init(address: string){
       this.address = address
       // init
       return storage.get<Array<StashData>>(this.key(address)).then((txns)=>{
           if(!txns){
               return []
           }
           return txns
       }).then((txns)=>{
           this.datas = txns
           return txns
       })
   }

   private key(address: string): string {
       return `stash:${address}`;
   }

   @computed
   get length() {
       return this.datas.length
   }

   @action.bound
   running(){
       if(this.datas.length){
           const txdata = this.datas[0]
           return blacknetjs.jsonrpc.transfer(accountStore.currentMnemonic, {
               amount: txdata.amount,
               message: txdata.message,
               from: txdata.from,
               to: txdata.to,
               encrypted: txdata.encrypted ? 1 : 0
           }).then((res:any)=>{
               if (res.code === 200) {
                   this.datas.splice(0, 1)
                   return res.body
               } else {
                   return Promise.reject(res.body)
               }
           })
       }
   }

   @action
   updateSeq(seq: number) {
       return this.seq = seq
   }

   @action
   updateAddress(address?: string) {
       return this.address = address
   }
   
   syncSeq(address?: string) {
       return api.getSeq(address || this.address || "").then((seq)=>{
           this.seq = parseInt(seq)
           return this.seq
       })
   }

   transfer(from: string, to: string, message: string | {[key: string]: any}){
       
       
   }

   lease(from: string, to: string, amount: number){
       
       
   }

   cancellease(from: string, to: string, height: number, amount: number){
       
       
   }

}

