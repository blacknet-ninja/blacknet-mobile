/**
 * App global option store
 * @file App 全局公共存储
 * @module app/stores/option
 * @author Pony Ma <https://gitlab.com/blacknet-ninja>
 */

import { observable, action, computed } from 'mobx'
import { boundMethod } from 'autobind-decorator'
import { LANGUAGES } from '@app/constants/language'
import { STORAGE } from '@app/constants/storage'
import i18n, { getDeviceLanguage, updateLanguage, TLanguage } from '@app/services/i18n'
import storage from '@app/services/storage'
import { updateTheme, isDarkSystemTheme } from '@app/style/colors'
import { IBlnVersion } from '@app/types/bln'
import {version as appVersion} from '../../package.json';
import storeAPI from '@app/services/storeapi'
import { versionStringCompare } from '@app/utils/bln'
import numeral from 'numeral'

export interface IOptionStore {
  language: TLanguage
  darkTheme: boolean
}

export enum CURRENCY {
  CNY = 'CNY',
  USD = 'USD',
  BTC = 'BTC'
}

class OptionStore {

  constructor() {
    this.resetStore()
  }
  
  @observable.ref language: TLanguage = LANGUAGES.ZH
  @observable.ref portfolio_currency: CURRENCY = CURRENCY.USD
  @observable.ref darkTheme: boolean = isDarkSystemTheme
  @observable.ref version?: IBlnVersion
  @observable blnName: string = "Blacknet"

  @computed get versionShouldUpdate() {
    if(this.version && versionStringCompare(this.currentVersion, this.version.VersionName) === -1){
      return true
    }
    return false
  }

  @computed get versionUpdateUrl() {
    return this.version?.DownloadUrl || ""
  }

  @computed get currentVersion() {
    return appVersion
  }

  @computed get isEnLang() {
    return this.language === LANGUAGES.EN
  }

  @computed get locale() {
    return i18n.locale(this.language)
  }

  @computed get currencyNumeral() {
    return this.portfolio_currency
  }

  @computed
  get languageCurrency (){
    switch (this.language) {
      case LANGUAGES.ZH:
        return CURRENCY.CNY
      case LANGUAGES.EN:
        return CURRENCY.USD
      default:
        return CURRENCY.BTC
    }
  }

  @computed get currencySymbol(){
    return numeral.localeData().currency.symbol
  }

  @computed get currency(){
    return this.portfolio_currency
  }

  @action.bound
  updateLanguageWithOutStorage(language: TLanguage) {
    this.language = language
    updateLanguage(language)
  }

  @action.bound
  updateCurrency(currency: CURRENCY) {
    // 设置curreny
    this.portfolio_currency = currency
    // 设置numeral
    numeral.locale(this.currencyNumeral)
    // save storage
    storage.set(STORAGE.LOCAL_PORTFOLIO_CURRENCY, currency)
  }

  @action.bound
  updateVersion(version?: IBlnVersion) {
    this.version = version
  }

  @action.bound
  updateLanguage(language: TLanguage) {
    this.updateLanguageWithOutStorage(language)
    storage.set(STORAGE.LOCAL_LANGUAGE, language)
  }

  @action.bound
  updateDarkTheme(darkTheme: boolean) {
    this.darkTheme = darkTheme
    storage.set(STORAGE.LOCAL_DARK_THEME, darkTheme)
    updateTheme(darkTheme)
  }

  @boundMethod
  refreshVersion() {
    storeAPI.fetchAppInfo()
    .then(this.updateVersion)
  }

  @boundMethod
  resetStore() {
    this.initLanguage()
    .then(()=> this.initPortfolioCurrency())
    this.initDarkTheme()
    this.initVersion()
  }

  private initPortfolioCurrency() {
    // 获取本地存储的currency 默认根据语言
    storage.get<CURRENCY>(STORAGE.LOCAL_PORTFOLIO_CURRENCY)
      .then(currency => {
        return currency ? currency : this.languageCurrency
      })
      .then(currency => {
        console.log('Init app currency:', currency)
        this.updateCurrency(currency)
      })
  }

  private initLanguage() {
    // 获取本地存储的用户设置语言，若用户未设置语言，则首选本机语言
    return storage.get<TLanguage>(STORAGE.LOCAL_LANGUAGE)
      .then(localLanguage => {
        return localLanguage
          ? Promise.resolve(localLanguage)
          : getDeviceLanguage()
      })
      .then(language => {
        console.log('Init app language:', language)
        this.updateLanguageWithOutStorage(language)
      })
  }

  private initDarkTheme() {
    storage.get<boolean>(STORAGE.LOCAL_DARK_THEME).then(darkTheme => {
      if (darkTheme != null) {
        console.log('Init app darkTheme:', darkTheme)
        this.updateDarkTheme(darkTheme)
      }
    })
  }

  private initVersion() {
    storeAPI.fetchAppInfo()
    .then(this.updateVersion)
  }
}

export const optionStore = new OptionStore()

// numeral.unformat
// cny
if(!Object.keys(numeral.locales).includes('cny')){
  numeral.register('locale', 'CNY', {
    delimiters: {
        thousands: ',',
        decimal: '.'
    },
    abbreviations: {
        thousand: '千',
        million: '百万',
        billion: '十亿',
        trillion: '兆'
    },
    ordinal: function (number) {
        return '.';
    },
    currency: {
        symbol: '¥'
    }
  });
}
// usd
if(!Object.keys(numeral.locales).includes('usd')){
  numeral.register('locale', 'USD', {
    delimiters: {
        thousands: ',',
        decimal: '.'
    },
    abbreviations: {
        thousand: 'k',
        million: 'm',
        billion: 'b',
        trillion: 't'
    },
    ordinal: function (number) {
        var b = number % 10;
        return (~~ (number % 100 / 10) === 1) ? 'th' :
            (b === 1) ? 'st' :
            (b === 2) ? 'nd' :
            (b === 3) ? 'rd' : 'th';
    },
    currency: {
        symbol: '$'
    }
  });
}
// btc
if(!Object.keys(numeral.locales).includes('btc')){
  numeral.register('locale', 'BTC', {
    delimiters: {
        thousands: ',',
        decimal: '.'
    },
    abbreviations: {
        thousand: 'k',
        million: 'm',
        billion: 'b',
        trillion: 't'
    },
    ordinal: function (number) {
        var b = number % 10;
        return (~~ (number % 100 / 10) === 1) ? 'th' :
            (b === 1) ? 'st' :
            (b === 2) ? 'nd' :
            (b === 3) ? 'rd' : 'th';
    },
    currency: {
        symbol: '฿'
    }
  });
}