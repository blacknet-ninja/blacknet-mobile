/**
 * Style colors
 * @file Theme 主题/颜色配置
 * @module app/colors
 * @author Pony Ma <https://gitlab.com/blacknet-ninja>
 */

import { observable } from 'mobx'
import { Appearance } from 'react-native-appearance'

export enum Themes {
  Default = 'default',
  Dark = 'Dark'
}

type ThemeKey =

  | 'primary' // 主题色
  | 'secondary' // 次要主题色

  | 'accent' // 强调色
  | 'red' // 红色，错误色
  | 'yellow' // 黄色，警告色
  | 'grey' // 银灰色
  | 'inverse' // 反色

  | 'border' // 边框色
  | 'background' // 全局背景色
  | 'cardBackground' // 模块背景色

  | 'textDefault' // 默认文本
  | 'textSecondary' // 次要文本
  | 'textMuted' // 禁用文本

  | 'textTitle' // 标题文本
  | 'textLink' // 链接文本
  | 'iconfontDefault'
  | 'pure'
  | 'transferIn'
  | 'transferOut'
  | 'sbgcolor'
  | 'headerBGColor'
  | 'textInputBG'
  | 'deep'
  | 'white' //白色

type Theme = Record<ThemeKey, string>

export const Default: Theme = {

  primary: '#E8BA3F',
  secondary: '#262626',
  accent: '#4caf50',
  red: '#ff5722',
  yellow: '#e8ba3f',
  grey: '#e3e3e3',
  inverse: '#333333',
  border: '#eee',//
  background: '#ffffff',//
  cardBackground: '#ffffff',//
  sbgcolor: '#f0f0f0',//

  textDefault: '#000',
  textSecondary: '#333',
  textMuted: '#666',
  iconfontDefault: '#333',
  textTitle: '#222',
  textLink: '#3CA2A2',
  pure: '#fff',
  transferIn: '#27892f',
  transferOut: '#c02a1d',
  headerBGColor: '#e8ba3f',
  textInputBG: '#eee',
  deep: '#E8BA3F',
  white: '#ffffff'
}

export const Dark: Theme = {
  primary: '#E8BA3F',
  secondary: '#ADADAD',
  accent: '#4caf50',
  red: '#ff5722',
  yellow: '#e8ba3f',
  grey: '#e3e3e3',
  inverse: '#ADADAD',
  border: '#5B5B5B',//
  background: '#3b3b3b',//
  cardBackground: '#3b3b3b',//
  sbgcolor: '#f0f0f0',//

  textDefault: '#ADADAD',
  textSecondary: '#ADADAD',
  textMuted: '#eee',
  iconfontDefault: '#ADADAD',
  textTitle: '#ADADAD',
  textLink: '#3CA2A2',
  pure: '#E8BA3F',
  textInputBG: '#ccc',
  transferIn: '#27892f',
  transferOut: '#c02a1d',
  headerBGColor: '#303030',
  deep: '#2d2d2d',
  white: '#ffffff'
}

export const isDarkSystemTheme = Appearance.getColorScheme() === 'dark'
const colors = observable<Theme>(isDarkSystemTheme ? Dark : Default)
// const colors = observable<Theme>(Dark)
export default colors
export const updateTheme = (darkTheme: boolean) => {
  Object.keys(Default).forEach(key => {
    const themeKty = (key as keyof Theme)
    colors[themeKty] = darkTheme ? Dark[themeKty] : Default[themeKty]
  })
}
