/**
 * BLNListItem
 * @file BLNListItem
 * @module app/components/common/list-title
 * @author Pony Ma <https://gitlab.com/blacknet-ninja>
 */

import React, { Component, ComponentType, ReactNode } from "react";
import { StyleSheet, View, ViewStyle, StyleProp, Text } from 'react-native'
import colors from '@app/style/colors'
import { TouchableView } from '@app/components/common/touchable-view'
import { observer } from "mobx-react";
import { observable } from "mobx";
import FontAwesome5 from "react-native-vector-icons/FontAwesome5";
import { IBlnTransaction } from "@app/types/bln";
import { isReceive, shortAddress, showAmountPrefix, showBalance, showTime, showType, showTypeIcon } from "@app/utils/bln";
import { TextLink } from "../common/text";
import BLNBalance from "./BLNBalance";
 
export interface BLNListItemProps {
    title?: ReactNode
    subtitle?: ReactNode
    trailing?: ReactNode
    leading?: ReactNode
    onPress?(): void
    style?: StyleProp<ViewStyle>
    contentsStyle?: StyleProp<ViewStyle>
    trailingStyle?: StyleProp<ViewStyle>
    leadingStyle?: StyleProp<ViewStyle>
    titleStyle?: StyleProp<ViewStyle>
    subtitleStyle?: StyleProp<ViewStyle>
  }
  
@observer
export default class BLNListItem extends React.PureComponent<BLNListItemProps> {

    constructor(props: BLNListItemProps) {
        super(props)
    }

    render() {
        const { styles } = obStyles
        return (
            <TouchableView
                style={[styles.container, this.props.style]}
                onPress={this.props.onPress}
            >
                {this.props.leading ?
                    <View style={[styles.leading, this.props.leadingStyle]}>{this.props.leading}</View>
                : null}
                {this.props.title || this.props.subtitle || this.props.leading || this.props.trailing ?
                    <View style={[styles.content, this.props.contentsStyle]}>
                        {this.props.title ?
                            <View style={[styles.title, this.props.titleStyle]}>{this.props.title}</View>
                        : null}
                        {this.props.subtitle ?
                            <View style={[styles.subtitle, this.props.subtitleStyle]}>{this.props.subtitle}</View>
                        : null}
                    </View>
                : null}
                {this.props.trailing ?
                    <View style={[styles.trailing, this.props.trailingStyle]}>{this.props.trailing}</View>
                : null}
            </TouchableView>
        );
    }
}

const obStyles = observable({
    get styles() {
        return StyleSheet.create({
            container: {
                flexDirection: 'row',
                flex: 1,
                alignItems: 'center',
                justifyContent: 'flex-start'
            },
            content: {
                flexDirection: 'column',
                flex: 1,
                justifyContent: "center",
            },
            leading: {

            },
            trailing: {

            },
            title: {
        
            },
            subtitle: {
        
            }
        })
    }
})

export interface BLNListTitleMenuProps {
    title?: string
    subtitle?: string
    trailing?: ReactNode
    leading?: ReactNode
    onPress?(): void
    style?: StyleProp<ViewStyle>
    contentsStyle?: StyleProp<ViewStyle>
    trailingStyle?: StyleProp<ViewStyle>
    leadingStyle?: StyleProp<ViewStyle>
    titleStyle?: StyleProp<ViewStyle>
    subtitleStyle?: StyleProp<ViewStyle>
    reverseColor?: boolean
    trailingNum?: number,
    trailingText?: string,
    titleNumberOfLines?: number,
    IconName?: string
    IconComponent?: ComponentType<any>;
    hiddenTrailingRight?: boolean

}

@observer
export class BLNListTitleMenu extends React.PureComponent<BLNListTitleMenuProps> {

    render() {
        const { styles } = obStyles
        return (
            <TouchableView
                style={[{margin: 15, marginTop: 12, marginBottom: 12}, styles.container, this.props.style]}
                onPress={this.props.onPress}
            >
                {this.renderLeading()}
                {this.renderContent()}
                {this.renderTailing()}
            </TouchableView>
        );
    }


    renderContent(){
        const { styles } = obStyles
        return this.props.title || this.props.subtitle || this.props.leading || this.props.trailing ?
            <View style={[styles.content, this.props.contentsStyle]}>
                {this.props.title ?
                    <View style={[styles.title, this.props.titleStyle]}>{<Text numberOfLines={this.props.titleNumberOfLines} style={[{color: colors.textDefault}, this.props.reverseColor ? {color: colors.transferOut} : {}]}>{this.props.title}</Text>}</View>
                : null}
                {this.props.subtitle ?
                    <View style={[styles.subtitle, this.props.subtitleStyle]}>{<Text style={{color: colors.textSecondary}}>{this.props.subtitle}</Text>}</View>
                : null}
            </View>
        : null
    }

    renderLeading(){
        const { styles } = obStyles
        if(this.props.leading){
            return <View style={[{marginRight: 10}, styles.leading, this.props.leadingStyle]}>{this.props.leading}</View>
        }
        const { IconComponent, IconName } = this.props
        if(IconName && IconComponent){
            return <View style={[{marginRight: 10}, styles.leading, this.props.leadingStyle]}>
                    <View style={[{
                        backgroundColor: this.props.reverseColor ? colors.transferOut : colors.primary,
                        width: 30,
                        height: 30,
                        borderRadius: 30,
                        justifyContent: "center",
                        alignItems: "center"
                    }]}>
                        {<IconComponent 
                            name={this.props.IconName} 
                            color={colors.white}
                            size={16}
                        />}
                    </View>
                </View>
        }
        return null
    }

    renderTailing(){
        const { styles } = obStyles
        if(this.props.trailing){
            return <View style={[{marginLeft: 10}, styles.trailing, this.props.trailingStyle]}>{this.props.trailing}</View>
        }
        if(this.props.trailingText || this.props.trailingNum || !this.props.hiddenTrailingRight){
            return <View style={[{flexDirection: "row", alignItems: "center", marginLeft: 10}, styles.trailing, this.props.trailingStyle]}>
                {this.props.trailingText ?
                    <Text style={{color: colors.textMuted, marginRight: 6}}>{this.props.trailingText}</Text>
                : null}
                {this.props.trailingNum ?
                    <View style={[{backgroundColor: colors.primary, borderRadius: 10, width: 20,  height: 20, justifyContent: "center", alignItems: "center", marginRight: 6}]}>
                        <Text style={{color: colors.white}}>{this.props.trailingNum}</Text>
                    </View>
                : null}
                {this.props.hiddenTrailingRight ? null :
                    <FontAwesome5
                        name="chevron-right"
                        color={colors.textMuted}
                        size={14}
                    />
                }
            </View>
        }
        return null
    }
}

export interface BLNDappButtonProps {
    text?: string
    IconName?: string
    IconComponent?: ComponentType<any>;
    onPress?(): void
}
  
export const BLNDappButton = observer((props: BLNDappButtonProps): JSX.Element => {
    const { styles } = obStyles
    const { IconComponent, text } = props
    return (
        <TouchableView onPress={props.onPress} style={[{
            flexDirection: 'column',
            alignItems: "center"
        }]}>
            <View style={[{
                backgroundColor: colors.primary,
                width: 40,
                height: 40,
                borderRadius: 40,
                justifyContent: "center",
                alignItems: "center"
            }]}>
                {IconComponent ? <IconComponent 
                    name={props.IconName} 
                    color={colors.white}
                    size={16}
                /> : null}
            </View>
            {text ? <Text style={{marginTop: 4, fontSize: 14, color: colors.border}}>{text}</Text>: null}
        </TouchableView>
    )
})


export interface BLNTransactionItemProps {
    title?: string,
    name?: string,
    address: string,
    tx: IBlnTransaction,
    symbol?: string,
    onPress?(): void
}

@observer
export class BLNTransactionItem extends React.PureComponent<BLNTransactionItemProps> {

    render() {
        const { styles } = obStyles
        let tx = this.props.tx, title = (<Text></Text>);
        if (tx.type === 254 || tx.type === 999) {
            title = (<TextLink numberOfLines={1} >{showType(this.props.address, this.props.tx)}</TextLink>)
        } else {
            title = (<TextLink numberOfLines={1} >{shortAddress(tx.txid.toLocaleLowerCase())}</TextLink>)
        }
        return <BLNListItem
            style={{alignItems: 'flex-start', paddingTop: 15, paddingLeft: 15}}
            contentsStyle={{marginLeft:10, paddingRight: 6, paddingBottom: 15, borderBottomColor: colors.border, borderBottomWidth: 1}}
            trailingStyle={{borderBottomColor: colors.border, borderBottomWidth: 1, paddingRight: 15}}
            title={title}
            subtitle={<Text style={{color: colors.textSecondary}}>{showTime(tx.time)}</Text>}
            leading={
                <View style={[{
                    backgroundColor: isReceive(this.props.address, tx) ? colors.transferOut : colors.primary,
                    width: 20,
                    height: 20,
                    borderRadius: 20,
                    justifyContent: "center",
                    alignItems: "center"
                }]}>
                    <FontAwesome5
                        name={showTypeIcon(this.props.address, this.props.tx)}
                        color={colors.white}
                    />
                </View>
            }
            trailing={<View style={{flex: 1}}>
                <BLNBalance origin={true} prefix={showAmountPrefix(this.props.address, this.props.tx)}>{showBalance(tx.amount)}</BLNBalance>
            </View>}
            onPress={this.props.onPress}
        />;
    }

}

export interface BLNLabelListTitleProps {
    trailing?: ReactNode
    leading?: ReactNode
    onPress?(): void
}

@observer
export class BLNLabelListTitle extends React.PureComponent<BLNLabelListTitleProps> {

    constructor(props: BLNLabelListTitleProps) {
        super(props)
    }

    render() {
        const { styles } = obStyles2
        return (
            <TouchableView
                onPress={this.props.onPress}
                style={styles.labelListTitle}
            >
                <View style={styles.labelListTitleLeading}>
                    {this.props.leading ? this.props.leading : null}
                </View>
                <View style={styles.labelListTitleTrailing}>
                    {this.props.trailing ? this.props.trailing : null}
                </View>
            </TouchableView>
        );
    }
}

const obStyles2 = observable({
    get styles() {
        return StyleSheet.create({
            labelListTitle: {
                flexDirection: 'row',
                paddingTop: 15,
                paddingBottom: 15,
                alignItems: 'center',
                justifyContent: 'space-between',
            },
            labelListTitleLeading: {
                minWidth: 0,
                marginRight: 15,
            },
            labelListTitleTrailing: {
                flex: 1,
                flexDirection: 'row',
                justifyContent: "flex-end"
            }
        })
    }
})