/**
 * Modal
 * @file Modal
 * @module app/components/ui/modal
 * @author Pony Ma <https://gitlab.com/blacknet-ninja>
 */

import React, {Component} from "react";
import {Animated, BackHandler, NativeEventSubscription, Platform, Easing, StyleSheet, Dimensions} from "react-native";
import RootSiblings from 'react-native-root-siblings';
import { observer } from "mobx-react";
import { observable } from "mobx";

const {height} = Dimensions.get('window')
const animationShortTime = 250 //动画时长为250ms
const DEVICE_BACK_EVENT = 'hardwareBackPress';

export interface ModalViewProps {
    isDarkMode?: boolean
    autoKeyboard?: boolean
    useReactModal?: boolean
    modalType?: number
    animationType?: 'fade' | 'slide'
    onRequestClose?(): void
}

@observer
export default class ModalView extends Component<ModalViewProps> {

    state: {
        visible: boolean,
        animationSlide: Animated.Value,
        animationFade: Animated.Value
    }
  constructor(props: ModalViewProps) {
    super(props);
    this.state = {
      visible: false,
      animationSlide: new Animated.Value(0),
      animationFade: new Animated.Value(0)
    };
  }
  
  RootSiblings?: RootSiblings
  handleBack?: NativeEventSubscription

  render() {
    if(!this.RootSiblings){
        return <></>;
    }
    this.RootSiblings.update(this._renderRootSiblings())
  }

  _renderRootSiblings(){
    const { styles } = obStyles
    return (
      <Animated.View style={[styles.root,
        {opacity: this.state.animationFade},
        {
          transform: [{
            translateY: this.state.animationSlide.interpolate({
              inputRange: [0, 1],
              outputRange: [height, 0]
            }),
          }]
        }]}>
        {this.props.children}
      </Animated.View>
    );
  }

  show(callback:Function){
    if (this.isShow()) {
      return
    }
    this.RootSiblings = new RootSiblings(this._renderRootSiblings(), () => {
        if (this.props.animationType === 'fade') {
            this._animationFadeIn(callback)
        } else if (this.props.animationType === 'slide') {
            this._animationSlideIn(callback)
        } else {
            this._animationNoneIn(callback)
        }
    });
    // 这里需要监听 back 键
    this._addHandleBack()
  }

  disMiss = (callback?: Function) => {
    if (!this.isShow()) {
      return
    }
    const {modalType, animationType} = this.props
    if (modalType === 1) { //modal
      this.setState({visible: false}, () => callback && callback())
    } else { //RootSiblings和View
      if (animationType === 'fade') {
        this._animationFadeOut(callback)
      } else if (animationType === 'slide') {
        this._animationSlideOut(callback)
      } else {
        this._animationNoneOut(callback)
      }
      // 移除 back 键的监听
      this._removeHandleBack()
    }
  }

  isShow = () => {
    return !!this.RootSiblings
  }

  _addHandleBack = () => {
    if (Platform.OS === 'ios') {
      return
    }
    // 监听back键
    this.handleBack = BackHandler.addEventListener(DEVICE_BACK_EVENT, () => {
      const {onRequestClose} = this.props
      if (onRequestClose) {
        onRequestClose()
      } else {
        this.disMiss()
      }
      return true
    });
  }

  _removeHandleBack = () => {
    if (Platform.OS === 'ios') {
      return
    }
    this.handleBack && this.handleBack.remove()
  }

  _animationNoneIn = (callback?: Function) => {
    this.state.animationSlide.setValue(1)
    this.state.animationFade.setValue(1)
    callback && callback()
  }

  _animationNoneOut = (callback?: Function) => {
    this._animationCallback(callback);
  }

  _animationSlideIn = (callback?: Function) => {
    this.state.animationSlide.setValue(0)
    this.state.animationFade.setValue(1)
    Animated.timing(this.state.animationSlide, {
        easing: Easing.in(Easing.ease),
        duration: animationShortTime,
        toValue: 1,
        useNativeDriver: false
    }).start(() => callback && callback());
  }

  _animationSlideOut = (callback?: Function) => {
    this.state.animationSlide.setValue(1)
    this.state.animationFade.setValue(1)
    Animated.timing(this.state.animationSlide, {
        easing: Easing.in(Easing.ease),
        duration: animationShortTime,
        toValue: 0,
        useNativeDriver: false
    }).start(() => this._animationCallback(callback));
  }

  _animationFadeIn(callback?: Function){
    this.state.animationSlide.setValue(1)
    this.state.animationFade.setValue(0)
    Animated.timing(this.state.animationFade, {
        easing: Easing.in(Easing.ease),
        duration: animationShortTime,
        toValue: 1,
        useNativeDriver: false
    }).start(() => callback && callback());
  }

  _animationFadeOut(callback?: Function){
    this.state.animationSlide.setValue(1)
    this.state.animationFade.setValue(1)
    Animated.timing(this.state.animationFade, {
        easing: Easing.in(Easing.ease),
        duration: animationShortTime,
        toValue: 0,
        useNativeDriver: false
    }).start(() => this._animationCallback(callback));
  }

  _animationCallback(callback?: Function){
    this.RootSiblings && this.RootSiblings.destroy(() => {
        callback && callback()
        this.RootSiblings = undefined
    })
  }
}

const obStyles = observable({
  get styles() {
      return StyleSheet.create({
        root: {
          position: 'absolute',
          left: 0,
          top: 0,
          right: 0,
          bottom: 0,
        }
      })
  }
})