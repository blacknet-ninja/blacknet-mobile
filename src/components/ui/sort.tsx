/**
 * Sort
 * @file sort
 * @module app/components/ui/sort
 * @author Pony Ma <https://gitlab.com/blacknet-ninja>
 */

import React from 'react'
import { StyleProp, Text, TextStyle, View, ViewStyle } from 'react-native'
import { observer } from 'mobx-react'
import colors from '@app/style/colors'
import { TouchableView } from '../common/touchable-view'
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5'

export const SortTouch = observer((props: {
    onChange?(direction?: 'up' | 'down'): void
    name?: string
    style?: StyleProp<ViewStyle>
    textStyle?: StyleProp<TextStyle>
    direction?: 'up' | 'down'
    iconColor?: string
    sortColor?: string
}): JSX.Element => {
    // const [direction, setDirection] = useState(props.direction)
    const upColor = props.direction === 'up' ? props.sortColor || colors.primary : props.iconColor || colors.textDefault
    const downColor = props.direction === 'down' ? props.sortColor || colors.primary : props.iconColor || colors.textDefault
    return (
        <TouchableView
            style={[{

            }]}
            onPress={() => {
                if (props.direction === 'up') {
                    // setDirection('down')
                    props.onChange && props.onChange('down')
                } else {
                    // setDirection('up')
                    props.onChange && props.onChange('up')
                }
            }}
        >
            <View style={[{
                flexDirection: 'row',
                justifyContent: 'center',
                alignItems: 'center'
            }]}>
                <View style={[{
                    flexDirection: 'column',
                    justifyContent: 'center',
                    alignItems: 'center'
                }]}>
                    <FontAwesome5 name={'sort-up'} size={16} style={[{ marginBottom: -8 }, { color: upColor }]} />
                    <FontAwesome5 name={'sort-down'} size={16} style={[{ marginTop: -8 }, { color: downColor }]} />
                </View>
                {props.name ?
                    <Text style={[{ marginLeft: 2, color: colors.textDefault }, props.textStyle]}>{props.name}</Text>
                    : null}
            </View>
        </TouchableView>
    )
})

