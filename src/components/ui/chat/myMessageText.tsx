/**
 * MyMessageText
 * @file myMessageText
 * @module app/components/common/ui/chat/myMessageText
 * @author Pony Ma <https://gitlab.com/blacknet-ninja>
 */

import { TouchableView } from '@app/components/common/touchable-view';
import { chatStore } from '@app/pages/chat/stores/chat';
import API from '@app/services/api';
import { accountStore } from '@app/stores/account';
import { optionStore } from '@app/stores/option';
import colors from '@app/style/colors';
import { IBlnScanMessage } from '@app/types/bln';
import { boundMethod } from 'autobind-decorator';
import { action, computed, observable } from 'mobx';
import { observer } from 'mobx-react';
import React from 'react';
import { ActivityIndicator, StyleSheet, View, Linking } from 'react-native';
import { IMessage, MessageTextProps, utils } from 'react-native-gifted-chat';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';
import PropTypes from 'prop-types';
// @ts-ignore
import ParsedText from 'react-native-parsed-text';
import Communications from 'react-native-communications';
import { BLNChatData } from '@app/utils/chat';

const { StylePropType } = utils;
const WWW_URL_PATTERN = /^www\./i;
const textStyle = {
    fontSize: 16,
    lineHeight: 20,
    marginTop: 5,
    marginBottom: 5,
    marginLeft: 10,
    marginRight: 10,
};

const obStyles = observable({
    get left() {
        return StyleSheet.create({
            messageText: {
                borderRadius: 3,
                backgroundColor: colors.border,
                marginBottom: 5
            },
            text: {
                color: 'black',
                ...textStyle,
            },
            link: {
                color: 'black',
                textDecorationLine: 'underline',
            },
            container: {
                justifyContent: 'flex-start'
            }
        })
    },
    get right() {
        return StyleSheet.create({
            messageText: {
                borderRadius: 3,
                backgroundColor: colors.primary,
                marginBottom: 5
            },
            text: {
                color: 'white',
                ...textStyle,
            },
            link: {
                color: 'white',
                textDecorationLine: 'underline',
            },
            container: {
                justifyContent: 'flex-end'
            }
        })
    },
    get styles() {
        return StyleSheet.create({
            container: {
                flexDirection: 'row',
                alignItems: 'center'
            },
            statusView: {
                paddingLeft: 10,
                paddingRight: 10
            }
        })
    }
})

const DEFAULT_OPTION_TITLES = ['Call', 'Text', 'Cancel'];

@observer
export default class MyMessageText extends React.Component<MessageTextProps<IMessage> & {
    position: string
    formatQuote?(text: string): Promise<{[key: string]: any}>
    formatText?(text: string): any
    extendObject?(txid: string): any
}> {
    static contextTypes = {
        actionSheet: PropTypes.func,
    };

    static defaultProps = {
        position: 'left',
        optionTitles: DEFAULT_OPTION_TITLES,
        currentMessage: {
            text: '',
        },
        containerStyle: {},
        textStyle: {},
        linkStyle: {},
        customTextStyle: {},
        textProps: {},
        parsePatterns: () => []
    };

    render(){
        const { position } = this.props
        const { styles } = obStyles
        if(!this.message){
            return null
        }
        return <View style={[styles.container, obStyles[position].container]}>
            {!this.sent && !this.pending  ? 
                <View style={[styles.statusView]}>
                    <TouchableView
                        onPress={this.reSend}
                    >
                        <FontAwesome5 name={'exclamation-circle'}  size={24} color={colors.transferOut} />
                    </TouchableView>
                </View>
            : null}
            {!this.sent && this.pending ? 
                <View style={styles.statusView}>
                    <ActivityIndicator color={optionStore.darkTheme ? colors.white : colors.textDefault}/>
                </View>
            : null}
            <View style={obStyles[position].messageText}>
                {/* <MessageText 
                    textStyle={{
                        left: {color: colors.textMuted}, 
                        right: {color: optionStore.darkTheme ? colors.textMuted : colors.textDefault}
                    }} 
                    {...this.props}
                /> */}
                {this.renderText()}
            </View>
        </View>
    }

    renderText(){
        let text = this.props.currentMessage?.text
        if(this.props.formatText){
            const data = this.props.formatText(this.props.currentMessage?.text || '')
            if(data){
                text = data.data.content
            }
        }
        const linkStyle = [
            obStyles[this.props.position].link,
            this.props.linkStyle && this.props.linkStyle[this.props.position],
        ];
        const textStyle = {
            left: {color: colors.textMuted}, 
            right: {color: optionStore.darkTheme ? colors.textMuted : colors.textDefault}
        }
        return (<View style={[
            obStyles[this.props.position].container,
            this.props.containerStyle &&
                this.props.containerStyle[this.props.position],
        ]}>
        <ParsedText style={[
            obStyles[this.props.position].text,
            textStyle[this.props.position],
            this.props.textStyle && this.props.textStyle[this.props.position],
            this.props.customTextStyle,
        ]} parse={[
            ...(this.props.parsePatterns ? this.props.parsePatterns(linkStyle) : []),
            { type: 'url', style: linkStyle, onPress: this.onUrlPress },
            { type: 'phone', style: linkStyle, onPress: this.onPhonePress },
            { type: 'email', style: linkStyle, onPress: this.onEmailPress },
        ]} childrenProps={{ ...this.props.textProps }}>
          {text}
        </ParsedText>
      </View>);
    }

    @boundMethod
    onUrlPress(url: any){
        if (WWW_URL_PATTERN.test(url)) {
            this.onUrlPress(`http://${url}`);
        }
        else {
            Linking.canOpenURL(url).then(supported => {
                if (!supported) {
                    console.error('No handler for URL:', url);
                }
                else {
                    Linking.openURL(url);
                }
            });
        }
    }
    @boundMethod
    onPhonePress(phone: any){
        const { optionTitles } = this.props;
        const options = optionTitles && optionTitles.length > 0
            ? optionTitles.slice(0, 3)
            : DEFAULT_OPTION_TITLES;
        const cancelButtonIndex = options.length - 1;
        this.context.actionSheet().showActionSheetWithOptions({
            options,
            cancelButtonIndex,
        }, (buttonIndex:any) => {
            switch (buttonIndex) {
                case 0:
                    Communications.phonecall(phone, true);
                    break;
                case 1:
                    Communications.text(phone);
                    break;
                default:
                    break;
            }
        });
    }
    @boundMethod
    onEmailPress(email: any){
        return Communications.email([email], null, null, null, null)
    }

    constructor(props: any){
        super(props)
        this.message = this.props.currentMessage
    }

    componentDidMount(){
        const { currentMessage } = this.props;
        this.message = currentMessage
        if(!this.sent && this.pending){
            this.send()
        }
    }

    @observable private message: IMessage | undefined

    @computed
    get sent(){
        return this.message?.sent
    }

    @computed
    get pending(){
        return this.message?.pending
    }

    @action
    private update(msg: IBlnScanMessage, id: string, to: string, success: boolean = false){
        let message = this.message
        if(message){
            message.pending = msg.pending
            message.sent = msg.sent
            message.received = msg.received
        }
        if(this.props.extendObject){
            message = {...message, ...this.props.extendObject(id)}
        }
        chatStore.replace(accountStore.currentAddress, to, id, msg)
        if(success){
            msg.to = to
            chatStore.upsertsList(accountStore.currentAddress, [msg])
        }
        this.message = message
    }

    @boundMethod
    @action
    private reSend(){
        if(this.message){
            this.message.pending = true
            this.message.sent = false
        }
        this.send()
    }

    @boundMethod
    private send(){
        let data
        if(this.props.formatText){
            data = this.props.formatText(this.props.currentMessage?.text || '')
        }
        if(!data){
            return null
        }
        const id = (this.message?._id || '').toString()
        const chatData = BLNChatData.formart(data)
        return API.sendChatMessage(
            accountStore.currentMnemonic,
            accountStore.currentAddress,
            data
        ).then((msg)=>{
            if(msg){
                msg.pending = false
                msg.received = false
                msg.sent = true
                msg.decrypted = true
                this.update(msg, id, chatData.getTo() ,true)
            } else {
                return Promise.reject(msg)
            }
        }).catch((e)=>{
            // fail
            let msg = { 
                from: accountStore.currentAddress,
                to: chatData.getToAddress(),
                time: chatData.time,
                txid: id,
                group: chatData.isGroup,
                sent: false,
                received: false,
                pending: false,
                text: chatData.getText()
            } as IBlnScanMessage
            this.update(msg, id, chatData.getTo())
        })
        .finally(() => {
            accountStore.refreshBalance()
        })
    }
}
