/**
 * MyMessageVideo
 * @file myMessageVideo
 * @module app/components/common/ui/chat/myMessageVideo
 * @author Pony Ma <https://gitlab.com/blacknet-ninja>
 */

import PropTypes from 'prop-types';
import React, { cloneElement, Component } from 'react';
import { ActivityIndicator, StatusBar, StyleSheet, Text, View, } from 'react-native';
import Lightbox from 'react-native-lightbox-v2';
import { IMessage, MessageVideoProps } from 'react-native-gifted-chat'
import sizes from '@app/style/sizes';
import colors from '@app/style/colors';
import { observer } from 'mobx-react';
import { action, computed, observable } from 'mobx';
import { boundMethod } from 'autobind-decorator';
import API from '@app/services/api';
import { accountStore } from '@app/stores/account';
import {chatStore} from '@app/pages/chat/stores/chat';
import { IBlnScanMessage } from '@app/types/bln';
import { TouchableView } from '@app/components/common/touchable-view';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';
import { optionStore } from '@app/stores/option';
import { IS_ANDROID } from '@app/config';
import Video, {RNVideo} from '@app/components/common/video';
import i18n from '@app/services/i18n';
import { LANGUAGE_KEYS } from '@app/constants/language';
import { BLNChatData } from '@app/utils/chat';

const obStyles = observable({
    get left() {
        return StyleSheet.create({
            container: {
                justifyContent: 'flex-start'
            }
        })
    },
    get right() {
        return StyleSheet.create({
            container: {
                justifyContent: 'flex-end'
            }
        })
    },
    get styles(){
        return StyleSheet.create({
            container: {
                flexDirection: 'row',
                alignItems: 'center'
            },
            statusView: {
                padding: 10
            },
            loading: {
                flex: 1,
                justifyContent: 'center',
                alignItems: 'center',
                flexDirection: 'column'
            },
            contentView: {
                position: 'relative'
            },
            loadingText: {
                marginTop: 2,
                color: optionStore.darkTheme ? colors.white : colors.textDefault
            },
            video: {
                width: '100%',
                height: '100%',
                position: 'absolute',
                flex: 1,
                left: 0,
                right: 0,
                top: 0,
                bottom: 0,
                zIndex: 0
            },
            preloadView: {
                position: 'absolute',
                flex: 1,
                left: 0,
                right: 0,
                top: 0,
                bottom: 0,
                zIndex: 1,
                backgroundColor: colors.background,
                justifyContent: 'center',
                alignItems: 'center'
            },
            loadingView: {
                position: 'absolute',
                backgroundColor: colors.background,
                opacity: 0.5,
                flex: 1,
                left: 0,
                right: 0,
                top: 0,
                bottom: 0,
                zIndex: 1
            },
            playView: {
                position: 'absolute',
                backgroundColor: colors.background,
                opacity: 0.2,
                flex: 1,
                left: 0,
                right: 0,
                top: 0,
                bottom: 0,
                zIndex: 1,
                justifyContent: 'center',
                alignItems: 'center'
            }
        })
    }
})

const DEFAULT_OPTION_TITLES = [i18n.t(LANGUAGE_KEYS.QUOTE), i18n.t(LANGUAGE_KEYS.CANCEL)];

@observer 
export default class MyMessageVideo extends Component<MessageVideoProps<IMessage> & {
    onQuotePress?(message: IMessage): void
    formatText?(text: string): any
    position: string
    extendObject?(txid: string): any
}> {
    static contextTypes = {
        actionSheet: PropTypes.func,
    };
    static defaultProps = {
        position: 'left'
    };

    player: React.RefObject<RNVideo> = React.createRef()
    maxWidth = sizes.screen.width / 3
    maxHeight = sizes.screen.height / 4
    state = {
        imageHeight: 45,
        imageWidth: 45,
        loaded: false,
        loadError: false
    }
    render() {
        const { containerStyle, position } = this.props;
        const { styles } = obStyles
        if (!this.message) return null;
        const video = this.renderVideo()
        return (
            <View style={[styles.container, obStyles[position].container, containerStyle]}>
                {!this.sent && !this.pending  ? 
                    <View style={[styles.statusView]}>
                        <TouchableView
                            onPress={this.reSend}
                        >
                            <FontAwesome5 name={'exclamation-circle'}  size={24} color={colors.transferOut} />
                        </TouchableView>
                    </View>
                : null}
                <Lightbox
                    onOpen={()=>{
                        if(IS_ANDROID){
                            StatusBar.setBackgroundColor('#000000')
                        }
                        StatusBar.setHidden(false, 'fade')
                    }}
                    onClose={()=>{
                        if(IS_ANDROID){
                            StatusBar.setBackgroundColor('transparent')
                        }
                        StatusBar.setHidden(false, 'fade')
                        this.player.current?.seek(0)
                    }}
                    renderContent={()=>{
                        return cloneElement(video, {
                            paused: false
                        })
                    }}
                    onLongPress={this.onLongPress}
                    >
                    <View style={[styles.contentView, {width: this.state.imageWidth, height: this.state.imageHeight}]}>
                        {video}
                        {this.renderLoaded()}
                        {!this.state.loadError && this.state.loaded && !this.sent && this.pending && this.process !== 100 ? 
                            this.renderLoading()
                        : 
                            this.renderPlay()
                        }
                    </View>
                </Lightbox>
            </View>
        );
    }

    @boundMethod
    onLongPress(){
        const { currentMessage } = this.props;
        if (currentMessage && currentMessage.video) {
            const options = DEFAULT_OPTION_TITLES
            const cancelButtonIndex = options.length - 1;
            this.context.actionSheet().showActionSheetWithOptions({
                options,
                cancelButtonIndex,
            }, (buttonIndex: number) => {
                switch (buttonIndex) {
                    case 0:
                        this.props.onQuotePress && this.props.onQuotePress(currentMessage)
                        break;
                    default:
                        break;
                }
            });
        }
    };

    renderVideo(){
        const { styles } = obStyles
        return <Video   // Can be a URL or a local file.
                resizeMode="contain"
                ref={this.player} // Store reference
                onLoad={(data: any)=>{
                    let height = data.naturalSize.height
                    let width = data.naturalSize.width
                    let orientation = data.naturalSize.orientation
                    if(orientation === 'landscape'){
                        if(width > this.maxWidth){
                            height = this.maxWidth / width * height
                            width = this.maxWidth
                        }
                    } else {
                        if(height > this.maxHeight){
                            width = this.maxHeight / height * width
                            height = this.maxHeight
                        }
                    }
                    this.player.current?.seek(0)
                    this.setState({
                        imageHeight: height,
                        imageWidth: width,
                        loaded: true
                    }, ()=>{
                        if( this.pending && !this.sent ){
                            this.animation()
                            this.upload()
                        }
                    })
                }}
                onReadyForDisplay={()=>{
                    this.setState({
                        loaded: true
                    })
                }}
                onError={(err: any)=>{
                    this.setState({
                        loadError: true
                    })
                }}
                style={[styles.video]}
                paused={true}
                source={{uri: this.message?.video, cache: true}}
                // onFullscreenPlayerDidDismiss={}
            />
    }

    renderLoaded(){
        const { styles } = obStyles
        if(!this.state.loaded){
            return <View style={[styles.preloadView, {width: this.state.imageWidth, height: this.state.imageHeight}]}>
                        <FontAwesome5 name={'video'}  size={40} color={colors.textMuted} />
                    </View>
        }
        return null
    }

    renderLoading(){
        const { styles } = obStyles
        return <View style={styles.loadingView}>
                    <View style={styles.loading}>
                        <ActivityIndicator color={optionStore.darkTheme ? colors.white : colors.textDefault}/>
                        <Text style={styles.loadingText}>{this.percent}</Text>
                    </View>
                </View>
    }

    renderPlay(){
        const { styles } = obStyles
        if(this.state.loaded){
            return <View style={styles.playView}>
                        <FontAwesome5 name={'play-circle'}  size={40} color={colors.textMuted} />
                    </View>
        }
        return null
    }

    componentDidMount(){
        this.message = this.props.currentMessage
    }

    @observable private process: number = 0
    @observable private message: IMessage | undefined

    @computed
    get sent(){
        return this.message?.sent
    }

    @computed
    get pending(){
        return this.message?.pending
    }

    @boundMethod
    @action
    updateProcess(n: number){
        this.process = n
    }

    @computed
    get percent(){
        return `${this.process}%`
    }

    @boundMethod
    @action
    private reSend(){
        if(this.message){
            this.message.sent = false
            this.message.pending = true
        }
        this.process = 0
        this.animation()
        this.upload()
    }

    @action
    private update(msg: IBlnScanMessage, id: string, to: string, success: boolean = false){
        let message = this.message
        if(message){
            message.pending = msg.pending
            message.sent = msg.sent
            message.received = msg.received
        }
        if(this.props.extendObject){
            message = {...message, ...this.props.extendObject(id)}
        }
        chatStore.replace(accountStore.currentAddress, to, id, msg)
        if(success){
            msg.to = to
            chatStore.upsertsList(accountStore.currentAddress, [msg])
        }
        this.message = message
    }

    @boundMethod
    private upload(){
        let data
        if(this.props.formatText){
            data = this.props.formatText(this.props.currentMessage?.text || '')
        }
        if(!data){
            return null
        }
        // data
        const {currentMessage} = this.props
        const id = (this.props.currentMessage?._id || '').toString()
        const chatData = BLNChatData.formart(data)
        API.upload(currentMessage?.image || '', chatData.getData().mime, id).then((res)=>{
            if(res){
                chatData.updateData({
                    uri: res.cid
                })
                return res.cid
            }
            return null
        }).then((text)=>{
            if(text){
                return API.sendChatMessage(
                    accountStore.currentMnemonic,
                    accountStore.currentAddress,
                    chatData.formart()
                )
            }
            return null
        }).then((msg)=>{
            if(msg){
                // success
                this.updateProcess(100)
                msg.pending = false
                msg.received = false
                msg.sent = true
                msg.decrypted = true
                this.update(msg, id, chatData.getTo() ,true)
            } else {
                // fail
                return Promise.reject(msg)
            }
        }).catch((e)=>{
            // fail
            let msg = { 
                from: accountStore.currentAddress,
                to: chatData.getToAddress(),
                time: chatData.time,
                txid: id,
                group: chatData.isGroup,
                sent: false,
                received: false,
                pending: false,
                text: chatData.getText()
            } as IBlnScanMessage
            this.update(msg, id, chatData.getTo())
        })
        .finally(() => {
            accountStore.refreshBalance()
        })
    }

    animation(){
        const hide = (n: number)=>{
            if(n < this.process){
                return
            }
            if(n < 99){
                requestAnimationFrame(()=>hide(n + 1))   
            }
            this.updateProcess(n)
        }
        return hide(0)
    }
}