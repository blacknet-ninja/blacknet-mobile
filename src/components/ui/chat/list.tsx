/**
 * list
 * @file list
 * @module app/components/common/ui/chat/list
 * @author Pony Ma <https://gitlab.com/blacknet-ninja>
 */

import React, { Component } from "react";
import { StyleSheet, View, Text, StyleProp, ViewStyle } from 'react-native'
import colors from '@app/style/colors'
import { TouchableView } from '@app/components/common/touchable-view'
import sizes from "@app/style/sizes";
import { showTimeFromNow } from "@app/utils/bln";
import { TextLink } from "@app/components/common/text";
import { observer } from "mobx-react";
import { observable } from "mobx";
import Image from "@app/components/common/image";
import { IBlnScanMessage } from "@app/types/bln";
import chat, { BLNChatData } from "@app/utils/chat";
import { accountStore } from "@app/stores/account";
import i18n from "@app/services/i18n";
import { LANGUAGE_KEYS } from "@app/constants/language";
import { userStore } from "@app/stores/users";

export interface ChatMessageListItemProps {
    name?: string
    time?: string
    last?: string
    avatar?: string
    style?: StyleProp<ViewStyle>
    onPress?(params: any): void
    msg?: IBlnScanMessage
}
  
@observer
export class ChatMessageListItem extends React.PureComponent<ChatMessageListItemProps> {

    render() {
        const { styles } = obStyles

        const message = this.props.msg
        if(!message){
            return
        }
        const data = chat.format(message.text)
        if(!data){
            return
        }
        const chatData = BLNChatData.formart(data)
        if(chatData.isUnknow()) {
            chatData.setData({
                content: `[${i18n.t(LANGUAGE_KEYS.CHAT_UNKNOW)}]`
            })
        }
        let text = chatData.getContent()
        if(chatData.isImage()) {
            text = '[image]'
        }
        if(chatData.isVideo()) {
            text = '[video]'
        }
        if(chatData.isAudio()) {
            text = '[audio]'
        }

        let name = message.group ? message.groupInfo?.name : userStore.getUserName(message.to)
        let logo = message.group ? message.groupInfo?.logo : userStore.getUserImage(message.to)

        let params = { 
            address: message.to, 
            name: name, 
            user: accountStore.currentAddress, 
            group: message.group, 
            groupInfo: message.groupInfo, 
            isPrivate: !message.group ? 1 : message.groupInfo && message.groupInfo.isPrivate ? 1 : 0
        }
        return (
            <TouchableView
                style={[styles.listWarp, this.props.style]}
                onPress={()=>{
                    this.props.onPress && this.props.onPress(params)
                }}
            >
                <View style={styles.avatarBox}>
                    {logo ?
                        <Image
                            style={styles.avatar}
                            source={{uri: logo}}
                        />
                    : 
                        <Image
                            style={styles.avatar}
                            source={require('@app/assets/images/1024.png')}
                        />
                    }
                </View>
                <View style={styles.box}>
                    <View style={styles.left}>
                        <TextLink style={styles.leftText} numberOfLines={1}>{name}</TextLink>
                        <Text style={[styles.leftText, {color: colors.textSecondary}]} numberOfLines={1}>{text}</Text>
                    </View>
                    <View style={styles.right}>
                        <Text style={{color: colors.textDefault}}>{message.time ? showTimeFromNow(message.time) : ""}</Text>
                    </View>
                </View>
            </TouchableView>
        );
    }
}

const obStyles = observable({
    get styles() {
        return StyleSheet.create({
            listWarp:{
                flexDirection: 'row',
                alignItems: 'center',
                marginTop: 3,
                flex: 1
            },
            avatar:{
                width: 40,
                height: 40,
                borderRadius: 40
            },
            avatarBox:{
                marginRight: 15,
                marginLeft: 15
            },
            left: {
                flex: 1,
                paddingRight: 10,
            },
            leftText: {
                paddingVertical: 4,
                
            },
            right: {
                minWidth: 0,
                paddingRight: 10
            },
            box: {
                borderBottomColor: colors.border,
                borderBottomWidth: 1,
                width: sizes.screen.width - 70,
                flexDirection: 'row',
                alignItems: 'center',
                justifyContent: 'space-between',
                paddingBottom: 5,
                paddingTop: 5,
            },
            title: {
                flex: 1
            },
            subTitle: {
                flex: 1
            },
            time: {
            }
        })
    }
})




