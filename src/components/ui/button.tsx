/**
 * button
 * @file button
 * @module app/components/common/ui/button
 * @author Pony Ma <https://gitlab.com/blacknet-ninja>
 */

import React, { Component } from "react";
import { StyleProp, StyleSheet, Text, TextStyle, ViewProps, ViewStyle } from 'react-native'
import colors from '@app/style/colors'
import { TouchableView } from '@app/components/common/touchable-view'
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5'
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons'
import { observer } from "mobx-react";
import { observable } from "mobx";
import storeapi from "@app/services/storeapi";
import i18n from "@app/services/i18n";
import { LANGUAGE_KEYS } from "@app/constants/language";
 
export interface ButtonProps {
    text?: String
    onPress?(): void
    style?: StyleProp<ViewStyle>
    textStyle?: StyleProp<TextStyle>
}

@observer
export default class Button extends React.PureComponent<ButtonProps> {

    render() {
        const { styles } = obStyles
        return (
            <TouchableView
                style={[styles.button, this.props.style]}
                onPress={this.props.onPress}
            >
                <Text style={[styles.bottomButtonText, this.props.textStyle]}>{this.props.text}</Text>
            </TouchableView>
        );
    }
}

@observer
export class DeleteButton extends React.PureComponent<ButtonProps> {

    render() {
        const { styles } = obStyles
        return (
            <TouchableView
            style={styles.deleteButton}
            onPress={this.props.onPress}
            >
                <Text style={styles.deleteButtonText}>{this.props.text}</Text>
            </TouchableView>
        );
    }
}

@observer
export class CopyButton extends React.PureComponent<ButtonProps> {

    render() {
        const { styles } = obStyles
        return (
            <TouchableView
            style={styles.copyButton}
            onPress={this.props.onPress}
            >
                <Text style={styles.copyButtonText}>{this.props.text}</Text>
            </TouchableView>
        );
    }
}


export interface AddButtonProps {
    style?: StyleProp<ViewStyle>
    size?: number
}
@observer
export class AddButton extends React.PureComponent<AddButtonProps & ButtonProps> {

    render() {
        const { styles } = obStyles
        return (
            <TouchableView
                style={[styles.addButton, this.props.style]}
                onPress={this.props.onPress}
            >
                <FontAwesome5 name={"comment-dots"} size={this.props.size} style={styles.addButtonIcon}></FontAwesome5>
            </TouchableView>
        );
    }
}


export interface IconButtonProps {
    style?: StyleProp<ViewStyle>
    size?: number
    name: string
    iconColor?: string
}
@observer
export class IconButton extends React.PureComponent<IconButtonProps & ButtonProps> {

    render() {
        const { styles } = obStyles
        return (
            <TouchableView
                style={[styles.IconButton, this.props.style]}
                onPress={this.props.onPress}
            >
                <MaterialCommunityIcons name={this.props.name} size={this.props.size} style={styles.IconButtonIcon} color={this.props.iconColor || colors.white}></MaterialCommunityIcons>
            </TouchableView>
        );
    }
}

export interface TextButtonProps {
    style?: StyleProp<ViewStyle>
    textStyle?: StyleProp<TextStyle>
    text: string
}
@observer
export class TextButton extends React.PureComponent<TextButtonProps & ButtonProps> {

    render() {
        const { styles } = obStyles
        return (
            <TouchableView
                style={[styles.TextButton, this.props.style]}
                onPress={this.props.onPress}
            >
                <Text style={[styles.TextButtonText, this.props.textStyle]}>{this.props.text}</Text>
            </TouchableView>
        );
    }
}

const obStyles = observable({
    get styles() {
        return StyleSheet.create({
            TextButton: {

            },
            TextButtonText: {
                color: colors.primary
            },
            IconButton: {
                justifyContent: "center",
                alignItems: "center",
                width: 40,
                height: 40
            },
            IconButtonIcon: {
            },
            addButton: {
                justifyContent: "center",
                alignItems: "center",
                backgroundColor: colors.deep,
            },
            addButtonIcon: {
            },
            copyButton: {
                height: 40,
                borderRadius: 2,
                flexDirection:"column",
                alignItems: "center",
                backgroundColor: colors.deep,
                justifyContent: "center"
            },
            copyButtonText: {
                lineHeight: 40,
                color: colors.pure
            },
            deleteButton: {
                height: 40,
                backgroundColor: colors.red,
                borderRadius: 2,
                flexDirection:"column",
                alignItems: "center",
                justifyContent: "center"
            },
            deleteButtonText: {
                lineHeight: 40,
                color: colors.primary
            },
            button: {
                height: 50,
                borderStyle: "solid",
                flexDirection: "row",
                alignItems: "center",
                justifyContent: "center",
                borderWidth: 1,
                borderColor: colors.deep,
                backgroundColor: colors.deep,
                borderRadius: 5,
                maxWidth: 150
            },
            bottomButtonText: {
                fontSize: 16,
                color: colors.textDefault
            },
            LabelButton: {
                padding: 4,
                paddingLeft: 10,
                paddingRight: 10,
                borderRadius: 16,
                backgroundColor: colors.primary,
                alignSelf: 'flex-start'
            },
            LabelButtonText: {
                color: colors.white,
                fontSize: 14
            },
            NextButton: {
                flexDirection: "row",
                justifyContent: "center"
            },
            PrevButton: {
                flexDirection: "row",
                justifyContent: "center"
            },
            NextButtonText: {
                marginRight: 10
            },
            PrevButtonText: {
                marginLeft: 10
            },
            SendButton: {
                flexDirection: "row",
                justifyContent: "center"
            },
            SendButtonText: {
                marginRight: 10
            },
            SendButtonIcon: {
                transform: [{ rotate: '-90deg'}]
            },
        })
    }
})
  



export interface LabelButtonProps {
    style?: StyleProp<ViewStyle>
    textStyle?: StyleProp<TextStyle>
    text: string
}
@observer
export class LabelButton extends Component<LabelButtonProps & ButtonProps> {

    constructor(props: LabelButtonProps & ButtonProps) {
        super(props)
    }

    render() {
        const { styles } = obStyles
        return (
            <TouchableView
                style={[styles.LabelButton, this.props.style]}
                onPress={this.props.onPress}
            >
                <Text style={[styles.LabelButtonText, this.props.textStyle]}>{this.props.text}</Text>
            </TouchableView>
        );
    }
}





export interface NextButtonProps {
    style?: StyleProp<ViewStyle>
    textStyle?: StyleProp<TextStyle>
    iconSize?: number
    iconColor?: string
}
@observer
export class NextButton extends Component<NextButtonProps & ButtonProps> {

    constructor(props: NextButtonProps & ButtonProps) {
        super(props)
    }

    render() {
        const { styles } = obStyles
        return (
            <TouchableView
                style={[styles.NextButton, this.props.style]}
                onPress={this.props.onPress}
            >
                <Text style={[styles.NextButtonText, {color: this.props.onPress ? colors.primary : colors.border}, this.props.textStyle]}>{i18n.t(LANGUAGE_KEYS.NEXT)}</Text>
                <FontAwesome5 name={"chevron-right"} size={this.props.iconSize || 16} style={styles.IconButtonIcon} color={this.props.iconColor || this.props.onPress ? colors.primary : colors.border}></FontAwesome5>
            </TouchableView>
        );
    }
}

export interface PrevButtonProps {
    style?: StyleProp<ViewStyle>
    textStyle?: StyleProp<TextStyle>
    iconSize?: number
    iconColor?: string
}
@observer
export class PrevButton extends Component<PrevButtonProps & ButtonProps> {

    constructor(props: PrevButtonProps & ButtonProps) {
        super(props)
    }

    render() {
        const { styles } = obStyles
        return (
            <TouchableView
                style={[styles.PrevButton, this.props.style]}
                onPress={this.props.onPress}
            >
                <FontAwesome5 name={"chevron-left"} size={this.props.iconSize || 16} style={styles.IconButtonIcon} color={this.props.iconColor || this.props.onPress ? colors.primary : colors.border}></FontAwesome5>
                <Text style={[styles.PrevButtonText, {color: this.props.onPress ? colors.primary : colors.border}, this.props.textStyle]}>{i18n.t(LANGUAGE_KEYS.PREV)}</Text>
            </TouchableView>
        );
    }
}

export interface DoneButtonProps {
    style?: StyleProp<ViewStyle>
    textStyle?: StyleProp<TextStyle>
    iconSize?: number
    iconColor?: string
    name?: string
}
@observer
export class DoneButton extends Component<DoneButtonProps & ButtonProps> {

    constructor(props: DoneButtonProps & ButtonProps) {
        super(props)
    }

    render() {
        const { styles } = obStyles
        return (
            <TouchableView
                style={[styles.NextButton, this.props.style]}
                onPress={this.props.onPress}
            >
                <Text style={[styles.NextButtonText, {color: this.props.onPress ? colors.primary : colors.border}, this.props.textStyle]}>{this.props.name || i18n.t(LANGUAGE_KEYS.DONE)}</Text>
                <FontAwesome5 name={"chevron-right"} size={this.props.iconSize || 16} style={styles.IconButtonIcon} color={this.props.iconColor || this.props.onPress ? colors.primary : colors.border}></FontAwesome5>
            </TouchableView>
        );
    }
}

export interface SendButtonProps {
    style?: StyleProp<ViewStyle>
    textStyle?: StyleProp<TextStyle>
    iconSize?: number
    iconColor?: string
    name?: string
}
@observer
export class SendButton extends Component<SendButtonProps & ButtonProps> {

    constructor(props: SendButtonProps & ButtonProps) {
        super(props)
    }

    render() {
        const { styles } = obStyles
        return (
            <TouchableView
                style={[styles.SendButton, this.props.style]}
                onPress={this.props.onPress}
            >
                <Text style={[styles.SendButtonText, {color: this.props.onPress ? colors.primary : colors.border}, this.props.textStyle]}>{this.props.name || i18n.t(LANGUAGE_KEYS.TX_SEND)}</Text>
                <MaterialCommunityIcons name={"send"} size={this.props.iconSize || 16} style={styles.SendButtonIcon} color={this.props.iconColor || this.props.onPress ? colors.primary : colors.border}></MaterialCommunityIcons>
            </TouchableView>
        );
    }
}