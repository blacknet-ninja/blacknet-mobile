import { observable } from "mobx"
import { Observer, observer } from "mobx-react"
import { Animated, StyleSheet, Text, View } from "react-native"
import { Header, StackHeaderProps} from '@react-navigation/stack'
import { Component } from "react"


export const MyHeader = observer((props: StackHeaderProps): JSX.Element => {
    const {
        scene,
        previous,
        layout,
        insets,
        navigation,
        styleInterpolator,
      } = props
      const { options } = scene.descriptor;
      const title =
        options.headerTitle !== undefined
          ? options.headerTitle
          : options.title !== undefined
          ? options.title
          : scene.route.name;
    return (
        <Animated.View
        //   style={[options.headerStyle]}
        >
            <Header
                {...props} 
            />
            {/* <View>
                <Text>77777</Text>
            </View> */}
        </Animated.View>
    );
})
