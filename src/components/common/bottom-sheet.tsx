/**
 * BottomSheet
 * @file BottomSheet
 * @module app/components/common/bottom-sheet
 * @author Pony Ma <https://gitlab.com/blacknet-ninja>
 */


import React, { Component, useEffect } from "react";
import { StyleSheet, View, Text, Button, TouchableOpacity, StatusBar } from 'react-native'
import RBSheet, { RBSheetProps } from "react-native-raw-bottom-sheet";
import colors from '@app/style/colors'
import { IChildrenProps } from '@app/types/props'
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5'
import RootSiblings from 'react-native-root-siblings';
import ListTitle from '@app/components/common/list-title'
import { Remind } from './remind'
import { languageMaps, TLanguage } from '@app/services/i18n';
import { LANGUAGES, LANGUAGE_KEYS } from '@app/constants/language';
import i18n from '@app/services/i18n'
import { observer } from "mobx-react";
import { observable } from "mobx";
import { IS_ANDROID } from "@app/config";
import { CURRENCY } from "@app/stores/option";
import BLNListItem from "../ui/BLNListItem";
import sizes from "@app/style/sizes";

export interface BottomSheetProps {
    visible?: boolean
    onClose?(): void
    onOpen?(): void
    height?: number
}

@observer
export default class BottomSheet extends React.PureComponent<BottomSheetProps&IChildrenProps> {

    private RBSheet: RBSheet | undefined;

    componentDidMount(){
        if(this.props.visible){
            this.RBSheet?.open()
        } else {
            this.RBSheet?.close()
        }
    }

    render() {
        return (
            <RBSheet
                animationType={"fade"}
                ref={(ref:any) => this.RBSheet = ref}
                closeOnDragDown
                onOpen={()=>{
                    if(IS_ANDROID){
                        StatusBar.setBackgroundColor('rgba(0,0,0, 0.5)')
                    }
                    this.props.onOpen && this.props.onOpen()
                }}
                onClose={()=>{
                    if(IS_ANDROID){
                        StatusBar.setBackgroundColor('transparent')
                    }
                    this.props.onClose && this.props.onClose()
                }}
                customStyles={{
                    container: {
                        borderTopLeftRadius: 10,
                        borderTopRightRadius: 10,
                        backgroundColor: colors.background
                    }
                }}
                height={this.props.height}
            >
                {this.props.children}
            </RBSheet>
        );
    }

    static importMnemonic(){
        const { styles } = obStyles
        const rsb = new RootSiblings(<BottomSheet 
            onClose={()=>rsb.destroy()}
            visible={true}>
            <View style={styles.importMnemonicContainer}>
                <ListTitle 
                    leading={
                        <View style={styles.importMnemonicLeading}>
                            <FontAwesome5
                            name="user"
                            style={styles.importMnemonicIcon}
                            />
                        </View>
                    }
                    title={
                    <Text style={styles.importMnemonicText}>{i18n.t(LANGUAGE_KEYS.IMPORT)}</Text>
                    }
                />
            </View>
          </BottomSheet>);
        return rsb
    }

    static createAccount(press:Function){
        const { styles } = obStyles
        const rsb = new RootSiblings(<BottomSheet 
            onClose={()=>rsb.destroy()}
            height={100}
            visible={true}>
            <View style={{flex: 1, paddingBottom: sizes.safeAreaViewBottom}}>
                <BLNListItem 
                    leading={<View style={{marginLeft: 15, borderRadius: 32, width:32, height: 32, justifyContent: 'center', alignItems: 'center',backgroundColor: colors.primary}}><FontAwesome5 name="plus" size={18} color={colors.white}/></View>}
                    title={<Text style={{color: colors.primary, marginLeft: 20, fontSize: 16}}>{i18n.t(LANGUAGE_KEYS.CREATE)}</Text>}
                    onPress={()=>{
                        rsb.destroy()
                        press && press()
                    }}
                />
            </View>
          </BottomSheet>);
        return rsb
    }

    static selectReward(press:Function){
        const { styles } = obStyles
        const rsb = new RootSiblings(<BottomSheet 
            onClose={()=>rsb.destroy()}
            height={250}
            visible={true}>
            <View style={styles.importMnemonicContainer}
                // onLayout={(e)=>{
                //     console.log('e.nativeEvent.layout.height', e.nativeEvent.layout.height)
                // }}
            >
                {[5, 10, 50 ,100].map((reward, i)=>{
                    return <ListTitle 
                        key={i}
                        contentsStyle={i===0 ? {borderTopWidth: 0} : {}}
                        trailingStyle={i===0 ? {borderTopWidth: 0} : {}}
                        leadingStyle={i===0 ? {borderTopWidth: 0} : {}}
                        onPress={()=>{
                            rsb.destroy()
                            press(reward)
                        }}
                        title={
                            <Text style={styles.selectTypeTitle}>{reward} BLN</Text>
                        }
                    />
                })}
            </View>
          </BottomSheet>);
        return rsb
    }

    static selectTxType(press:Function, selected?: number){
        const txTypes = [-1, 0, 2, 3, 254];
        const txTypeNames = [
            i18n.t(LANGUAGE_KEYS.TX_ALL), 
            i18n.t(LANGUAGE_KEYS.TX_TRANSFER), 
            i18n.t(LANGUAGE_KEYS.TX_LEASE), 
            i18n.t(LANGUAGE_KEYS.TX_CANCEL_LEASE), 
            i18n.t(LANGUAGE_KEYS.TX_POS_GENERATED)
        ];
        const { styles } = obStyles
        const rsb = new RootSiblings(<BottomSheet 
            onClose={()=>rsb.destroy()}
            height={300}
            visible={true}>
            <View style={styles.importMnemonicContainer}>
                {txTypes.map((type, i)=>{
                    return <ListTitle 
                        key={i}
                        contentsStyle={i===0 ? {borderTopWidth: 0} : {}}
                        trailingStyle={i===0 ? {borderTopWidth: 0} : {}}
                        leadingStyle={i===0 ? {borderTopWidth: 0} : {}}
                        onPress={()=>{
                            rsb.destroy()
                            press(type)
                        }}
                        trailing={
                            (selected===type || (selected===undefined && type===-1)) ? <Remind /> : null
                        }
                        title={
                            <Text style={styles.selectTypeTitle}>{type === -1 ? i18n.t(LANGUAGE_KEYS.TX_ALL) : txTypeNames[i]}</Text>
                        }
                    />
                })}
            </View>
          </BottomSheet>);
        return rsb
    }

    static selectReceivedType(press:Function, selected?: number){
        const { styles } = obStyles
        const txTypes = [0, 2];
        const txTypeNames = [
            i18n.t(LANGUAGE_KEYS.TX_TRANSFER), 
            i18n.t(LANGUAGE_KEYS.TX_LEASE)
        ];
        const rsb = new RootSiblings(<BottomSheet 
            onClose={()=>rsb.destroy()}
            height={150}
            visible={true}>
            <View style={{flex: 1, paddingBottom: sizes.safeAreaViewBottom, paddingTop: 10}}>
                {txTypes.map((type, i)=>{
                    // return <ListTitle 
                    //     key={i}
                    //     contentsStyle={i===0 ? {borderTopWidth: 0} : {}}
                    //     trailingStyle={i===0 ? {borderTopWidth: 0} : {}}
                    //     leadingStyle={i===0 ? {borderTopWidth: 0} : {}}
                    //     onPress={()=>{
                    //         rsb.destroy()
                    //         press(type)
                    //     }}
                    //     trailing={
                    //         selected===type ? <Remind /> : null
                    //     }
                    //     title={
                    //         <Text style={styles.selectTypeTitle}>{txTypeNames[i]}</Text>
                    //     }
                    // />
                    return <BLNListItem 
                        title={<Text style={{color: colors.primary, textAlign: "center", fontSize: 16}}>{txTypeNames[i]}</Text>}
                        onPress={()=>{
                            rsb.destroy()
                            press && press(type)
                        }}
                    />
                })}
            </View>
          </BottomSheet>);
        return rsb
    }

    static selectLanguage(press:Function, selected?: LANGUAGES){
        const { styles } = obStyles
        const rsb = new RootSiblings(<BottomSheet 
            onClose={()=>rsb.destroy()}
            height={150}
            visible={true}>
            <View style={{flex: 1, paddingBottom: sizes.safeAreaViewBottom, paddingTop: 10}}>
                {Object.keys(languageMaps).map((language,i) =>{
                    const lang = language as TLanguage
                    // return <ListTitle 
                    //     key={language}
                    //     contentsStyle={i===0 ? {borderTopWidth: 0} : {}}
                    //     trailingStyle={i===0 ? {borderTopWidth: 0} : {}}
                    //     leadingStyle={i===0 ? {borderTopWidth: 0} : {}}
                    //     onPress={()=>{
                    //         rsb.destroy()
                    //         press(lang)
                    //     }}
                    //     trailing={
                    //         selected===lang ? <Remind /> : null
                    //     }
                    //     title={
                    //         <Text style={styles.selectTypeTitle}>{languageMaps[lang].name}</Text>
                    //     }
                    // />
                    return <BLNListItem 
                        title={<Text style={{color: colors.primary, textAlign: "center", fontSize: 16}}>{languageMaps[lang].name}</Text>}
                        onPress={()=>{
                            rsb.destroy()
                            press && press(lang)
                        }}
                    />
                })}
            </View>
          </BottomSheet>);
        return rsb
    }
    static selectCurrency(press:Function, selected?: CURRENCY){
        const { styles } = obStyles
        const rsb = new RootSiblings(<BottomSheet 
            onClose={()=>rsb.destroy()}
            height={180}
            visible={true}>
            <View style={{flex: 1, paddingBottom: sizes.safeAreaViewBottom, paddingTop: 10}}>
                {[CURRENCY.CNY, CURRENCY.USD, CURRENCY.BTC].map((currency,i) =>{
                    // return <ListTitle 
                    //     key={currency}
                    //     contentsStyle={i===0 ? {borderTopWidth: 0} : {}}
                    //     trailingStyle={i===0 ? {borderTopWidth: 0} : {}}
                    //     leadingStyle={i===0 ? {borderTopWidth: 0} : {}}
                    //     onPress={()=>{
                    //         rsb.destroy()
                    //         press(currency)
                    //     }}
                    //     trailing={
                    //         selected===currency ? <Remind /> : null
                    //     }
                    //     title={
                    //         <Text style={styles.selectTypeTitle}>{currency}</Text>
                    //     }
                    // />
                    return <BLNListItem 
                        title={<Text style={{color: colors.primary, textAlign: "center", fontSize: 16}}>{currency}</Text>}
                        onPress={()=>{
                            rsb.destroy()
                            press && press(currency)
                        }}
                    />
                })}
            </View>
          </BottomSheet>);
        return rsb
    }

    static walletMenu(press?:Function){
        const { styles } = obStyles
        const rsb = new RootSiblings(<BottomSheet 
            onClose={()=>rsb.destroy()}
            visible={true}>
                <View style={{flex: 1, paddingBottom: sizes.safeAreaViewBottom, paddingTop: 10}}>
                    <BLNListItem 
                        leading={<View style={{marginLeft: 15, borderRadius: 32, width:32, height: 32, justifyContent: 'center', alignItems: 'center',backgroundColor: colors.primary}}><FontAwesome5 name="coins" size={18} color={colors.white}/></View>}
                        title={<Text style={{color: colors.primary, marginLeft: 20, fontSize: 16}}>{i18n.t(LANGUAGE_KEYS.PORTFOLIOADD)}</Text>}
                        onPress={()=>{
                            rsb.destroy()
                            press && press(0)
                        }}
                    />
                    <BLNListItem 
                        leading={<View style={{marginLeft: 15, borderRadius: 32, width:32, height: 32, justifyContent: 'center', alignItems: 'center',backgroundColor: colors.primary}}><FontAwesome5 name="chevron-right" size={18} color={colors.white}/></View>}
                        title={<Text style={{color: colors.primary, marginLeft: 20, fontSize: 16}}>{i18n.t(LANGUAGE_KEYS.TX_LEASE)}</Text>}
                        onPress={()=>{
                            rsb.destroy()
                            press && press(1)
                        }}
                    />
                    <BLNListItem 
                        leading={<View style={{marginLeft: 15, borderRadius: 32, width:32, height: 32, justifyContent: 'center', alignItems: 'center',backgroundColor: colors.primary}}><FontAwesome5 name="chevron-left" size={18} color={colors.white}/></View>}
                        title={<Text style={{color: colors.primary, marginLeft: 20, fontSize: 16}}>{i18n.t(LANGUAGE_KEYS.TX_CANCEL_LEASE)}</Text>}
                        onPress={()=>{
                            rsb.destroy()
                            press && press(2)
                        }}
                    />
                    <BLNListItem 
                        leading={<View style={{marginLeft: 15, borderRadius: 32, width:32, height: 32, justifyContent: 'center', alignItems: 'center',backgroundColor: colors.primary}}><FontAwesome5 name="user" size={18} color={colors.white}/></View>}
                        title={<Text style={{color: colors.primary, marginLeft: 20, fontSize: 16}}>{i18n.t(LANGUAGE_KEYS.TX_SIGN_MESSAGE)}</Text>}
                        onPress={()=>{
                            rsb.destroy()
                            press && press(3)
                        }}
                    />
                    <BLNListItem 
                        leading={<View style={{marginLeft: 15, borderRadius: 32, width:32, height: 32, justifyContent: 'center', alignItems: 'center',backgroundColor: colors.primary}}><FontAwesome5 name="comment" size={18} color={colors.white}/></View>}
                        title={<Text style={{color: colors.primary, marginLeft: 20, fontSize: 16}}>{i18n.t(LANGUAGE_KEYS.TX_VERIFY_MESSAGE)}</Text>}
                        onPress={()=>{
                            rsb.destroy()
                            press && press(4)
                        }}
                    />
                </View>
          </BottomSheet>);
        return rsb
    }
}

export const MyRBSheet= observer(
    React.forwardRef((props: RBSheetProps&BottomSheetProps&IChildrenProps, ref: any) => {
        // useEffect(()=>{
        //     if(props.visible){
        //         ref?.open()
        //     }else{
        //         ref?.close()
        //     }
        // })
        return (
            <RBSheet
                ref={ref}
                animationType={"fade"}
                closeOnDragDown
                customStyles={{
                    container: {
                        borderTopLeftRadius: 10,
                        borderTopRightRadius: 10,
                        backgroundColor: colors.background
                    }
                }}
                {...props}
                onOpen={()=>{
                    if(IS_ANDROID){
                        StatusBar.setBackgroundColor('rgba(0,0,0, 0.5)')
                    }
                    props.onOpen && props.onOpen()
                }}
                onClose={()=>{
                    if(IS_ANDROID){
                        StatusBar.setBackgroundColor('transparent')
                    }
                    props.onClose && props.onClose()
                }}
                height={props.height}
            >
                {props.children}
            </RBSheet>
        )
    })
)

const obStyles = observable({
    get styles() {
        return StyleSheet.create({
            selectTypeTitle: {
                fontSize: 18,
                color: colors.textSecondary
            },
            importMnemonicContainer:{
                flex: 1,
                flexDirection: "column",
                paddingLeft: 20,
                paddingRight: 20
            },
            importMnemonicLeading: {
                width: 40,
                height: 40,
                borderRadius: 40,
                backgroundColor: colors.border,
                alignItems: "center",
                justifyContent: "center"
            },
            importMnemonicIcon:{
                fontSize: 25
            },
            importMnemonicText:{
                fontSize: 18
            }
        })
    }
})