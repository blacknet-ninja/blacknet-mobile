/**
 * Camera
 * @file 公共相机组件
 * @module app/components/common/camera
 * @author Pony Ma <https://gitlab.com/blacknet-ninja>
 */

import React from 'react'
import { observer } from 'mobx-react'
import { CameraKitCamera } from 'react-native-camera-kit';
import QRCodeScanner from 'react-native-qrcode-scanner';
import { RNCamera } from 'react-native-camera';

export const Camera = observer(
    React.forwardRef((props: any & {
        onSuccess?(data: string): void
    }, ref: any): JSX.Element => {
        return (
            // <CameraKitCamera
            //     style={{ flex: 1 }}
            //     cameraOptions={{
            //         zoomMode: 'off'
            //     }}
            //     scanBarcode={true}
            //     {...props}
            //     ref={ref}
            //     onReadCode={(e: any)=>{
            //         if(props.onSuccess){
            //             props.onSuccess(e.nativeEvent.codeStringValue)
            //         }
            //     }}
            // >
            // </CameraKitCamera>
            <QRCodeScanner
                {...props}
                ref={ref}
                onRead={(e)=>{
                    if(props.onSuccess){
                        props.onSuccess(e.data)
                    }
                }}
            />
        )
    })
)

export default Camera