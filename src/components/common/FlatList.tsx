/**
 * FlatList
 * @file FlatList
 * @module app/components/common/FlatList
 * @author Pony Ma <https://gitlab.com/blacknet-ninja>
 */

import React, { Component, forwardRef, useState } from 'react'
import { FlatListProps, FlatList as RNFlatList, View, StyleSheet } from 'react-native'
import { Observer, observer } from 'mobx-react'
import { IS_IOS } from '@app/config';
import { observable } from 'mobx';
import sizes from '@app/style/sizes';
import i18n from '@app/services/i18n';
import { LANGUAGE_KEYS } from '@app/constants/language';
import { Text } from './text';
import mixins from '@app/style/mixins';
import colors from '@app/style/colors';
import fonts from '@app/style/fonts';
import { AutoActivityIndicator } from './activity-indicator';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';

export const MyFlatList= observer(
    React.forwardRef((props: FlatListProps<any>, ref: any) => {
        let onEndReachedCalledDuringMomentum = false;
        return (
            <RNFlatList
                ref={ref}
                style={[props.style]}
                windowSize={300}
                initialNumToRender={20}
                onMomentumScrollBegin={()=>{
                    onEndReachedCalledDuringMomentum=  false
                }}
                onEndReached = {(...args) => {
                    if (!onEndReachedCalledDuringMomentum) {

                        if(props.onEndReached) props.onEndReached(...args)
    
                        onEndReachedCalledDuringMomentum = true;
                    }
                }}
                onEndReachedThreshold={IS_IOS ? 0.05 : 0.2}
                {...props}
            />
        )
    })
)

const obStyles = observable({
  get styles() {
    return StyleSheet.create({
        centerContainer: {
            justifyContent: 'center',
            alignItems: 'center',
            padding: sizes.gap
        },
        loadmoreViewContainer: {
            ...mixins.rowCenter,
            padding: sizes.goldenRatioGap
        },
        normalTitle: {
            ...fonts.base,
            color: colors.textSecondary
        },
        smallTitle: {
            ...fonts.small,
            color: colors.textSecondary
        }
    })
  }
})

export interface MyFlatListFooterProps {
    visible?: boolean
}

const myFlatListFooter_NO_MORE = (props: MyFlatListFooterProps): JSX.Element => {
    const { styles } = obStyles
    return (
        <Observer
            render={() => (
                <View style={[styles.centerContainer, styles.loadmoreViewContainer]}>
                    {props.visible ? 
                        <Text style={styles.smallTitle}>{i18n.t(LANGUAGE_KEYS.NO_MORE)}</Text>
                    : null}
                </View>
            )}
        />
    )
}
myFlatListFooter_NO_MORE.defaultProps = {
    visible: false,
} as Partial<MyFlatListFooterProps>;
export const MyFlatListFooter_NO_MORE = observer(myFlatListFooter_NO_MORE)


const myFlatListFooter_LOADING = (props: MyFlatListFooterProps): JSX.Element => {
    const { styles } = obStyles
    return (
        <Observer
            render={() => (
                <View style={[styles.centerContainer, styles.loadmoreViewContainer]}>
                    {props.visible ?
                        <>
                            <AutoActivityIndicator style={{ marginRight: sizes.gap / 4 }} />
                            <Text style={styles.smallTitle}>{i18n.t(LANGUAGE_KEYS.LOADING)}</Text>
                        </>
                    : null}
                </View>
            )}
        />
    )
}
myFlatListFooter_LOADING.defaultProps = {
    visible: true,
} as Partial<MyFlatListFooterProps>;
export const MyFlatListFooter_LOADING = observer(myFlatListFooter_LOADING)


const myFlatListFooter_LOADMORE = (props: MyFlatListFooterProps): JSX.Element => {
    const { styles } = obStyles
    return (
        <Observer
            render={() => (
                <View style={[styles.centerContainer, styles.loadmoreViewContainer]}>
                    {props.visible ?
                        <>
                            <FontAwesome5 name="chevron-down" color={colors.textSecondary} />
                            <Text style={[styles.smallTitle, { marginLeft: sizes.gap / 4 }]}>
                            {i18n.t(LANGUAGE_KEYS.LOADMORE)}
                            </Text>
                        </>
                    : null}
                </View>
            )}
        />
    )
}
myFlatListFooter_LOADMORE.defaultProps = {
    visible: true,
} as Partial<MyFlatListFooterProps>;
export const MyFlatListFooter_LOADMORE = observer(myFlatListFooter_LOADMORE)