import Animated, { clockRunning, Easing} from 'react-native-reanimated';
import { useClock, snapPoint as snap_point, timing, useGestureHandler, spring} from 'react-native-redash';
import { State as GestureHandlerStates } from 'react-native-gesture-handler';

const setValue = (anim: Animated.Value<any>, val: any)=>{
    anim.setValue(val)
}
const value = (x:any)=>{
    return new Animated.Value(x)
}
const clock = ()=>{
    return new Animated.Clock()
}
const set = (k: any, v: any)=>{
    return Animated.set(k, v)
}
const bezier = Easing.bezier
const linear = Easing.linear
const easings = {
    linear: linear,
    easeIn: bezier(0.42, 0, 1, 1),
    easeOut: bezier(0, 0, 0.58, 1),
    easeInOut: bezier(0.42, 0, 0.58, 1),
    cubic: bezier(0.55, 0.055, 0.675, 0.19),
    keyboard: bezier(0.17, 0.59, 0.4, 0.77),
}
const snapPoint = (value: any, velocity: any, snapPoints: any)=>{
    return snap_point(value, velocity, snapPoints)
}
const reSpring = (config: any)=>{
    return spring(config)
}
const reTiming = (config: any)=>{
    return timing(config)
}
const useGesture = (opts: any)=>{
    let gesture = useGestureHandler(opts)
    return {
        onHandlerStateChange: gesture.onHandlerStateChange,
        onGestureEvent: gesture.onGestureEvent
    }
}

const withEasing = (params: any)=>{
    let {
        value: val,
        velocity,
        offset,
        state,
        easing = easings.easeOut,
        duration = 200,
        animationOver = value(1),
        snapPoints
    }  = params;
    let position = value(1),
    c = clock(),
    interrupted = Animated.and(
        Animated.eq(state, GestureHandlerStates.BEGAN),
        clockRunning(c)
    ),
    vel = Animated.multiply(velocity, 1.5),
    to = snapPoint(position, vel, snapPoints),
    finishAnimation = [Animated.set(offset, position), Animated.stopClock(c), set(animationOver, 1)]
    return Animated.block([
        Animated.cond(interrupted, finishAnimation),
        Animated.cond(animationOver, Animated.set(position, offset)),
        Animated.cond(
            Animated.neq(state, GestureHandlerStates.END),
            [
                Animated.set(animationOver, 0),
                Animated.set(position, Animated.add(offset, val))
            ]
        ),
        Animated.cond(
            Animated.and(
                Animated.eq(state, GestureHandlerStates.END), 
                Animated.not(animationOver)
            ),
            [
                Animated.set(position, timing({
                    clock: c,
                    easing: easing,
                    duration: duration,
                    from: position,
                    to: to
                })),
                Animated.cond(Animated.not(clockRunning(c)), finishAnimation)
            ]
        ),
        position
    ]);
}  

export default {
    ...Animated,
    bezier,
    linear,
    easings,
    Easing,
    useClock,
    useGesture,
    setValue,
    withEasing,
    reSpring,
    reTiming
}

export type AnimatedValue = typeof Animated.Value