/**
 * users service
 * @file users service
 * @module app/services/users
 * @author Pony Ma <https://gitlab.com/blacknet-ninja>
 */

import { boundMethod } from 'autobind-decorator'
import storage from '@app/services/storage'
import { IBlnScanAccount } from '@app/types/bln'
import API from '@app/services/api'
import STORAGE from '@app/constants/storage';
import { defaultAvatar } from '@app/utils/bln'

export class UsersService extends Map<String, IBlnScanAccount>{

    constructor(){
        super()
        this.init()
    }

    @boundMethod
    nickname(address?: string){
        if(!address) return undefined
        const user = this.get(address)
        if(user && user.profile){
            return user.profile.nickname || address
        }
        return address
    }

    @boundMethod
    signmessage(address?: string){
        if(!address) return undefined
        const user = this.get(address)
        if(user && user.profile){
            return user.profile.signmessage
        }
        return undefined
    }

    @boundMethod
    avatar(address?: string){
        if(!address) return undefined
        const user = this.get(address)
        if(user && user.profile){
            return user.profile.imageurl
        }
        return defaultAvatar(address)
    }

    @boundMethod
    refresh(){
        this.init()
    }

    private setCache(us: Array<IBlnScanAccount>) {
        return storage.set(STORAGE.LOCAL_FORUM_USERS, us)
    }

    private getCache(): Promise<Array<IBlnScanAccount>>{
        return storage.get<Array<IBlnScanAccount>>(STORAGE.LOCAL_FORUM_USERS).then((us)=>{
            if(!us){
                return [];
            }
            return us;
        })
    }

    private init() {
        this.getCache().then((users)=>{
            if(users.length){
                users.forEach((us)=>{
                    this.set(us.address, us)
                })
            }
            console.log('Init app cache users:', users.length)
        })
        API.blnScanUsers().then((users)=>{
            if(users.length){
                this.setCache(users)
                users.forEach((us)=>{
                    this.set(us.address, us)
                })
            }
            console.log('Init app api accounts:', users.length)
            return users
        })
    }
    
}

export default new UsersService()