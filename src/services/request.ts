/**
 * Request service
 * @file Request
 * @module app/services/request
 * @author Pony Ma <https://gitlab.com/blacknet-ninja>
 */

import axios from 'axios'
import { stringify } from 'query-string'
import { LANGUAGE_KEYS } from '@app/constants/language'
import { TRequestUrlPath, TRequestData, IRequestParams } from '@app/types/http'
import i18n from '@app/services/i18n'
import { showToast } from './toast'

//request interceptors
axios.interceptors.request.use(
  function(config) {
    // add custom header
    // config.headers.token = 'this is my token'
    return config
  },
  function(error) {
    return Promise.reject(error) // 请求出错
  }
)

//response interceptors
axios.interceptors.response.use(
  function(response) {
    return response.data
  },
  function(error) {
    return Promise.reject(error)
  }
)

// 构造参数
export const formatURL = (url: TRequestUrlPath, params?: IRequestParams): TRequestUrlPath => {
  let query = ''
  if (params && Object.keys(params).length) {
    query = url.includes('?')
      ? `&${stringify(params)}`
      : `?${stringify(params)}`
  }
  axios.CancelToken.source()
  return `${url}${query}`
}

// 请求服务
export const httpService = <T>(url: TRequestUrlPath, options: RequestInit = {}): Promise<any> => {
  const defaultOptions = {
    includeCredentials: true,
    headers: {
      'Accept': 'application/json',
      'Content-Type': 'application/json'
    }
  }
  const start = new Date().getTime()
  return Promise.race([
    fetch(url, Object.assign(defaultOptions, options))
    .catch(() => {
        Promise.reject(new Error(i18n.t(LANGUAGE_KEYS.NETWORK_ERROR)))
    }),
    delay(30000)
  ])
  .then((res)=>{
    console.log(url, new Date().getTime()-start+"ms")
    return res
  })
  .catch((err: Error) => {
    showToast(`${err.message}`)
    console.log(`url：${url}`, err.message)
    return Promise.reject(err)
  })
}

export const get = <T>(url: TRequestUrlPath, params?: IRequestParams): Promise<Response> => {
    const source = axios.CancelToken.source()
    return axios.get(url, {
        params: params,
        cancelToken: source.token
    })
}

export const post = <T>(url: TRequestUrlPath, data?: TRequestData): Promise<Response> => {
    const source = axios.CancelToken.source()
    return axios.post(url, data, {
        cancelToken: source.token
    })
}

const delay = (timeOut = 30*1000) =>{
  return new Promise((resolve,reject) =>{
      setTimeout(() =>{
          reject(new Error(i18n.t(LANGUAGE_KEYS.NETWORK_TIMEOUT)))
      }, timeOut);
  })
}

export default {
    get,
    post,
    upload: fetch
}