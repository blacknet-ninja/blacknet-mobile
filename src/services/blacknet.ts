/**
 * blacknet service
 * @file blacknet
 * @module app/services/blacknet
 * @author Pony Ma <https://gitlab.com/blacknet-ninja>
 */

import { blnnodeAPI } from '@app/config'
import { IRequestParams } from '@app/types/http';
import fetch from './request'

function getSeq(address: string, params?: IRequestParams) {
    return fetch.get(`${blnnodeAPI}/api/v1/walletdb/getsequence/${address}`, params).then((res)=>res.text());
}

export default {
    getSeq
}