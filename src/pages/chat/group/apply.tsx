/**
 * GroupApply
 * @file apply
 * @module app/pages/chat/group/apply
 * @author Pony Ma <https://gitlab.com/blacknet-ninja>
 */

import React, { Component } from 'react'
import { StyleSheet, TextInput, ScrollView, Text, View } from 'react-native'
import { action, computed, observable } from 'mobx'
import { observer } from 'mobx-react'
import colors from '@app/style/colors'
import { DoneButton } from '@app/components/ui/button'
import { accountStore } from '@app/stores/account'
import i18n from '@app/services/i18n'
import { LANGUAGE_KEYS } from '@app/constants/language'
import { showToast } from '@app/services/toast'
import Loading from '@app/components/common/loading'
import { boundMethod } from 'autobind-decorator'
import { HeaderTitle } from '@react-navigation/stack'
import { IBlnGroup } from '@app/types/bln'
import { IIndexProps } from '@app/pages/home'
import storeapi from '@app/services/storeapi'
import BLNListItem from '@app/components/ui/BLNListItem'
import Image from '@app/components/common/image'

@observer export class ChatGroupApplyScreen extends Component<IIndexProps> {

    componentDidMount(){
        this.message = ""
        this.renderTitle()
    }

    constructor(props:any){
        super(props)
        this.group = this.props.route.params.group
    }

    @boundMethod
    private renderTitle() {
        this.props.navigation.setOptions({
            headerTitle: <HeaderTitle>{`${i18n.t(LANGUAGE_KEYS.CHAT_APPLY)}`}</HeaderTitle>
        })
    }

    private group: IBlnGroup = {} as IBlnGroup

    @observable private message: string = ""

    @computed get showDone() {
        return !!this.message && this.message !== ""
    }

    @action.bound
    onchangeText(text: any) {
        this.message = text
    }

    @boundMethod
    private onPressSubmit(){
        const ld = Loading.show()
        const groupOwner = this.group.from
        const isPrivate = !!this.group.data.isPrivate
        const groupID = this.group.txid
        storeapi.applyGroup(accountStore.currentMnemonic, accountStore.currentAddress, groupOwner, isPrivate, groupID, this.message)
        .then((txid: string)=>{
            showToast(`${i18n.t(LANGUAGE_KEYS.CHAT_APPLY_SUCCESS)}`)
            this.onchangeText("")
            setTimeout(()=>{
                this.props.navigation.goBack()
            }, 1000)
        })
        .catch((err: any)=>{
            showToast(`${err}`)
        })
        .finally(()=>{
            Loading.hide(ld)
        })
    }

    render() {
        const { styles } = chatGroupApplyStyles
        return (
            <View style={{flex: 1}}>
                <ScrollView style={styles.container}>
                    <BLNListItem 
                        title={<Text>{this.group && this.group.data ? this.group.data.name : null}</Text>}
                        subtitle={<Text>{this.group && this.group.data ? this.group.data.description : null}</Text>}
                        style={{marginBottom: 15}}
                        contentsStyle={{marginLeft: 10}}
                        leading={<Image style={{width: 50, height: 50, borderRadius: 4}} source={this.group && this.group.data && this.group.data.logo ? {uri: this.group.data.logo} : require('@app/assets/images/1024.png')} />}
                    />
                    {!this.group.data.isPrivate ? null : 
                        <TextInput
                            autoFocus={true}
                            style={styles.textarea}
                            multiline={true}
                            onChangeText={this.onchangeText}
                            value={this.message}
                            numberOfLines={5}
                            textAlignVertical={'top'}
                        />
                    }
                </ScrollView>
                <View style={styles.bottom}>
                    {this.renderBottom()}
                </View>
            </View>
        )
    }

    private renderBottom(){
        return <BLNListItem 
                trailing={<DoneButton name={i18n.t(LANGUAGE_KEYS.CHAT_JOIN)} 
                onPress={this.showDone || !this.group.data.isPrivate ? this.onPressSubmit : undefined}
            />}
        />
    }
}

const chatGroupApplyStyles = observable({
    get styles() {
      return StyleSheet.create({
        bottom: {
            height: 50,
            flexDirection: "row",
            alignItems: "center",
            justifyContent: "center",
            borderTopColor: colors.border,
            borderTopWidth: 1,
            paddingLeft: 10,
            paddingRight: 10
        },
        textarea: {
            height: 150,
            borderColor: colors.border, 
            borderWidth: 1,
            padding: 10,
            color: colors.textDefault,
            marginBottom: 20
        },
        container: {
            flex: 1,
            paddingLeft: 20,
            paddingRight: 20,
            paddingTop: 40
        }
      })
    }
})