/**
 * GroupUpdate
 * @file update
 * @module app/pages/chat/group/update
 * @author Pony Ma <https://gitlab.com/blacknet-ninja>
 */

import React, { Component } from 'react'
import { StyleSheet, TextInput, ScrollView, Text, View } from 'react-native'
import { action, computed, observable } from 'mobx'
import { observer } from 'mobx-react'
import colors from '@app/style/colors'
import { DoneButton } from '@app/components/ui/button'
import { accountStore } from '@app/stores/account'
import i18n from '@app/services/i18n'
import { LANGUAGE_KEYS } from '@app/constants/language'
import { showToast } from '@app/services/toast'
import Loading from '@app/components/common/loading'
import { boundMethod } from 'autobind-decorator'
import { LabelListTitle } from '@app/components/ui/list-title'
import { HeaderTitle } from '@react-navigation/stack'
import { IIndexProps } from '@app/pages/home'
import storeapi from '@app/services/storeapi'
import group from '@app/utils/group'
import { chatGroupStore } from '../stores/group'
import BLNListItem from '@app/components/ui/BLNListItem'

@observer export class ChatGroupUpdateScreen extends Component<IIndexProps> {

    @observable private name: string = ""
    @observable private description: string = ""

    @computed get showDone() {
        if(this.props.route.params.type === 'description'){
            return !!this.description && this.description !== ""
        }
        return !!this.name && this.name !== ""
    }

    @action.bound
    onChangeName(text: any) {
        this.name = text
    }
    @action.bound
    onChangeDesc(text: any) {
        this.name = text
    }

    componentDidMount(){
        this.renderTitle()
        this.name = chatGroupStore.getGroupName(this.props.route.params.groupInfo, accountStore.currentMnemonic)
        this.description = chatGroupStore.getGroupDesc(this.props.route.params.groupInfo, accountStore.currentMnemonic)
    }

    @boundMethod
    private renderTitle() {
        this.props.navigation.setOptions({
            headerTitle: <HeaderTitle>{`${i18n.t(LANGUAGE_KEYS.MODIFY)}${i18n.t(LANGUAGE_KEYS.CHAT_INFO)}`}</HeaderTitle>
        })
    }

    @boundMethod
    private onPressSubmit(){
        const ld = Loading.show()
        const txid = this.props.route.params.address
        const isPrivate = !!this.props.route.params.isPrivate
        let groupSK = ''
        if(isPrivate){
            groupSK = group.decrypt(accountStore.currentMnemonic, accountStore.currentAddress, this.props.route.params.groupInfo.privateKey)
        }
        storeapi.updateGroup(accountStore.currentMnemonic, accountStore.currentAddress, isPrivate, txid, {
            name: this.name,
            description: this.description,
            logo: chatGroupStore.getGroupLogo(this.props.route.params.groupInfo, accountStore.currentMnemonic)
        }, groupSK)
        .then((data: any)=>{
            showToast(`${i18n.t(LANGUAGE_KEYS.SUCCESS)}`)
            chatGroupStore.updateData(txid, {
                ...this.props.route.params.groupInfo,
                ...data
            })
            setTimeout(()=>{
                this.props.navigation.goBack()
            }, 1000)
        })
        .catch((err: any)=>{
            showToast(`${err}`)
        })
        .finally(()=>{
            Loading.hide(ld)
        })
    }

    render() {
        const { styles } = chatGroupApplyStyles
        return (
            <View style={{flex:1}}>
                <ScrollView style={styles.container}>
                    {this.props.route.params.type === 'name' ? 
                        <> 
                            <LabelListTitle
                                leading={<Text style={{color:colors.textDefault}}>{i18n.t(LANGUAGE_KEYS.CHAT_GROUP_NAME)}</Text>}
                            />
                            <TextInput
                                style={styles.textarea}
                                autoFocus={true}
                                multiline={true}
                                onChangeText={this.onChangeName}
                                value={this.name}
                                numberOfLines={5}
                            />
                        </>
                    : null}
                    {this.props.route.params.type === 'description' ? 
                        <> 
                            <LabelListTitle
                                leading={<Text style={{color:colors.textDefault}}>{i18n.t(LANGUAGE_KEYS.CHAT_GROUP_DESC)}</Text>}
                            />
                            <TextInput
                                style={styles.textarea}
                                autoFocus={true}
                                multiline={true}
                                onChangeText={this.onChangeDesc}
                                value={this.description}
                                numberOfLines={5}
                            />
                        </>
                    : null}
                </ScrollView>
                <View style={styles.bottom}>
                    {this.renderBottom()}
                </View>
            </View>
        )
    }

    private renderBottom(){
        return <BLNListItem
                trailing={<DoneButton
                    name={i18n.t(LANGUAGE_KEYS.CONFIRM)}
                    onPress={this.showDone ? this.onPressSubmit : undefined}
                />}
            />
    }
}

const chatGroupApplyStyles = observable({
    get styles() {
      return StyleSheet.create({
        bottom: {
            height: 50,
            flexDirection: "row",
            alignItems: "center",
            justifyContent: "center",
            borderTopColor: colors.border,
            borderTopWidth: 1,
            paddingLeft: 10,
            paddingRight: 10
        },
        textarea: {
            height: 150,
            borderColor: colors.border, 
            borderWidth: 1,
            padding: 10,
            color: colors.textDefault,
            marginBottom: 20
        },
        container: {
            flex: 1,
            paddingLeft: 20,
            paddingRight: 20,
            paddingTop: 40
        }
      })
    }
})