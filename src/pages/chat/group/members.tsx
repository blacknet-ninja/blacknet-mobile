/**
 * GroupMembers
 * @file members
 * @module app/pages/chat/group/members
 * @author Pony Ma <https://gitlab.com/blacknet-ninja>
 */

import React, { Component } from 'react'
import { StyleSheet, View, Text } from 'react-native'
import { action, computed, observable } from 'mobx'
import { Observer, observer } from 'mobx-react'
import colors from '@app/style/colors'
import { accountStore } from '@app/stores/account'
import i18n from '@app/services/i18n'
import { LANGUAGE_KEYS } from '@app/constants/language'
import { showToast } from '@app/services/toast'
import Loading from '@app/components/common/loading'
import { boundMethod } from 'autobind-decorator'
import { HeaderTitle } from '@react-navigation/stack'
import ListTitle from '@app/components/common/list-title'
import Image from '@app/components/common/image'
import { FlatList } from 'react-native-gesture-handler'
import { IIndexProps } from '@app/pages/home'
import { chatGroupStore } from '../stores/group'
import { isFunction } from 'lodash'
import { TouchableView } from '@app/components/common/touchable-view'
import { userStore } from '@app/stores/users'
import storeapi from '@app/services/storeapi'
import { HomeRoutes } from '@app/routes'
import { MyFlatListFooter_LOADING, MyFlatListFooter_LOADMORE, MyFlatListFooter_NO_MORE } from '@app/components/common/FlatList'
import { NoResult } from '@app/components/ui/NoResult'

@observer export class ChatGroupMembersScreen extends Component<IIndexProps> {

    private unsubscribe: any;
    private showLoading: boolean = false
    @observable private isLoading: boolean = false
    private isAdmin: boolean = true
    @observable private members: string[] = []

    componentWillUnmount() {
        // unsubscribe
        isFunction(this.unsubscribe) && this.unsubscribe()
    }
    componentDidMount(){
        this.unsubscribe = this.props.navigation.addListener('focus', this.onScreenFocus)
        this.init()
    }
    onScreenFocus = () => {
        this.showLoading = false
        this.fetchMembers().finally(() => {
            this.showLoading = true
        })
    }

    @boundMethod
    private init() {
        if(this.props.route.params.group){
            const group = chatGroupStore.get(this.props.route.params.address)
            if(group){
                this.isAdmin = group.from === accountStore.currentAddress
            }
        }
        this.updateData(chatGroupStore.getMember(this.props.route.params.address))
        this.props.navigation.setOptions({
            headerTitle: <Observer render={()=>
                <HeaderTitle>{`${i18n.t(LANGUAGE_KEYS.CHAT_GROUP_MEMBER)}`}</HeaderTitle>
            } />
        })
    }

    @boundMethod
    private fetchMembers() {
        this.updateLoadingState(true)
        return chatGroupStore.refreshMembers(this.props.route.params.address).then((list)=>{
            this.updateData(list)
            return list
        })
        .finally(() => this.updateLoadingState(false))
    }

    @action
    private updateData(members: string[]){
        this.members = members
    }

    @computed
    get memberCount(){
        return this.members.length
    }

    @computed
    get listData(){
        return this.members.slice()
    }

    @computed
    private get isNoMoreData(): boolean {
        return true
    }

    @action
    private updateLoadingState(loading: boolean) {
        if (!this.showLoading) {
            return
        }
        this.isLoading = loading
    }

    @boundMethod
    private renderListFooterView(): JSX.Element | null {
        if (!this.listData.length) {
            return null
        }
        if (this.listData.length < 100 || this.isNoMoreData) {
            return (
                <MyFlatListFooter_NO_MORE />
            )
        }
        if (this.isLoading) {
            return (
                <MyFlatListFooter_LOADING />
            )
        }
        return (
            <MyFlatListFooter_LOADMORE />
        )
    }

    @boundMethod
    private renderListEmptyView(): JSX.Element | null {
        return (
            <Observer
                render={() => (
                    <NoResult />
                )}
            />
        )
    }

    render() {
        const { styles } = chatGroupMemberStyles
        return (
            <FlatList 
                style={[styles.container]}
                data={this.listData}
                ListEmptyComponent={this.renderListEmptyView}
                ListFooterComponent={this.renderListFooterView}
                refreshing={this.isLoading}
                renderItem={({item: address}) => (
                    <ListTitle
                        leadingStyle={{
                            width: 'auto',
                            height: 'auto',
                            paddingLeft: 0,
                            paddingRight: 15
                        }}
                        leading={<Image style={styles.avatar} source={{ uri: userStore.getUserImage(address) }} />}
                        title={<Text numberOfLines={1} style={{color: colors.textTitle}}>{userStore.getName(address)}</Text>}
                        trailing={this.renderOp(address)}
                    />
                )}
                keyExtractor={(item, index)=>`${item.toString()}:${index}`}
            />
        )
    }

    private renderOp(address: string){
        const { styles } = chatGroupMemberStyles
        return <View style={{flexDirection: 'row', alignItems: 'center'}}>
                <TouchableView
                    style={[styles.button]}
                    onPress={()=>{
                        this.onPressChat(address)
                    }}
                >
                    <Text>{i18n.t(LANGUAGE_KEYS.CHAT)}</Text>
                </TouchableView>
                {!this.isAdmin ? null :
                    <TouchableView
                        style={[{marginLeft: 8}, styles.button]}
                        onPress={()=>{
                            this.onPressRemove(address)
                        }}
                    >
                        <Text>{i18n.t(LANGUAGE_KEYS.CHAT_GROUP_REMOVE)}</Text>
                    </TouchableView>
                }
            </View>
    }

    @boundMethod
    private onPressRemove(userAddress: string){
        const ld = Loading.show()
        const txid = this.props.route.params.address
        const isPrivate = !!this.props.route.params.isPrivate
        storeapi.adminRemoveGroup(accountStore.currentMnemonic, accountStore.currentAddress, txid, isPrivate, txid, userAddress)
        .catch((err: any)=>{
            showToast(`${err}`)
        })
        .finally(()=>{
            chatGroupStore.hiddenMember(this.props.route.params.address, userAddress)
            this.updateData(chatGroupStore.getMember(this.props.route.params.address))
            Loading.hide(ld)
        })
    }
    
    @boundMethod
    private onPressChat(address: string){
        this.props.navigation.navigate(HomeRoutes.ChatMessage, { address: address, name: `${userStore.getName(address)}`, user: accountStore.currentAddress })
    }
}

const chatGroupMemberStyles = observable({
    get styles() {
      return StyleSheet.create({
        container: {
            flex: 1
        },
        listTitleIcon: {
            color: colors.textDefault,
            fontSize: 16
        },
        listTitleText: {
            color: colors.textDefault,
            marginRight: 10
        },
        button: {
            backgroundColor: colors.primary,
            paddingLeft:8,
            paddingRight: 8,
            paddingTop: 4,
            paddingBottom: 4,
            borderRadius: 4
        },
        avatar: {
            height: 50,
            width: 50,
            borderRadius: 50
        }
      })
    }
})