/**
 * GroupQrcode
 * @file qrcode
 * @module app/pages/chat/group/qrcode
 * @author Pony Ma <https://gitlab.com/blacknet-ninja>
 */

import React, { Component } from 'react'
import { StyleSheet, View } from 'react-native'
import { observable } from 'mobx'
import { Observer, observer } from 'mobx-react'
import i18n from '@app/services/i18n'
import { LANGUAGE_KEYS } from '@app/constants/language'
import { boundMethod } from 'autobind-decorator'
import { HeaderBackButton, HeaderTitle } from '@react-navigation/stack'
import { IIndexProps } from '@app/pages/home'
import QRCode from 'react-native-qrcode-svg'
import colors from '@app/style/colors'
import Image from '@app/components/common/image'
import { Text } from '@app/components/common/text'
import { ChatRoutes } from '@app/routes'

@observer export class ChatGroupQrcodeScreen extends Component<IIndexProps> {

    componentDidMount(){
        this.renderTitle()
    }

    @boundMethod
    private renderTitle() {
        this.props.navigation.setOptions({
            headerTitle: <HeaderTitle>{`${i18n.t(LANGUAGE_KEYS.GROUP)} ${i18n.t(LANGUAGE_KEYS.QRCODE)}`}</HeaderTitle>,
            headerLeft: (props: any) => (
                <Observer render={()=>
                    <HeaderBackButton
                        {...props}
                        label={i18n.t(LANGUAGE_KEYS.BACK)}
                        onPress={() => {
                            if(this.props.route.params.isBack){
                                this.props.navigation.goBack();
                            } else {
                                this.props.navigation.replace(ChatRoutes.Message, this.props.route.params)
                            }
                        }}
                    />
                } />
            ),
            headerStyle: {
                backgroundColor: colors.primary,
                borderBottomWidth: 0,
                shadowOffset: {width: 0, height: 0},
                elevation: 0
            }
        })
    }

    render() {
        const { styles } = chatGroupQrcodeStyles
        console.log("this.props.route.params", `groupid: ${this.props.route.params.txid}?isPrivate=${!!this.props.route.params.isPrivate}`)
        return (
            <View style={styles.container}>
                <View style={styles.content}>
                    <View style={styles.headerView}>
                        {!this.props.route.params.logo ? null :
                            <View style={styles.logoView}>
                                <Image style={styles.logo} source={{ uri: this.props.route.params.logo}} />
                            </View>
                        }   
                        {!this.props.route.params.name ? null :
                            <View style={styles.titleView}>
                                <Text style={{fontSize: 24}}>{this.props.route.params.name}</Text>
                            </View>
                        }
                    </View>
                    <QRCode
                        value={`groupid: ${this.props.route.params.txid}?isPrivate=${!!this.props.route.params.isPrivate}`}
                        // logo={require('@app/assets/images/1024.png')}
                        // logoSize={50}
                        size={300}
                        logoBackgroundColor="transparent"
                    />
                </View>
            </View>
        )
    }
}

const chatGroupQrcodeStyles = observable({
    get styles() {
      return StyleSheet.create({
        container: {
            flex: 1,
            justifyContent: 'center',
            alignItems: 'center',
            backgroundColor: colors.headerBGColor
        },
        content: {
            backgroundColor: colors.white,
            padding: 30,
            borderRadius: 6,
            marginTop: -60
        },
        titleView: {
            marginBottom: 30
        },
        logoView: {
            marginRight: 10,
            marginBottom: 30
        },
        headerView: {
            justifyContent: 'flex-start',
            flexDirection: 'row',
            alignItems: 'center'
        },
        logo: {
            height: 60,
            width: 60
        }
      })
    }
})
