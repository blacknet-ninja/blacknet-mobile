/**
 * group
 * @file group
 * @module app/pages/chat/group
 * @author Pony Ma <https://gitlab.com/blacknet-ninja>
 */

import React, { Component } from 'react'
import { StyleSheet, View, TextInput, ScrollView, Text } from 'react-native'
import { action, computed, observable } from 'mobx'
import { Observer, observer } from 'mobx-react'
import { IPageProps } from '@app/types/props'
import colors from '@app/style/colors'
import { DeleteButton, DoneButton } from '@app/components/ui/button'
import { accountStore } from '@app/stores/account'
import i18n from '@app/services/i18n'
import { LANGUAGE_KEYS } from '@app/constants/language'
import { showToast } from '@app/services/toast'
import CheckBox from 'react-native-check-box'
import API from '@app/services/api'
import Loading from '@app/components/common/loading'
import { boundMethod } from 'autobind-decorator'
import { LabelListTitle } from '@app/components/ui/list-title'
import { HeaderTitle } from '@react-navigation/stack'
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5'
import MaterialIcons from 'react-native-vector-icons/MaterialIcons'
import ListTitle from '@app/components/common/list-title'
import Image from '@app/components/common/image'
import { FlatList } from 'react-native-gesture-handler'
import { defaultAvatar } from '@app/utils/bln'
import sizes from '@app/style/sizes'
import storeapi from '@app/services/storeapi'
import { ChatRoutes, GlobalRoutes, HomeRoutes } from '@app/routes'
import { isFunction } from 'lodash'
import { TouchableView } from '@app/components/common/touchable-view'
import { chatGroupStore } from './stores/group'
import ImagePicker from "react-native-image-crop-picker";
import { chatStore } from './stores/chat'
import group from '@app/utils/group'
import { IBlnGroup } from '@app/types/bln'
import BLNListItem from '@app/components/ui/BLNListItem'

class FormStore {
    @observable isChecked: boolean = false
    @observable desc: string = ""
    @observable name: string = ""
    @observable logo: string = ""

    @action.bound
    changeChecked(checked?: boolean) {
        this.isChecked = !this.isChecked
    }
    @action.bound
    changeDesc(v: string) {
        this.desc = v
    }
    @action.bound
    changeName(v: string) {
        this.name = v
    }
    @action.bound
    changeLogo(v: string) {
        this.logo = v
    }
    @action.bound
    reset() {
        this.desc = ""
        this.name = ""
        this.logo = ""
        this.isChecked = false
    }
    @computed
    get verify(): boolean{
        return this.desc != undefined && this.desc != '' && this.name != undefined && this.name != '' && this.logo != undefined && this.logo != ''
    }
    @computed
    get data(): any{
        return {
            name: this.name,
            description: this.desc,
            logo: this.logo,
            isPrivate: this.isChecked
        }
    }
}
export const formStore = new FormStore()

export interface IIndexProps extends IPageProps {}

@observer export class CreateChatGroupScreen extends Component<IIndexProps> {

    componentDidMount(){
        formStore.reset()
    }

    static getPageScreenOptions = ({ navigation }: any) => {
        return {
            title: i18n.t(LANGUAGE_KEYS.CREATE_GROUP),
            lazy : false
        }
    }

    @boundMethod
    private onPressSubmit(){
        if(!formStore.verify){
            return
        }
        const ld = Loading.show()
        storeapi.createGroup(accountStore.currentMnemonic, accountStore.currentAddress, formStore.isChecked, formStore.data)
        .then((group: IBlnGroup)=>{
            const isPrivate = formStore.isChecked
            chatGroupStore.upsert([group])
            // go to qrcode page
            this.props.navigation.replace(ChatRoutes.GroupQrcode, {
                txid: group.txid,
                isPrivate: isPrivate,
                name: formStore.name,
                description: formStore.desc,
                logo: formStore.logo,
                address: group.txid,
                user: accountStore.currentAddress,
                group: group,
                groupInfo: group.data,
            })
        })
        .catch((err: any)=>{
            showToast(`${err}`)
        })
        .finally(()=>{
            Loading.hide(ld)
        })
    }

    render() {
        const { styles } = obStyles
        return (
            <View style={styles.container}>
                <ScrollView style={styles.list}>
                    {this.renderLogo(formStore.logo)}
                    <LabelListTitle
                        leading={<Text style={{color:colors.textTitle}}>{i18n.t(LANGUAGE_KEYS.CHAT_GROUP_NAME)}</Text>} 
                    />
                    <TextInput
                        style={styles.input}
                        multiline={true}
                        onChangeText={text => formStore.changeName(text)}
                        value={formStore.name}
                        numberOfLines={5}
                    />
                    <LabelListTitle
                        leading={<Text style={{color:colors.textTitle}}>{i18n.t(LANGUAGE_KEYS.CHAT_GROUP_DESC)}</Text>}
                    />
                    <TextInput
                        style={styles.textarea}
                        multiline={true}
                        onChangeText={text => formStore.changeDesc(text)}
                        value={formStore.desc}
                        numberOfLines={5}
                    />
                    <View style={styles.checkboxView}>
                        <CheckBox
                            style={styles.checkbox}
                            onClick={()=>{
                                formStore.changeChecked()
                            }}
                            checkBoxColor={colors.textTitle}
                            leftTextStyle={{color: colors.textTitle}}
                            rightTextStyle={{color: colors.textTitle}}
                            isChecked={formStore.isChecked}
                            rightText={i18n.t(LANGUAGE_KEYS.CHAT_GROUP_IS_PRIVATE)}
                        />
                    </View>
                </ScrollView>
                <View style={styles.bootomBar}>
                    {this.renderBottom()}
                </View>
            </View>
        )
    }

    private renderBottom(){
        return <BLNListItem 
                trailing={<DoneButton name={i18n.t(LANGUAGE_KEYS.CONFIRM)} onPress={formStore.verify ? this.onPressSubmit : undefined}/>}
            />
    }

    @boundMethod
    renderLogo(imgUrl?: string){
        return <View style={{justifyContent: 'center', alignItems: "center", marginBottom: 20}}>
            <TouchableView
                style={{position: 'relative',height: 120, width: 120, borderRadius: 120, borderColor: colors.border, borderWidth: 1}}
                onPress={this.uploadImage}
            >
                {imgUrl ? 
                    <Image
                        style={{height: 120, width: 120, borderRadius: 120}}
                        source={{ uri: imgUrl}}
                    />
                : <View style={{position: 'absolute', left: 0, right: 0, top: 0, bottom: 0, justifyContent: 'center', alignItems: "center", zIndex: 1}}>
                    <Text style={{color: colors.textTitle}}>{i18n.t(LANGUAGE_KEYS.SELECT_IMAGE)}</Text>    
                </View>}
            </TouchableView>
        </View>
    }

    @boundMethod
    uploadImage() {
        const ld = Loading.show();
        ImagePicker.openPicker({
            width: 400,
            height: 400,
            cropping: true,
        }).then(async (image) => {
            return API.uploadImageOrVideo(image)
        }).then((url) => {
            formStore.changeLogo(url)
            return url
        }).finally(() => {
            Loading.hide(ld);
        }).catch((e) => {
            // console.log()
        });
    }
}

const obStyles = observable({
    get styles() {
      return StyleSheet.create({
        checkboxView:{
            justifyContent: 'center',
            alignItems: 'center',
            paddingTop: 20,
            paddingBottom: 20
        },
        checkbox: {
            width: '100%'
        },
        input: {
            borderColor: colors.border, 
            borderWidth: 1,
            padding: 10,
            color: colors.textDefault
        },
        textarea: {
            height: 150,
            borderColor: colors.border, 
            borderWidth: 1,
            padding: 10,
            color: colors.textDefault
        },
        container: {
            flex: 1
        },
        list: {
            flex: 1,
            paddingLeft: 20,
            paddingRight: 20,
            paddingTop: 40
        },
        text: {
            color: colors.textTitle
        },
        bootomBar: {
            height: 50,
            flexDirection: "row",
            alignItems: "center",
            justifyContent: "center",
            borderTopColor: colors.border,
            borderTopWidth: 1,
            paddingLeft: 10,
            paddingRight: 10
        }
      })
    }
})

@observer export class ChatInfoScreen extends Component<IIndexProps> {

    private unsubscribe: any;
    // @observable private group: IBlnGroup | undefined
    @observable private members: string[] = []

    componentWillUnmount() {
        // unsubscribe
        isFunction(this.unsubscribe) && this.unsubscribe()
    }
    componentDidMount(){
        this.unsubscribe = this.props.navigation.addListener('focus', this.onScreenFocus)
        this.init()
    }
    onScreenFocus = () => {
        this.fetchMembers()
        chatGroupStore.refreshUpdate(this.props.route.params.address, !!this.props.route.params.isPrivate)
    }

    @computed
    get isAdmin() {
        if(this.group){
            return this.group.from === accountStore.currentAddress
        }
        return true
    }

    @computed
    get groupData() {
        return this.props.route.params.group && this.group ? {
            name: this.group.data.name,
            description: this.group.data.description,
            logo: this.group.data.logo
        } : {
            name: undefined,
            description: undefined,
            logo: undefined
        }
    }

    @computed
    get group() {
        return this.props.route.params.group ? chatGroupStore.get(this.props.route.params.address) : undefined
    }

    @boundMethod
    private init() {
        this.updateMembers(chatGroupStore.getMember(this.props.route.params.address))
        this.props.navigation.setOptions({
            headerTitle: <Observer render={()=>
                <HeaderTitle>{`${i18n.t(LANGUAGE_KEYS.CHAT_GROUP_INFO)}`}</HeaderTitle>
            } />
        })
    }

    @boundMethod
    private fetchMembers() {
        return chatGroupStore.refreshMembers(this.props.route.params.address).then((list)=>{
            this.updateMembers(list)
            return list
        })
    }

    @action
    private updateMembers(members: string[]){
        this.members = members
    }

    @computed
    get listData(){
        const data = this.members.slice(0, this.isAdmin ? 23 : 24)
        let op = []
        if(this.isAdmin){
            op = ["plus", "minus"]
        }else{
            op = ["plus"]
        }
        return [...data, ...op]
    }

    @boundMethod
    private onPressAvatar(address?: string){
        if(address){
            this.props.navigation.navigate(HomeRoutes.ExplorerDetail, {
                type: "account",
                id: address
            })
        }
    }

    render() {
        const { styles } = chatStyles
        const width = sizes.screen.width / 5
        return (
            <FlatList 
                style={[styles.container]}
                data={this.listData}
                renderItem={({item}) => (
                    <View style={[{
                        width: width,
                        justifyContent: 'center',
                        alignItems: 'center'
                    }]}>
                        {item === "plus" || item === "minus" ? 
                            item === "plus" ? 
                                <TouchableView
                                    onPress={()=>{
                                        this.props.navigation.navigate(ChatRoutes.GroupQrcode, {
                                            txid: this.props.route.params.address,
                                            ...this.props.route.params,
                                            ...this.props.route.params.groupInfo,
                                            ...this.groupData,
                                            isBack: true
                                        })
                                    }}
                                    style={{
                                        margin: 10,
                                        width: width - 20,
                                        height: width - 20,
                                        justifyContent: "center",
                                        alignItems: "center"
                                    }}
                                >
                                    <FontAwesome5
                                        color={colors.textDefault}
                                        name="plus"
                                        size={35}
                                    />
                                </TouchableView> 
                            :
                                <TouchableView
                                    onPress={()=>{
                                        this.props.navigation.navigate(ChatRoutes.GroupMember, {...this.props.route.params, groupInfo: this.group?.data})
                                    }}
                                    style={{
                                        margin: 10,
                                        width: width - 20,
                                        height: width - 20,
                                        justifyContent: "center",
                                        alignItems: "center"
                                    }}
                                >
                                    <FontAwesome5
                                        color={colors.textDefault}
                                        name="minus"
                                        size={35}
                                    />
                                </TouchableView>
                        :
                            <TouchableView
                                onPress={()=>{
                                    this.onPressAvatar(item)
                                }}
                                style={{
                                    margin: 10,
                                    width: width - 20,
                                    height: width - 20,
                                    justifyContent: "center",
                                    alignItems: "center"
                                }}
                            >
                                <Image
                                    style={{
                                        margin: 10,
                                        width: width - 20,
                                        height: width - 20,
                                        borderRadius: width - 20
                                    }}
                                    source={{uri: defaultAvatar(item)}}
                                />
                            </TouchableView>
                        }
                    </View>
                )}
                keyExtractor={(item)=>item.toString()}
                numColumns={5}
                ListFooterComponent={()=>(
                    <View>
                        {this.renderGroupName()}
                        {this.renderGroupDesc()}
                        {this.renderGroupQrcode()}
                        {this.renderGroupApply()}
                        {this.renderGroupQuite()}
                    </View>
                )}
                ListHeaderComponent={()=>
                    <View>
                        {this.renderGroupLogo()}
                    </View>
                }
            />
        )
    }

    @boundMethod
    renderGroupLogo(imgUrl?: string){
        const { styles } = chatStyles
        imgUrl = this.groupData.logo
        return <View style={[styles.groupLogoView, {justifyContent: 'center', alignItems: "center", marginBottom: 20}]}>
            <TouchableView
                style={{position: 'relative',height: 120, width: 120, borderRadius: 120, borderColor: colors.border, borderWidth: 1}}
                onPress={this.isAdmin ? this.uploadImage : undefined}
            >
                {imgUrl ? 
                    <Image
                        style={{height: 120, width: 120, borderRadius: 120}}
                        source={{ uri: imgUrl}}
                    />
                : this.isAdmin ? <View style={{position: 'absolute', left: 0, right: 0, top: 0, bottom: 0, justifyContent: 'center', alignItems: "center", zIndex: 1}}>
                    <Text style={{color: colors.textDefault}}>{i18n.t(LANGUAGE_KEYS.SELECT_IMAGE)}</Text>    
                </View> : null }
            </TouchableView>
        </View>
    }

    @boundMethod
    uploadImage() {
        const ld = Loading.show();
        ImagePicker.openPicker({
            width: 400,
            height: 400,
            cropping: true,
        })
        .then(async (image) => {
            return API.uploadImageOrVideo(image)
        }).then((url) => {
            return this.onUpdateLogo(url)
        }).finally(() => {
            Loading.hide(ld);
        }).catch((e) => {
            // console.log(e)
        });
    }

    @boundMethod
    private onUpdateLogo(uri: string){
        const txid = this.props.route.params.address
        const isPrivate = !!this.props.route.params.isPrivate
        let groupSK = ''
        if(isPrivate){
            groupSK = group.decrypt(accountStore.currentMnemonic, accountStore.currentAddress, this.props.route.params.groupInfo.privateKey)
        }
        return storeapi.updateGroup(accountStore.currentMnemonic, accountStore.currentAddress, isPrivate, txid, {
            ...this.groupData,
            logo: uri
        }, groupSK)
        .then((data: any)=>{
            showToast(`${i18n.t(LANGUAGE_KEYS.SUCCESS)}`)
            chatGroupStore.updateData(txid, {
                ...this.props.route.params.groupInfo,
                ...data
            })
            return data
        })
    }
    
    renderGroupName(){
        const { styles } = chatStyles
        return <View style={styles.groupNameView}>
            <ListTitle
                style={[]}
                leading={
                    <View>
                        <MaterialIcons
                            name="group"
                            style={styles.listTitleIcon}
                        />
                    </View>
                }
                title={
                    <Text style={styles.listTitleText} numberOfLines={1}>{i18n.t(LANGUAGE_KEYS.CHAT_GROUP_NAME)}</Text>
                }
                trailing={
                    <ListRow name={this.groupData.name}/>
                }
                onPress={this.isAdmin ? () => {
                    this.props.navigation.navigate(GlobalRoutes.ChatGroupUpdate, {type: "name", ...this.props.route.params, groupInfo: this.group?.data})
                } : undefined}
            />
        </View>
    }
    renderGroupDesc(){
        const { styles } = chatStyles
        
        return <View style={styles.groupDescView}>
            <ListTitle
                style={[]}
                leading={
                    <View>
                        <MaterialIcons
                            name="description"
                            style={styles.listTitleIcon}
                        />
                    </View>
                }
                title={
                    <Text style={styles.listTitleText}>{i18n.t(LANGUAGE_KEYS.CHAT_GROUP_DESC)}</Text>
                }
                trailing={
                    <ListRow name={this.groupData.description}/>
                }
                onPress={this.isAdmin ? () => {
                    this.props.navigation.navigate(GlobalRoutes.ChatGroupUpdate, {type: "description", ...this.props.route.params, groupInfo: this.group?.data})
                } : undefined}
            />
        </View>
    }
    renderGroupApply(){
        const { styles } = chatStyles
        if(!this.isAdmin){
            return null
        }
        return <View style={styles.groupApplyView}>
            <ListTitle
                style={[]}
                leading={
                    <View>
                        <FontAwesome5
                            name="list"
                            style={styles.listTitleIcon}
                        />
                    </View>
                }
                title={
                    <Text style={styles.listTitleText}>{i18n.t(LANGUAGE_KEYS.CHAT_GROUP_APPLY_LIST)}</Text>
                }
                trailing={
                    <ListRow />
                }
                onPress={this.isAdmin ? () => {
                    this.props.navigation.navigate(ChatRoutes.GroupApplyList, {...this.props.route.params, groupInfo: this.group?.data})
                } : undefined}
            />
        </View>
    }

    renderGroupQrcode(){
        const { styles } = chatStyles
        return <View style={styles.groupQrcodeView}>
            <ListTitle
                style={[]}
                leading={
                    <View>
                        <FontAwesome5
                            name="qrcode"
                            style={styles.listTitleIcon}
                        />
                    </View>
                }
                title={
                    <Text style={styles.listTitleText}>{i18n.t(LANGUAGE_KEYS.QRCODE)}</Text>
                }
                trailing={
                    <ListRow />
                }
                onPress={() => {
                    this.props.navigation.navigate(ChatRoutes.GroupQrcode, {
                        txid: this.props.route.params.address,
                        ...this.props.route.params,
                        ...this.props.route.params.groupInfo,
                        ...this.groupData,
                        isBack: true
                    })
                }}
            />
        </View>
    }
    
    renderGroupQuite(){
        const { styles } = chatStyles
        return <View style={styles.groupQuiteView}>
            <DeleteButton
                text={i18n.t(LANGUAGE_KEYS.QUIT)}
                onPress={this.onPressQuite}
            />
        </View>
    }

    @boundMethod
    private onPressQuite(){
        const ld = Loading.show()
        const txid = this.props.route.params.address
        const isPrivate = !!this.props.route.params.isPrivate
        storeapi.quiteGroup(accountStore.currentMnemonic, accountStore.currentAddress, txid, isPrivate, txid)
        .then(()=>{
            chatStore.hideList(accountStore.currentAddress, this.props.route.params.address)
            this.props.navigation.popToTop();
        })
        .catch((err: any)=>{
            showToast(`${err}`)
        })
        .finally(()=>{
            Loading.hide(ld)
        })
    }
}

const ListRow = observer((props: any & {
    name?: string
}): JSX.Element => {
    return (
        <TouchableView
            {...props}
            style={[
                {
                    flexDirection: 'row',
                    alignItems: 'center',
                    justifyContent: 'center'
                },
                props.style
            ]}
        >
            {props.name ? <Text style={{marginRight: 4, color: colors.textDefault}}>{props.name}</Text> : null}
            <FontAwesome5
                color={colors.textDefault}
                name="chevron-right"
            />
        </TouchableView>
    )
})

const chatStyles = observable({
    get styles() {
      return StyleSheet.create({
        container: {
            flex: 1
        },
        groupLogo: {
            height: 60,
            width: 60,
            borderRadius: 60
        },
        groupLogoView: {
            marginTop: 20,
            marginBottom: 20,
            justifyContent: 'center',
            alignItems: 'center'
        },
        avatarView: {
            marginBottom: 20
        },
        groupNameView: {

        },
        groupDescView: {

        },
        groupApplyView: {

        },
        groupQrcodeView: {

        },
        groupQuiteView: {
            marginTop: 20,
            paddingLeft: 15,
            paddingRight: 15
        },
        listTitleIcon: {
            color: colors.textDefault,
            fontSize: 16
        },
        listTitleText: {
            color: colors.textDefault,
            marginRight: 10
        }
      })
    }
})
