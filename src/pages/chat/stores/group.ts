/**
 * chat group store
 * @file chat group store
 * @module app/pages/explorer/stores/group
 * @author Pony Ma <https://gitlab.com/blacknet-ninja>
 */

import { observable, action, computed } from 'mobx'
import { boundMethod } from 'autobind-decorator'
import storage from '@app/services/storage'
import { STORAGE } from '@app/constants/storage'
import { IBlnGroup, IBlnGroupData } from '@app/types/bln'
import { verifyAccount } from '@app/utils/bln'
import { IRequestParams, ITxResultPaginate } from '@app/types/http'
import storeapi, { GroupOpType } from '@app/services/storeapi'
import Blacknetjs from 'blacknetjs';
import chat from '@app/utils/chat'
import { accountStore } from '@app/stores/account'

export class ChatGroupStoreV2 {

    @observable datas: Map<string, IBlnGroup> = new Map()
    @observable members: Map<string, {[key:string]: any}[]> = new Map()

    constructor() {
        this.init()
    }

    @action.bound
    private init() {
        // init cache
        storage.get<Array<IBlnGroup>>(STORAGE.LOCAL_CHAT_GROUPS)
        .then(lists => {
            if (lists && lists.length) {
                lists.forEach((group) => {
                    const v = this.datas.get(group.txid)
                    if (v) {
                        this.datas.set(group.txid, { ...v, ...group })
                    } else {
                        this.datas.set(group.txid, group)
                    }
                })
            }
            return lists
        }).finally(this.refresh)
        // member 
        storage.get<Array<any>>(STORAGE.LOCAL_CHAT_GROUP_MEMBERS)
        .then(lists => {
            if (lists && lists.length) {
                lists.forEach((group) => {
                    this.members.set(group[0], group[1])
                })
            }
            return lists
        })
        // storage.remove(STORAGE.LOCAL_CHAT_GROUP_MEMBERS)
    }

    preload(): Promise<any>{
        if(this.datas.size){
            return Promise.resolve()
        }
        return this.refresh()
    }

    @computed
    get lists(){
        return Array.from(this.datas, ([name, value]) => value)
    }

    @action.bound
    upsert(lists: Array<IBlnGroup>) {
        lists.forEach((group) => {
            const v = this.datas.get(group.txid)
            if (v) {
                // group = v.time < group.time ? { ...v, ...group } : { ...group, ...v }
                // group = { ...v, ...group, data: v.data.decrypted ? {...group.data, ...v.data} : {...v.data, ...group.data}}
                group = { ...v, ...group }
            }
            if(group.data.isPrivate){
                group.data.name = this.getGroupName(group.data, accountStore.currentMnemonic)
                group.data.logo = this.getGroupLogo(group.data, accountStore.currentMnemonic)
                group.data.description = this.getGroupDesc(group.data, accountStore.currentMnemonic)
                group.data.decrypted = true
            }
            this.datas.set(group.txid, group)
        })
        return this.save()
    }

    @boundMethod
    get(txid: string){
        if(verifyAccount(txid)){
            let group
            this.datas.forEach((g)=>{
                if(g.to === txid){
                    group = g
                }
            })
            return group
        }
        return this.datas.get(txid)
    }

    @action.bound
    set(txid: string, group: IBlnGroup){
        const v = this.datas.get(txid)
        if (v) {
            return this.datas.set(txid, { ...v, ...group })
        } else {
            return this.datas.set(txid, group)
        }
    }

    @action.bound
    updateData(txid: string, group: IBlnGroupData, time?: string){
        const v = this.datas.get(txid)
        time = time || chat.timeNowStr()
        if (v && time > v.time) {
            group = {...v.data, ...group}
            if(group.isPrivate){
                group.name = this.getGroupName(group, accountStore.currentMnemonic)
                group.logo = this.getGroupLogo(group, accountStore.currentMnemonic)
                group.description = this.getGroupDesc(group, accountStore.currentMnemonic)
                group.decrypted = true
            }
            this.datas.set(txid, { ...v, data: group, time: time })
        }
        return this.save()
    }

    @action.bound
    refreshUpdate(txid: string, isPrivate?: boolean){
        return storeapi.fetchGroupOption(GroupOpType.GroupUpdate, {
            "data.txid": txid
        }).then((json)=>{
            if(json && json.txns && json.txns.length){
                return json.txns[0]
            }
            return null
        })
        .then((group)=>{
            if(group && group.data && group.data.txid){
                if(isPrivate){
                    group.data.isPrivate = isPrivate ? 1 : 0
                }
                this.updateData(group.data.txid, group.data as IBlnGroupData, group.time)
            }
            return group
        })
    }

    @action.bound
    refresh(params?: IRequestParams | undefined): Promise<ITxResultPaginate<IBlnGroup[]>> {
        return storeapi.fetchGroupLists(params)
            .then((lists) => {
                if (lists && lists.txns && lists.txns.length) {
                    this.upsert(lists.txns)
                }
                return lists
            })
    }

    @boundMethod
    search(text: string): Array<IBlnGroup> {
        const reg = new RegExp(text, 'ig');
        const arr: Array<IBlnGroup> = [];
        this.datas.forEach((group) => {
            if (reg.test([group.data.name, group.data.description].join(' '))) {
                arr.push(group)
            }
        })
        return arr
    }

    private save(){
        return storage.set(STORAGE.LOCAL_CHAT_GROUPS, Array.from(this.datas, ([name, value]) => value))
    }


    @boundMethod
    getGroupName(group?: IBlnGroupData, mnemonic?: string, address?: string) {
        if(!group){
            return null
        }
        if(mnemonic && group.isPrivate){
            address = address || Blacknetjs.Address(mnemonic)
            try {
                const groupSK = Blacknetjs.Decrypt(mnemonic, address, group.privateKey);
                const groupPK = Blacknetjs.Address(groupSK)
                return Blacknetjs.Decrypt(groupSK, groupPK, group.name);
            } catch (error) {
                return null   
            }
        }
        return group.name
    }

    @boundMethod
    getGroupLogo(group?: IBlnGroupData, mnemonic?: string, address?: string) {
        if(!group){
            return null
        }
        if(mnemonic && group.isPrivate){
            address = address || Blacknetjs.Address(mnemonic)
            try {
                const groupSK = Blacknetjs.Decrypt(mnemonic, address, group.privateKey);
                const groupPK = Blacknetjs.Address(groupSK)
                return Blacknetjs.Decrypt(groupSK, groupPK, group.logo);
            } catch (error) {
                return null   
            }
        }
        return group.logo
    }
    @boundMethod
    getGroupDesc(group?: IBlnGroupData, mnemonic?: string, address?: string) {
        if(!group){
            return null
        }
        if(mnemonic && group.isPrivate){
            address = address || Blacknetjs.Address(mnemonic)
            try {
                const groupSK = Blacknetjs.Decrypt(mnemonic, address, group.privateKey);
                const groupPK = Blacknetjs.Address(groupSK)
                return Blacknetjs.Decrypt(groupSK, groupPK, group.description);
            } catch (error) {
                return null   
            }
        }
        return group.description
    }




    // group members
    @action.bound
    refreshMembers(txid: string): Promise< string[]> {
        return storeapi.blnScanChatGroupMemebers(txid)
            .then((lists) => {
                if (lists) {
                    const members: {[key:string]: any}[]= []
                    const oldMembers = this.members.get(txid)
                    lists.forEach((address)=>{
                        let index = -1
                        if(oldMembers){
                            oldMembers.forEach((m, i)=>{
                                if(m.address === address){
                                    index = i
                                }
                            })
                        }
                        if(index>-1 && oldMembers){
                            members.push({
                                ...oldMembers[index],
                                address: address
                            })
                        }else{
                            members.push({
                                address: address
                            })
                        }
                    })
                    this.setMembers(txid, members)
                }
                // return lists
                return this.getMember(txid)
            })
    }
    @boundMethod
    getMember(txid: string){
        const members: string[] = []
        const lists = this.members.get(txid)
        if(lists){
            lists.forEach((m)=>{
                if(!m.hidden && m.address){
                    members.push(m.address)
                }
            })
        }
        return members
    }

    @boundMethod
    hiddenMember(txid: string, address: string){
        const lists = this.members.get(txid)
        if(lists){
            const members: {[key:string]: any}[] = []
            lists.forEach((m)=>{
                if(m.address === address){
                    members.push({...m, hidden: true})
                } else {
                    members.push(m)
                }
            })
            this.setMembers(txid, members)
        }
    }

    @action.bound
    setMembers(txid: string, m: {[key:string]: any}[]){
        this.members.set(txid, m)
        return storage.set(STORAGE.LOCAL_CHAT_GROUP_MEMBERS, Array.from(this.members, ([k,v])=>[k, v]))
    }
}

export const chatGroupStore = new ChatGroupStoreV2()
