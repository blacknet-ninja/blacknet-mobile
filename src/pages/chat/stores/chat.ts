/**
 * chant store
 * @file chat store
 * @module app/pages/explorer/stores/chat
 * @author Pony Ma <https://gitlab.com/blacknet-ninja>
 */

import { observable, action } from 'mobx'
import { boundMethod } from 'autobind-decorator'
import storage from '@app/services/storage'
import { IBlnScanMessage } from '@app/types/bln'
import storeAPI from '@app/services/storeapi'
import { accountStore } from '@app/stores/account'
import { verifyAccount } from '@app/utils/bln'
import chat, { BLNChatData, ChatDataType } from '@app/utils/chat'
import { chatKeyStore } from './key'
import { isFunction } from 'lodash'

export class ChatStoreV2 {

    @observable lists: Array<IBlnScanMessage> = []
    @observable messages: Array<IBlnScanMessage> = []
    @observable currentKey: string = ""

    @action.bound
    init(address: string) {
        // init list cache
        return this.getList(address)
        .then(lists => {
            return this.setList(lists)
        })
    }

    @boundMethod
    private listKey(address: string):string{
        return `chat:messages:${address}`;
    }

    @boundMethod
    getList(address: string): Promise<IBlnScanMessage[]>{
        return storage.get<Array<IBlnScanMessage>>(this.listKey(address)).then((txns)=>{
            if(!txns){
                return []
            }
            return txns
        })  
    }

    @action.bound
    setList(txns: Array<IBlnScanMessage>) {
        return this.lists = txns.slice()
    }

    @action.bound
    hideList(address: string, txid: string) {
        return this.getList(address).then((txns)=>{
            if(!txns){
                return 
            }
            const ntxs: IBlnScanMessage[]= []
            txns.slice().forEach((t, i)=>{
                if(t.to === txid){
                    t.hidden = true
                }
                ntxs.push(t)
            })
            return this.setListWithStorage(address, ntxs)
        })
    }

    @action.bound
    hasInList(txid: string) {
        for (let index = 0; index < this.lists.length; index++) {
            const t = this.lists[index];
            if(t.to === txid && t.decrypted){
                return t
            }
        }
        return null
    }

    @boundMethod
    inGroup(txid: string){
        let inGroup = false
        this.lists.forEach((tx)=>{
            if(!verifyAccount(tx.to) && tx.to === txid){
                inGroup = true
            }
        })
        return inGroup
    }

    @action.bound
    setListWithStorage(address: string, txns: Array<IBlnScanMessage>) {
        this.lists = txns
        return storage.set(this.listKey(address), this.lists)
    }

    @boundMethod
    upsertsList(address: string, ntxns: IBlnScanMessage[]) {
        return this.getList(address).then((txns)=>{
            if(!txns){
                return this.setListWithStorage(address, ntxns) 
            }
            for (const tx of ntxns) {
                let index = -1;
                txns.forEach((t, i)=>{
                    if(t.to === tx.to){
                        index = i
                    }
                })
                if(index>-1){
                    if(tx.decrypted){
                        txns.splice(index, 1, {...txns[index], ...tx, hidden: tx.hidden === undefined ? txns[index].hidden : tx.hidden})
                    }else if(tx.group && !tx.time){
                        txns.splice(index, 1, {...txns[index], ...tx, hidden: tx.hidden === undefined ? txns[index].hidden : tx.hidden})
                    }else if (tx.time >= txns[index].time){
                        txns.splice(index, 1, {...txns[index], ...tx, hidden: tx.hidden === undefined ? txns[index].hidden : tx.hidden})
                    }
                }else{
                    txns.push(tx)
                }
            }
            const groups: IBlnScanMessage[]= []
            txns.forEach((tx, i)=>{
                if(tx.group){
                    txns.splice(i, 1)
                    groups.push(tx)
                }
            })
            txns.sort((a, b)=>{
                return parseInt(b.time) - parseInt(a.time)      
            })
            return this.setListWithStorage(address, groups.concat(txns))
        })
    }

    @boundMethod
    private key(address: string, to: string):string{
        return `chat:messages:${address}:with:${to}`;
    }

    @action.bound
    get(address: string, to: string): Promise<IBlnScanMessage[]>{
        const currentKey = this.key(address, to)
        return storage.get<Array<IBlnScanMessage>>(this.key(address, to)).then((txns)=>{
            if(!txns){
                return []
            }
            return txns
        }).then((txns)=>{
            this.messages = txns
            this.currentKey = currentKey
            return txns
        })
    }

    @action.bound
    initMessage(address: string, to: string): Promise<IBlnScanMessage[]>{
        const currentKey = this.key(address, to)
        return storage.get<Array<IBlnScanMessage>>(currentKey).then((txns)=>{
            if(!txns){
                return []
            }
            return txns
        }).then((txns)=>{
            this.messages = txns
            this.currentKey = currentKey
            return txns
        })
    }

    hasInMessages(address: string, to: string, txid: string) {
        return new Promise((resolve, reject)=>{
            if(this.key(address, to) === this.currentKey){
                resolve(this.messages)
            }else{
                this.get(address, to).then((messages)=>{
                    resolve(messages)
                }).catch(()=>{
                    resolve([])
                })
            }
        }).then((lists: any)=>{
            for (let index = 0; index < lists.length; index++) {
                const t = lists[index];
                if(t.txid === txid && t.decrypted){
                    return t
                }
            }
            return null
        })
    }

    @boundMethod
    getOne(address: string, to: string, txid: string): Promise<IBlnScanMessage|undefined>{
        return this.get(address, to).then((txs)=>{
            let tx: IBlnScanMessage | undefined = undefined
            txs.forEach((t)=>{
                if(t.txid === txid){
                    tx = t
                }
            })
            return tx
        }).then((tx)=>{
            // 缓存没找到请求网络
            return tx
        })
    }

    @action.bound
    set(address: string, to: string, txns: Array<IBlnScanMessage>) {
        const currentKey = this.key(address, to)
        this.messages = txns
        this.currentKey = currentKey
        return storage.set(currentKey, txns)
    }

    @action.bound
    upserts(address: string, to: string, ntxns: Array<IBlnScanMessage>) {
        return this.get(address, to).then((txns)=>{
            if(!txns){
                return this.set(address, to, ntxns) 
            }
            for (const tx of ntxns) {
                let index = -1;
                txns.slice().forEach((t, i)=>{
                    if(t.txid && t.txid === tx.txid){
                        index = i
                    }
                })
                if(index>-1){
                    if(tx.decrypted){
                        txns.splice(index, 1, {...txns[index], ...tx})
                    }else if(parseInt(tx.time) >= parseInt(txns[index].time)){
                        txns.splice(index, 1, {...txns[index], ...tx})
                    }
                }else{
                    txns.push(tx)
                }
            }
            txns.sort((a, b)=>{
                return parseInt(b.time) - parseInt(a.time)
            })
            return this.set(address, to, txns)
        })
    }

    @action.bound
    replace(address: string, to: string, txid: string, tx: IBlnScanMessage) {
        return this.get(address, to).then((txns)=>{
            if(!txns){
                return this.set(address, to, [tx]) 
            }
            let index = -1;
            txns.slice().forEach((t, i)=>{
                if(t.txid && t.txid === txid){
                    index = i
                }
            })
            if(index>-1){
                if(parseInt(tx.time) >= parseInt(txns[index].time)){
                    txns.splice(index, 1, {...txns[index], ...tx})
                }
            }else{
                txns.push(tx)
            }
            return this.set(address, to, txns)
        })
    }

    @boundMethod
    refreshChatMap(callback?: any){
        return storeAPI.blnScanChatMapV2(accountStore.currentAddress)
        .then((msgs)=>{
            const promises: any[] = []
            msgs.forEach((message)=>{
                if(!chatStore.hasInList(message.to)){
                    const chatData = BLNChatData.unpack(message, accountStore.currentMnemonic, accountStore.currentAddress)
                    if(ChatDataType.Empty !== chatData.type){
                        promises.push(new Promise((resolve, reject)=>{
                            try {
                                if(chatData.isGroup){
                                    if(chatData.isPrivate){
                                        // const groupSK = chatKeyStore.getSK(accountStore.currentMnemonic, accountStore.currentAddress)
                                        chatKeyStore.getSKAsync(accountStore.currentMnemonic, accountStore.currentAddress, chatData.getTo()).then((groupSK)=>{
                                            if(groupSK){
                                                const groupPK = chat.address(groupSK)
                                                chatData.decrypt(groupSK, groupPK, accountStore.currentMnemonic, accountStore.currentAddress)
                                            }
                                            resolve(chatData)
                                        })
                                    } else {
                                        chatData.decrypt("", "", accountStore.currentMnemonic, accountStore.currentAddress)
                                        resolve(chatData)
                                    }
                                } else {
                                    if(chatData.isPrivate){
                                        if(message.from === accountStore.currentAddress){
                                            chatData.decrypt(accountStore.currentMnemonic, message.to, accountStore.currentMnemonic, accountStore.currentAddress)
                                        }else{
                                            chatData.decrypt(accountStore.currentMnemonic, message.from, accountStore.currentMnemonic, accountStore.currentAddress)
                                        }
                                    } else {
                                        chatData.decrypt("", "", accountStore.currentMnemonic, accountStore.currentAddress)
                                    }
                                    resolve(chatData)
                                }
                            } catch (error) {
                                resolve(chatData)
                            }
                        }).then(()=>{
                            message.decrypted = true
                            message.text = chatData.getText()
                            return message
                        }))
                    } else {
                        message.text = chatData.getText()
                        promises.push(Promise.resolve(message))
                    }
                }
            })
            return Promise.all(promises)
        })
        .then((json) => {
            if(callback && isFunction(callback)){
                callback(json)
            }
            return this.upsertsList(accountStore.currentAddress, json)
        })
    }
    
}


export const chatStore = new ChatStoreV2()

