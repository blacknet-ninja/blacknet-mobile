/**
 * chat apply store
 * @file chat apply store
 * @module app/pages/explorer/stores/apply
 * @author Pony Ma <https://gitlab.com/blacknet-ninja>
 */

import { observable, action, computed } from 'mobx'
import storage from '@app/services/storage'
import { STORAGE } from '@app/constants/storage'
import { IBlnScanMessage } from '@app/types/bln'
import { IRequestParams, ITxResultPaginate } from '@app/types/http'
import storeapi, { GroupOpType } from '@app/services/storeapi'

export class ChatApplyStore {
    
    @observable datas: Map<string, IBlnScanMessage[]> = new Map()

    constructor() {
        this.init()
    }

    @action.bound
    private init() {
        // init cache
        // [txid, Array<IBlnScanMessage>]
        storage.get<[string, IBlnScanMessage[]][]>(STORAGE.LOCAL_CHAT_GROUP_APPLY_LIST)
        .then(lists => {
            if (lists && lists.length) {
                lists.forEach((list) => {
                    const [txid, arr] = list
                    this.datas.set(txid, arr)
                })
            }
            return lists
        })
    }

    @computed
    get array(){
        return Array.from(this.datas)
    }

    @action.bound
    refresh(txid: string, isPrivate?: boolean, params?: IRequestParams | undefined): Promise<ITxResultPaginate<IBlnScanMessage[]>> {
        return storeapi.fetchGroupOption(isPrivate ? GroupOpType.GroupApply : GroupOpType.GroupAddPublic, {"data.txid": txid, ...params})
        .then((datas) => {
            if(datas && datas.txns){
                const res: IBlnScanMessage[] = []
                const oldLists = this.datas.get(txid)
                datas.txns.forEach((list)=>{
                    if(oldLists){
                        let index = -1
                        oldLists.forEach((m, i)=>{
                            if(list.txid === m.txid){
                                index = i
                            }
                        })
                        if(index>-1){
                            res.push({
                                ...oldLists[index],
                                ...list,
                                hidden: list.hidden !== undefined ? list.hidden : oldLists[index].hidden,
                                hiddenAgree: list.hiddenAgree !== undefined ? list.hiddenAgree : oldLists[index].hiddenAgree,
                                hiddenReject: list.hiddenReject !== undefined ? list.hiddenReject : oldLists[index].hiddenReject,
                            })
                        }else{
                            res.push(list)
                        }
                    } else {
                        res.push(list)
                    }
                })
                this.set(txid, res)
                datas.txns = res
            }
            return datas
        })
    }

    get(txid: string){
        const lists = this.datas.get(txid)
        const res: IBlnScanMessage[] = []
        if(lists){
            lists.forEach((m)=>{
                if(!m.hidden){
                    res.push(m)
                }
            })
        }
        return res
    }

    @action.bound
    set(txid: string, lists: IBlnScanMessage[]){  
        this.datas.set(txid, lists)
        return storage.set(STORAGE.LOCAL_CHAT_GROUP_APPLY_LIST, Array.from(this.datas))
    }

    @action.bound
    hidden(id: string, txid: string){
        const lists = this.datas.get(id)
        if(lists){
            const res: IBlnScanMessage[] = []
            lists.forEach((m)=>{
                if(m.txid === txid){
                    res.push({...m, hidden: true, hiddenAgree: false, hiddenReject: false})
                } else {
                    res.push(m)
                }
            })
            this.set(txid, res)
        }
    }

    @action.bound
    agree(id: string, txid: string){
        const lists = this.datas.get(id)
        if(lists){
            const res: IBlnScanMessage[] = []
            lists.forEach((m)=>{
                if(m.txid === txid){
                    res.push({...m, hiddenAgree: true, hiddenReject: false})
                } else {
                    res.push(m)
                }
            })
            this.set(id, res)
        }
    }

    @action.bound
    reject(id: string, txid: string){
        const lists = this.datas.get(id)
        if(lists){
            const res: IBlnScanMessage[] = []
            lists.forEach((m)=>{
                if(m.txid === txid){
                    res.push({...m, hiddenReject: true, hiddenAgree: false})
                } else {
                    res.push(m)
                }
            })
            this.set(id, res)
        }
    }
}

export const chatApplyStore = new ChatApplyStore()