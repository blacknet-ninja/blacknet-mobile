/**
 * Chat
 * @file chat
 * @module pages/chat/index
 * @author Pony Ma <https://gitlab.com/blacknet-ninja>
 */

import React, { Component, RefObject } from 'react'
import { StyleSheet, View, FlatList, Text, ActivityIndicator } from 'react-native'
import { boundMethod } from 'autobind-decorator'
import { observable, action, computed } from 'mobx'
import { Observer, observer } from 'mobx-react'
import { IPageProps } from '@app/types/props'
import colors from '@app/style/colors'
import sizes from '@app/style/sizes'
import fonts from '@app/style/fonts'
import mixins from '@app/style/mixins'
import { IBlnScanerURL, IBlnScanMessage } from '@app/types/bln'
import { accountStore } from '@app/stores/account'
import i18n from '@app/services/i18n'
import { LANGUAGE_KEYS } from '@app/constants/language'
import { ButtonResult } from '@app/components/ui/NoResult'
import { verifyAccount } from '@app/utils/bln'
import { getHeaderButtonStyle } from '@app/style/mixins'
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5'
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons'
import Clipboard from '@react-native-community/clipboard';
import { ChatMessageListItem } from '@app/components/ui/chat/list'
import { ChatRoutes, GlobalRoutes, HomeRoutes, SettingsRoutes } from '@app/routes'
import { chatStore } from '@app/pages/chat/stores/chat'
export type ListElement = RefObject<FlatList<IBlnScanMessage>>
export interface IIndexProps extends IPageProps { }

import {
    Menu,
    MenuProvider,
    MenuOptions,
    MenuTrigger,
    renderers,
    MenuOption,
} from 'react-native-popup-menu';
import { RowList } from '@app/components/common/list-title'
import { WithPromiseActivityIndicator } from '@app/components/common/activity-indicator'
import { DoneButton, TextButton } from '@app/components/ui/button'
const { Popover } = renderers

@observer export class ChatScreen extends Component<IIndexProps> {

    static getPageScreenOptions = ({ navigation }: any) => {
        return {
            headerStyle: {
                backgroundColor: colors.background,
                // backgroundColor: "transparent",
                borderBottomWidth: 1,
                shadowOffset: {width: 0, height: 0},
                elevation: 0
            },
            headerTitle: () => (<AutoI18nTitle
                color={colors.textDefault}
                size={18}
                i18nKey={LANGUAGE_KEYS.CHAT}
            />),
            headerRight: () => <Observer render={() => (
                <Menu renderer={Popover} rendererProps={{ placement: 'bottom' }}>
                    <MenuTrigger>
                        <FontAwesome5
                            name="plus"
                            {...getHeaderButtonStyle(18)}
                            color={colors.textTitle}
                        />
                    </MenuTrigger>
                    <MenuOptions customStyles={{
                        optionsContainer: {
                            paddingLeft: 0,
                            paddingRight: 0,
                            paddingTop: 0,
                            paddingBottom: 0,
                            flexDirection: 'column'
                        }
                    }}>
                        <MenuOption
                            onSelect={() => navigation.navigate(GlobalRoutes.ChatNewGroup)}
                            style={{
                                borderBottomWidth: 1,
                                borderBottomColor: optionStore.darkTheme ? colors.textDefault : colors.border
                            }}
                        >
                            <RowList
                                leading={<FontAwesome5
                                    name="comments"
                                    color={colors.textTitle}
                                    size={16}
                                />}
                                trailing={<Text style={{color: colors.textTitle}}>{i18n.t(LANGUAGE_KEYS.GROUP)}</Text>}
                            />
                        </MenuOption>
                        <MenuOption
                            onSelect={() => navigation.navigate(GlobalRoutes.ChatNewChat)}
                            style={{
                                borderBottomWidth: 1,
                                borderBottomColor: optionStore.darkTheme ? colors.textDefault : colors.border
                            }}
                        >
                            <RowList
                                leading={<FontAwesome5
                                    name="comment"
                                    color={colors.textTitle}
                                    size={16}
                                />}
                                trailing={<Text style={{color: colors.textTitle}}>{i18n.t(LANGUAGE_KEYS.CHAT)}</Text>}
                            />
                        </MenuOption>
                        <MenuOption
                            onSelect={() => navigation.navigate(HomeRoutes.QrScanner, {
                                callback: (data: any)=>{
                                    if(data.groupid){
                                        const group = chatGroupStore.get(data.groupid)
                                        group && navigation.navigate(GlobalRoutes.ChatApplyGroup, {
                                            group: group,
                                            groupid: data.groupid
                                        })
                                    }
                                }
                            })}
                            style={{
                                borderBottomWidth: 1,
                                borderBottomColor: optionStore.darkTheme ? colors.textDefault : colors.border
                            }}
                        >
                            <RowList
                                leading={<MaterialCommunityIcons
                                    name="qrcode-scan"
                                    color={colors.textTitle}
                                    size={16}
                                />}
                                trailing={<Text style={{color: colors.textTitle}}>{i18n.t(LANGUAGE_KEYS.SCAN_QRCODE)}</Text>}
                            />
                        </MenuOption>
                        <MenuOption
                            onSelect={() => navigation.navigate(ChatRoutes.GroupLists)}
                        >
                            <RowList
                                leading={<FontAwesome5
                                    name="bullhorn"
                                    color={colors.textTitle}
                                    size={16}
                                />}
                                leadingStyle={{
                                    bottom: -5
                                }}
                                trailing={<Text style={{color: colors.textTitle}}>{i18n.t(LANGUAGE_KEYS.PUBLIC_GROUPS)}</Text>}
                            />
                        </MenuOption>
                    </MenuOptions>
                </Menu>
            )} />
        }
    }

    private unsubscribe: any;

    componentDidMount() {
        chatStore.init(accountStore.currentAddress)
        // Listen for screen focus event
        this.unsubscribe = this.props.navigation.addListener('focus', this.onScreenFocus)
    }

    componentWillUnmount() {
        // unsubscribe
        isFunction(this.unsubscribe) && this.unsubscribe()
    }

    onScreenFocus = () => {
        this.showLoading = false
        this.fetchLists(action(() => {
            this.showLoading = true
        }))
    }

    private listElement: ListElement = React.createRef()

    @boundMethod
    scrollToListTop() {
        const listElement = this.listElement.current
        if (this.ListData.length) {
            listElement && listElement.scrollToIndex({ index: 0, viewOffset: 0 })
        }
    }
    @observable private isLoading: boolean = false

    @computed
    private get ListData(): IBlnScanMessage[] {
        const txns: IBlnScanMessage[] = []
        chatStore.lists.forEach((t)=>{
            if(!t.hidden){
                if(t.group){
                    const groupData = chatGroupStore.get(t.to)
                    if(groupData){
                        t = {...t, groupInfo: groupData.data}
                    }
                }
                txns.push(t)
            }
        })
        return txns
    }

    private showLoading: boolean = false

    @action
    private updateLoadingState(loading: boolean) {
        if (!this.showLoading) {
            return
        }
        this.isLoading = loading
    }

    @boundMethod
    private fetchLists(callback?: any): Promise<any> {
        this.updateLoadingState(true)
        return chatStore.refreshChatMap(callback).finally(() => this.updateLoadingState(false))
    }

    private getIdKey(tx: IBlnScanMessage, index?: number): string {
        return `index:${index}:sep:${tx.to}`
    }

    @boundMethod
    private renderListEmptyView(): JSX.Element | null {
        const { styles } = obStyles
        if(!this.showLoading){
            return (
                <Observer
                    render={() => (
                        <View style={{justifyContent: "center", alignItems: "center", flexDirection: "column", flex: 1, marginTop: sizes.screen.heightHalf - 150}}>
                            <ActivityIndicator color={colors.primary} size={'large'} />
                            <Text style={{marginTop: 10, color: colors.primary}}>{i18n.t(LANGUAGE_KEYS.LOADING)}</Text>
                        </View>
                    )}
                />
            )
        }
        return (
            <Observer
                render={() => (
                    <View style={styles.buttonResult}>
                        <ButtonResult
                            style={styles.buttonResult}
                            title={i18n.t(LANGUAGE_KEYS.CHAT_NO_CONTACT)}
                            button={i18n.t(LANGUAGE_KEYS.CHAT_START_CONTACT)}
                            onPress={() => {
                                this.onPressNavigation(GlobalRoutes.ChatNewChat)
                            }}
                        />
                    </View>
                )}
            />
        )
    }

    @boundMethod
    private getItemLayout(_: any, index: number) {
        const height = 40
        return {
            index,
            length: height,
            offset: height * index
        }
    }

    render() {
        const { styles } = obStyles
        return (
            <View style={styles.listWarp}>
                <WithPromiseActivityIndicator
                    promise={chatGroupStore.preload()}
                >
                    <MyFlatList
                        style={obStyles.styles.listView}
                        data={this.ListData}
                        ref={this.listElement}
                        getItemLayout={this.getItemLayout}
                        // // 列表为空时渲染
                        ListEmptyComponent={this.renderListEmptyView}
                        // // 当前列表 loading 状态
                        refreshing={this.isLoading}
                        // // 刷新
                        onRefresh={this.fetchLists}
                        // // 唯一 ID
                        keyExtractor={this.getIdKey}
                        // 单个主体
                        renderItem={({ item: msg, index }) => {
                            return (
                                <ChatMessageListItem
                                    msg={msg}
                                    onPress={(params) => {
                                        this.onPressNavigation(ChatRoutes.Message, params)
                                    }}
                                />
                            )
                        }}
                    />
                </WithPromiseActivityIndicator>
            </View>
        )
    }

    @boundMethod
    private onPressNavigation(routeName: ChatRoutes | SettingsRoutes | HomeRoutes | GlobalRoutes, params?: any) {
        this.props.navigation.navigate(routeName, params)
    }
}

const obStyles = observable({
    get styles() {
        return StyleSheet.create({
            plusButton: {
                position: "absolute",
                width: 36,
                height: 36,
                backgroundColor: colors.primary,
                borderRadius: 18,
                shadowColor: colors.secondary,
                shadowOpacity: 0.3,
                shadowOffset: { width: 0, height: 0 },
                bottom: 20,
                left: "50%",
                marginLeft: -18
            },
            centerContainer: {
                justifyContent: 'center',
                alignItems: 'center',
                padding: sizes.gap

            },
            loadmoreViewContainer: {
                ...mixins.rowCenter,
                padding: sizes.goldenRatioGap
            },
            normalTitle: {
                ...fonts.base,
                color: colors.textSecondary
            },
            smallTitle: {
                ...fonts.small,
                color: colors.textSecondary
            },
            listView: {
                width: sizes.screen.width
            },
            listWarp: {
                flex: 1,
                backgroundColor: colors.cardBackground
            },
            container: {
                flex: 1,
                backgroundColor: colors.background
            },
            headerCheckedIcon: {
                position: 'absolute',
                right: sizes.gap - 4,
                bottom: -1
            },
            buttonResult: {
                height: sizes.screen.height - 175,
                justifyContent: "center"
            },
            input: {
                textAlignVertical: 'top',
                color: colors.textDefault
            },
            inputBox: {
                marginLeft: 20,
                marginRight: 20,
                marginTop: 30
            },
            scanner: {
                paddingRight: 15
            },
            lists: {
                flex: 1
            },
            bootomBar: {
                height: 50,
                flexDirection: "row",
                alignItems: "center",
                justifyContent: "center",
                borderTopColor: colors.border,
                borderTopWidth: 1,
                paddingLeft: 10,
                paddingRight: 10
            },
            subtitle: {
                color: colors.textTitle
            },
        })
    }
})

import { LabelListTitle } from '@app/components/ui/list-title'
import { ScrollView, TextInput } from 'react-native-gesture-handler'
import showToast from '@app/services/toast'
import { MyFlatList } from '@app/components/common/FlatList'
import { AutoI18nTitle } from '@app/components/layout/title'
import { isFunction } from 'lodash'
import { chatGroupStore } from './stores/group'
import { optionStore } from '@app/stores/option'
import BLNListItem from '@app/components/ui/BLNListItem'
import { HeaderButton, HeaderButtons, Item } from 'react-navigation-header-buttons'

class FormStore {

    @observable name?: string
    @observable address?: string

    @action.bound
    changeName(v?: string) {
        this.name = v
    }

    @action.bound
    changeAddress(v?: string, show?: boolean) {
        this.address = v
    }

    @boundMethod
    reset() {
        this.address = undefined
        this.name = undefined
    }

    @computed
    get verify(): boolean {
        return this.address != undefined && verifyAccount(this.address)
    }
}
export const formStore = new FormStore()


@observer export class NewChatScreen extends Component<IIndexProps> {

    constructor(props: IIndexProps) {
        super(props)
    }

    static getPageScreenOptions = ({ navigation }: any) => {
        return {
            title: i18n.t(LANGUAGE_KEYS.NEW_CHAT),
            lazy : false,
            headerRight: (props: any) => (
                <Observer
                    render={() => {
                        return (
                            <HeaderButtons HeaderButtonComponent={HeaderButton}>
                                <Item
                                    {...props}
                                    iconName={"qrcode-scan"}
                                    iconSize={18}
                                    color={colors.textDefault}
                                    IconComponent={MaterialCommunityIcons}
                                    onPress={() => {
                                        navigation.navigate(HomeRoutes.QrScanner, {
                                            callback: (uri: IBlnScanerURL)=>{
                                                if (uri.address) {
                                                    formStore.changeAddress(uri.address)
                                                }
                                            }
                                        })
                                
                                    }}
                                />
                            </HeaderButtons>
                        );
                    }}
                />
            )
        }
    }

    componentDidMount() {
        formStore.reset()
        if (this.props.route.params && this.props.route.params.address) {
            formStore.changeAddress(this.props.route.params.address, true)
        }
    }

    render() {
        const { styles } = obStyles
        return (
            <View style={styles.container}>
                <ScrollView style={styles.lists}>
                    <View style={styles.inputBox}>
                        <LabelListTitle
                            leading={<Text style={styles.subtitle}>{i18n.t(LANGUAGE_KEYS.ADDRESS)}</Text>}
                            trailing={<TextButton text={i18n.t(LANGUAGE_KEYS.PASTE)} onPress={()=>{
                                Clipboard.getString().then((text: string)=>{
                                    formStore.changeAddress(text)
                                })
                            }}/>}
                        />
                        <TextInput
                            style={[styles.input, { height: 100 }]}
                            onChangeText={text => formStore.changeAddress(text)}
                            value={formStore.address}
                            multiline={true}
                            numberOfLines={4}
                            autoFocus={true}
                            placeholder={`blacknet....`}
                        />
                    </View>
                </ScrollView>
                <View style={styles.bootomBar}>
                    {this.renderBottom()}
                </View>
            </View>
        )
    }

    private renderBottom(){
        return <BLNListItem 
                trailing={<DoneButton name={i18n.t(LANGUAGE_KEYS.CONFIRM)} onPress={formStore.verify ? this.onPressSubmit : undefined}/>}
            />
    }

    @boundMethod
    onSuccessScaner(uri: IBlnScanerURL) {
        if (uri.address) {
            formStore.changeAddress(uri.address)
        }
    }

    @boundMethod
    private onPressSubmit() {
        if (!formStore.verify) {
            return showToast(i18n.t(LANGUAGE_KEYS.INPUT_ERROR))
        }
        this.props.navigation.replace(ChatRoutes.Message, { address: formStore.address, name: `${formStore.name ?? formStore.address}`, user: accountStore.currentAddress })
    }
}