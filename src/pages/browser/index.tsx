/**
 * Browser
 * @file Browser
 * @module pages/browser/index
 * @author Pony Ma <https://gitlab.com/blacknet-ninja>
 */

 import { IPageProps } from "@app/types/props";
 import { observer } from "mobx-react";
 import React, { Component } from "react";
 import { WebView } from 'react-native-webview';
 import styles from './styles';

 export interface IIndexProps extends IPageProps { }

 @observer
 export class BrowserScreen extends Component<IIndexProps> {
 
   render() {
        let { uri } = this.props.route.params
        if(!/^(http|https)/i.test(uri)){
            uri = "http://"+uri
        }
        return (
            <WebView 
                source={{ uri: uri }} 
                style={styles.webview.webview}    
            />
        );
   }
 
 }