/**
 * Browser Styles
 * @file styles
 * @module pages/browser/styles
 * @author Pony Ma <https://gitlab.com/blacknet-ninja>
 */


import { observable } from "mobx";
import { StyleSheet } from "react-native";

export default observable({
  get webview(){
    return StyleSheet.create({
        webview: {
          
        }
    })
  }
})