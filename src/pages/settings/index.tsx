/**
 * Settings
 * @file Settings
 * @module pages/settings/index
 * @author Pony Ma <https://gitlab.com/blacknet-ninja>
 */

import React, { Component } from 'react'
import { StyleSheet, View, Text, SafeAreaView, ScrollView, Alert, Switch, Image, NativeSyntheticEvent, NativeScrollEvent } from 'react-native'
import { boundMethod } from 'autobind-decorator'
import { action, observable } from 'mobx'
import { Observer, observer } from 'mobx-react'
import { IPageProps } from '@app/types/props'
import colors from '@app/style/colors'
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5'
import ListTitle from '@app/components/common/list-title'
import { CopyButton, DeleteButton } from '@app/components/ui/button'
import { StackActions } from '@react-navigation/native'
import { HomeRoutes, SettingsRoutes } from '@app/routes'
import { accountStore } from '@app/stores/account'
import { CURRENCY, optionStore } from '@app/stores/option'
import { languageMaps } from '@app/services/i18n'
import BottomSheet from '@app/components/common/bottom-sheet'
import { LANGUAGES } from '@app/constants/language'
import i18n from '@app/services/i18n'
import { LANGUAGE_KEYS } from '@app/constants/language'
import TouchID from '@app/services/touchid'
import showToast from '@app/services/toast'
import { stashStore } from '@app/stores/stash'
import * as RootNavigation from '../../rootNavigation';
import Upgrade from '@app/components/common/upgrade'
import { isFunction } from 'lodash'
import { userStore } from '@app/stores/users'
import { TouchableView } from '@app/components/common/touchable-view'
import { PostListName } from '@app/components/ui/forum'
import BLNListItem, { BLNListTitleMenu } from '@app/components/ui/BLNListItem'
import { Header } from '@react-navigation/stack'
import sizes from '@app/style/sizes'
import { HeaderButton, HeaderButtons, Item } from 'react-navigation-header-buttons'
import Ionicons from 'react-native-vector-icons/Ionicons'
import { shortAddress } from '@app/utils/address'
import Modal from 'react-native-modal';
import QRCode from 'react-native-qrcode-svg';
import Clipboard from '@react-native-community/clipboard';

class FormStore {
    @observable isVisible: boolean = false

    @action.bound
    changeVisible(checked: boolean) {
        this.isVisible = checked
    }

}
export const formStore = new FormStore()

export interface IIndexProps extends IPageProps { }

@observer export class Settings extends Component<IIndexProps> {

    private unsubscribe: any;

    static getPageScreenOptions = ({ navigation }: any) => {
        return {
        }
    }

    componentDidMount() {
        // Listen for screen focus event
        this.unsubscribe = this.props.navigation.addListener('focus', this.onScreenFocus)

        this.props.navigation.setOptions({
            title: null,
            headerStyle: {
                borderBottomWidth: 0,
                shadowOffset: {width: 0, height: 0},
                elevation: 0
            },
            headerRight: (props: any) => (
                <Observer
                    render={() => {
                        return (
                            <HeaderButtons HeaderButtonComponent={HeaderButton}>
                                <Item
                                    {...props}
                                    iconName={"share-outline"}
                                    iconSize={22}
                                    color={colors.textDefault}
                                    IconComponent={Ionicons}
                                    onPress={() => {
                                        formStore.changeVisible(true)    
                                    }}
                                />
                            </HeaderButtons>
                        );
                    }}
                />
            )
        })
    }

    componentWillUnmount() {
        // unsubscribe
        isFunction(this.unsubscribe) && this.unsubscribe()
    }

    onScreenFocus = () => {
        optionStore.refreshVersion()
    }

    render() {
        const { styles } = obStyles
        return (
            <SafeAreaView style={styles.container}>
                <ScrollView
                    scrollEventThrottle={16}
                    onScroll={this.onScroll}
                >
                    <View style={styles.content}>
                        {this._nameItem()}
                        {this._localauthItem()}
                        {this._themeDarkItem()}
                        {this._backupItem()}
                        {this._switchLanguageItem()}
                        {this._switchCurrencyItem()}
                        {this._communityItem()}
                        {this._stashItem()}
                        {this._versionItem()}
                        {this._logoutItem()}
                        <View style={{height: sizes.screen.heightHalf}}></View>
                    </View>
                </ScrollView>
                {this.renderMyQRcodeModal()}
            </SafeAreaView>
        )
    }

    @boundMethod
    private onScroll(event: NativeSyntheticEvent<NativeScrollEvent>) {
        const y = event.nativeEvent.contentOffset.y;
        if(y > 65){
            this.renderCustomHeader(true)
        }else{
            this.renderCustomHeader()
        }
    }

    renderCustomHeader(show?: boolean){
        if(!show){
            return this.props.navigation.setOptions({
                headerStyle: {
                    borderBottomWidth: 0,
                    shadowOffset: {width: 0, height: 0},
                    elevation: 0
                },
                header: (props: any) => {
                    return  <Header
                                {...props} 
                            />
                }
            })
        }
        return this.props.navigation.setOptions({
            headerStyle: {
                borderBottomWidth: 1,
                shadowOffset: {width: 0, height: 0},
                elevation: 0
            },
            header: (props: any) => {
                return <View>
                    <Header
                        {...props} 
                    />
                    <View style={{position: "absolute", top: sizes.safeAreaViewTop, right: 50, left: 0}}>
                        <BLNListTitleMenu
                            style={{margin: 15, marginTop: 8}}
                            title={userStore.getUserName(accountStore.currentAddress)}
                            titleNumberOfLines={1}
                            subtitle={shortAddress(accountStore.currentAddress)}
                            hiddenTrailingRight={true}
                            subtitleStyle={{}}
                            leading={<Image style={{width: 30, height: 30, borderRadius: 30}} source={{ uri: userStore.getUserImage(accountStore.currentAddress) }} />}
                            onPress={() => {
                                formStore.changeVisible(true)    
                            }}
                        />
                    </View>
                </View>
            }

        })
    }

    _nicknameItem() {
        const { styles } = obStyles
        return (<TouchableView
            onPress={() => {
                this.onPressNavigation(SettingsRoutes.Profile)
            }}>
            <View style={styles.szHeader}>
                <View style={styles.szLeft}>
                    <Image style={styles.avatar} source={{ uri: userStore.getUserImage(accountStore.currentAddress) }} />
                </View>

                <View style={styles.szName}>
                    <PostListName>{userStore.getUserName(accountStore.currentAddress)}</PostListName>
                </View>
                <View style={styles.rightlogo}>
                    <FontAwesome5
                        name="chevron-right"
                        style={styles.listTitleIcon}
                    />
                </View>
            </View>
        </TouchableView>
        )
    }

    _nameItem() {
        return <View style={{borderBottomWidth: 1, borderBottomColor: colors.border}}>
            <BLNListItem
                style={{marginLeft: 15, marginRight: 15, marginBottom: 15}}
                contentsStyle={{marginLeft: 10}}
                onPress={() => {
                    this.onPressNavigation(SettingsRoutes.Profile)
                }}
                leading={<Image style={{width: 50, height: 50, borderRadius: 50}} source={{ uri: userStore.getUserImage(accountStore.currentAddress) }} />}
                title={<Text style={{color: colors.textDefault}} numberOfLines={1}>{userStore.getUserName(accountStore.currentAddress)}</Text>}
                subtitle={<Text style={{color: colors.textSecondary}}>{accountStore.currentAddress}</Text>}
            />
        </View>
    }

    @boundMethod
    private onPressNavigation(routeName: SettingsRoutes, params?: any) {
        this.props.navigation.navigate(routeName)
    }

    @boundMethod
    private onPressLogout() {
        accountStore.switchAccount(undefined)
        stashStore.reset();
        this.props.navigation.dispatch(StackActions.replace(HomeRoutes.Home))
    }

    @boundMethod
    private onPressDelete() {
        const resetAction = StackActions.replace(HomeRoutes.Home)
        Alert.alert(i18n.t(LANGUAGE_KEYS.WARNING), i18n.t(LANGUAGE_KEYS.DELETE_CURRENT_ACCOUNT),
            [
                {
                    text: i18n.t(LANGUAGE_KEYS.CONFIRM), onPress: () => {
                        accountStore.deleteAccount(accountStore.current)
                        accountStore.switchAccount(undefined)
                        this.props.navigation.dispatch(resetAction)
                    }
                },
                { text: i18n.t(LANGUAGE_KEYS.CANCEL) }
            ]
        );
    }

    @boundMethod
    private toggleSwitch(toggle: boolean) {
        TouchID.isSupported()
            .then((enable: boolean) => {
                if (enable) {
                    TouchID.authenticate().then(() => {
                        accountStore.updateTouchid(toggle)
                    })
                } else {
                    showToast(i18n.t(LANGUAGE_KEYS.TOUCHID_NOT_SUPPORTED))
                }
            })
    }

    @boundMethod
    private toggleThemeSwitch(toggle: boolean) {
        optionStore.updateDarkTheme(toggle)
    }

    _localauthItem() {
        return <BLNListTitleMenu 
            IconComponent={FontAwesome5}
            IconName={"fingerprint"}
            title={i18n.t(LANGUAGE_KEYS.TOUCHID_FACEID)}
            trailing={<Switch
                trackColor={{ false: colors.grey, true: colors.primary }}
                ios_backgroundColor={colors.background}
                onValueChange={this.toggleSwitch}
                value={accountStore.currentTouchidEnable}
            />}
        />
    }

    _themeDarkItem() {
        return <BLNListTitleMenu 
            IconComponent={FontAwesome5}
            IconName={"adjust"}
            title={i18n.t(LANGUAGE_KEYS.THEME_DARK)}
            trailing={<Switch
                trackColor={{ false: colors.grey, true: colors.primary }}
                ios_backgroundColor={colors.background}
                onValueChange={this.toggleThemeSwitch}
                value={optionStore.darkTheme}
            />}
        />
    }

    _profileItem() {
        const { styles } = obStyles
        return (<ListTitle
            style={[styles.list, { marginTop: -1 }]}
            leading={
                <View style={styles.listTitleLeading}>
                    <FontAwesome5
                        name="id-card"
                        style={styles.listTitleIcon}
                    />
                </View>
            }
            title={
                <Text style={styles.listTitleText}>{i18n.t(LANGUAGE_KEYS.PROFILE)}</Text>
            }
            trailing={
                <FontAwesome5
                    color={colors.textDefault}
                    name="chevron-right"
                />
            }
            onPress={() => {
                this.onPressNavigation(SettingsRoutes.Profile)
            }}
        />)
    }

    _backupItem() {
        return <BLNListTitleMenu 
            IconComponent={FontAwesome5}
            IconName={"shield-alt"}
            title={i18n.t(LANGUAGE_KEYS.BACKUP)}
            trailingNum={!accountStore.currentBackup ? 1 : undefined}
            onPress={() => {
                if(accountStore.currentBackup){
                    return showToast(i18n.t(LANGUAGE_KEYS.BACKED_UP_NOT_VIEW))
                }   
                accountStore.authenticate().then(() => {
                    this.onPressNavigation(SettingsRoutes.Backup)
                }).catch((error) => {
                    showToast(`${error}`)
                })
            }}
        />
    }

    _contactItem() {
        return <BLNListTitleMenu 
            IconComponent={FontAwesome5}
            IconName={"address-book"}
            title={i18n.t(LANGUAGE_KEYS.CONTACT)}
            onPress={() => {
                this.onPressNavigation(SettingsRoutes.Contact)
            }}
        />
    }

    _switchLanguageItem() {
        return <BLNListTitleMenu 
            IconComponent={FontAwesome5}
            IconName={"language"}
            title={i18n.t(LANGUAGE_KEYS.SWTICH_LANGUAGE)}
            trailingText={languageMaps[optionStore.language].name}
            onPress={() => {
                BottomSheet.selectLanguage((lang: LANGUAGES) => {
                    optionStore.updateLanguage(lang)
                }, optionStore.language)
            }}
        />
    }

    _switchCurrencyItem() {
        return <BLNListTitleMenu 
            IconComponent={FontAwesome5}
            IconName={"dollar-sign"}
            title={i18n.t(LANGUAGE_KEYS.SWTICH_CURRENCY)}
            trailingText={optionStore.currency}
            onPress={() => {
                BottomSheet.selectCurrency((currency: CURRENCY) => {
                    optionStore.updateCurrency(currency)
                }, optionStore.currency)
            }}
        />
    }

    _communityItem() {
        return <BLNListTitleMenu 
            IconComponent={FontAwesome5}
            IconName={"home"}
            title={i18n.t(LANGUAGE_KEYS.COMMUNITY)}
            onPress={() => {
                this.onPressNavigation(SettingsRoutes.Community)
            }}
        />
    }

    _versionItem() {
        return <BLNListTitleMenu 
            IconComponent={FontAwesome5}
            IconName={"code"}
            title={i18n.t(LANGUAGE_KEYS.VERSION)}
            hiddenTrailingRight={true}
            trailingText={optionStore.currentVersion}
            onPress={this.checkVersionPress}
        />
    }

    _feedbackItem() {
        return <BLNListTitleMenu 
            IconComponent={FontAwesome5}
            IconName={"pen-square"}
            title={i18n.t(LANGUAGE_KEYS.FEEDBACK)}
            onPress={() => {
                this.onPressNavigation(SettingsRoutes.Feedback)
            }}
        />
    }

    _stashItem() {
        return <BLNListTitleMenu 
            IconComponent={FontAwesome5}
            IconName={"history"}
            title={i18n.t(LANGUAGE_KEYS.HISTORY)}
            trailingNum={stashStore.pendingLength}
            onPress={() => {
                RootNavigation.navigate(HomeRoutes.Stash, {});
            }}
        />
    }

    _logoutItem() {
        return <BLNListTitleMenu 
            IconComponent={FontAwesome5}
            IconName={"chevron-right"}
            title={i18n.t(LANGUAGE_KEYS.QUIT)}
            hiddenTrailingRight={true}
            reverseColor={true}
            onPress={() => {
                this.onPressLogout()
            }}
        />
    }

    _DeleteItem() {
        return (<DeleteButton
            text={i18n.t(LANGUAGE_KEYS.DELETE)}
            onPress={() => {
                this.onPressDelete()
            }}
        />)
    }

    @boundMethod
    private checkVersionPress() {
        if (!optionStore.versionShouldUpdate) {
            return showToast(i18n.t(LANGUAGE_KEYS.ISLASTESTVERSION))
        }
        return Upgrade.update(optionStore.version)
    }

    @boundMethod
    private renderMyQRcodeModal(){
        return <Modal 
            isVisible={formStore.isVisible}
            onBackdropPress={()=>{
                formStore.changeVisible(false)
            }}
        >
            <View style={{
                backgroundColor: colors.background,
                padding: 16,
                borderRadius: 4,
                position: "relative"
            }}
            >
                <View style={{justifyContent: "center", alignItems: "center", marginBottom: 15}}>
                    <QRCode
                        value={`blacknet:${accountStore.currentAddress}`}
                        size={300}
                        logoBackgroundColor="transparent"
                    />
                </View>
                <CopyButton text={i18n.t(LANGUAGE_KEYS.COPY)} onPress={()=>{
                    Clipboard.setString(accountStore.currentAddress)
                    showToast(i18n.t(LANGUAGE_KEYS.COPIED_ADDRESS))
                    formStore.changeVisible(false)
                }}/>
            </View>
        </Modal>
    }
}

const obStyles = observable({
    get styles() {
        return StyleSheet.create({
            versionView: {
                position: 'relative'
            },
            versionViewPoint: {
                width: 8,
                height: 8,
                borderRadius: 8,
                backgroundColor: colors.transferOut,
                position: 'absolute',
                right: 0
            },
            stashPoint: {
                width: 18,
                height: 18,
                borderRadius: 18,
                backgroundColor: colors.transferOut,
                position: 'absolute',
                right: 14,
                justifyContent: 'center',
                top: -3
            },
            container: {
                flex: 1
            },
            content: {
                flex: 1
            },
            list: {
            },
            balance: {
                height: 200,
                alignItems: "center",
                justifyContent: "center"
            },
            balanceText: {
                color: colors.yellow,
                fontSize: 30
            },
            listTitleLeading: {
            },
            listTitleIcon: {
                color: colors.textDefault,
                fontSize: 16
            },
            listTitleText: {
                color: colors.textDefault,
                marginRight: 10
            },
            listLanguage: {
                flexDirection: 'row',
                alignItems: 'center'
            }, avatar: {
                flex: 1,
                marginTop: 10,
                height: 80,
                width: 80,
                borderRadius: 10,
                marginLeft: 20
            },
            szName: {
                marginLeft: -5,
                flex: 2,
                marginTop: 20
            },
            szHeader: {
                flexDirection: "row",
                paddingTop: 20,
                paddingBottom: 20
            },
            szLeft: {
                flex: 1,
            },
            rightlogo: {
                color: colors.textDefault,
                marginRight: 10,
                paddingTop: 10,
                flexDirection: 'column',
                justifyContent: 'center',
            }
        })
    }
})
