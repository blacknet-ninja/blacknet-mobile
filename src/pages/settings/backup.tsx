/**
 * Backup
 * @file Backup
 * @module pages/settings/backup
 * @author Pony Ma <https://gitlab.com/blacknet-ninja>
 */

import React, { Component } from 'react'
import { StyleSheet, View, Text, SafeAreaView, ScrollView } from 'react-native'
import { action, computed, observable } from 'mobx'
import { Observer, observer } from 'mobx-react'
import { IPageProps } from '@app/types/props'
import colors from '@app/style/colors'
import { DoneButton, NextButton, PrevButton, TextButton } from '@app/components/ui/button'
import { accountStore } from '@app/stores/account'
import { LabelListTitle } from '@app/components/ui/list-title'
import Clipboard from '@react-native-community/clipboard'
import i18n from '@app/services/i18n'
import { LANGUAGE_KEYS } from '@app/constants/language'
import { showToast } from '@app/services/toast'
import { boundMethod } from 'autobind-decorator'
import BLNListItem from '@app/components/ui/BLNListItem'
import { BLNTextInput } from '@app/components/common/searchInput'
import Dialog from 'react-native-dialog'

export interface IIndexProps extends IPageProps {}

class FormStore {
  @observable step: number = 1
  @observable index: number = 0
  @observable indexText: string = ""
  @observable onFocus: boolean = false
  @observable words: string[] = accountStore.currentMnemonicWords
  @observable visible: boolean = false
  @action.bound
  changeVisible(v: boolean) {
      this.visible = v
  }
  @action.bound
  changeStep(v: number) {
      this.step = v
  }
  @action.bound
  changeIndex(v: number) {
      this.index = v
  }
  @action.bound
  changeIndexText(v: string) {
      this.indexText = v
  }
  @action.bound
  changeFocus(v: boolean) {
      this.onFocus = v
  }
  @action.bound
  reset(){
      this.step = 1
      this.index = 0
      this.indexText = ""
      this.onFocus = false
      this.visible = false
  }
  @computed
  get verify(): boolean {
      return this.indexText != undefined && this.indexText != ''
  }
  @computed
  get verifyWord(): boolean {
      return this.indexText === this.words[formStore.index - 1]
  }
}

export const formStore = new FormStore()

@observer export class BackupScreen extends Component<IIndexProps> {

    static getPageScreenOptions = ({ navigation }: any) => {
        return {
          headerTitle: (props: any) => (
            <Observer
                render={() => {
                    return (
                        <View style={{justifyContent: 'center', alignItems: "center"}}>
                          <Text>{i18n.t(LANGUAGE_KEYS.BACKUP)}</Text>
                          <Text style={{color: colors.textMuted}}>{`${i18n.t(LANGUAGE_KEYS.STEP)} ${formStore.step}/3`}</Text>
                        </View>
                    );
                }}
            />
          )
        }
    }

    @boundMethod
    private onPressCopy(){
      Clipboard.setString(accountStore.currentMnemonic)
      showToast(i18n.t(LANGUAGE_KEYS.COPIED_MNEMONIC))
    }

    componentDidMount(){
      formStore.reset()
    }

    render() {
        const { styles } = obStyles
        let page = this.renderStep1()
        if(formStore.step === 2){
          page = this.renderStep2()
        }
        if(formStore.step === 3){
          page = this.renderStep3()
        }
        return (
            <SafeAreaView style={styles.container}>
                <ScrollView style={styles.lists}>
                    {page}
                </ScrollView>
                <View style={styles.bootomBar}>
                    {this.renderBottom()}
                </View>
                {this.renderConfirmDialog()}
            </SafeAreaView>
        )
    }

    private renderBottom(){
      const { styles } = obStyles
      if(formStore.step === 1){
          return <BLNListItem
              trailing={<NextButton 
                  onPress={()=>{
                    formStore.changeIndex(Math.floor(Math.random() * 12) + 1)
                    formStore.changeStep(2)
                  }}
              />}
          />
      }
      if(formStore.step === 2){
        return <BLNListItem 
            leading={<PrevButton onPress={()=>{
              formStore.changeStep(1)
            }} />}
            trailing={<NextButton 
              onPress={()=>{
                if(formStore.verifyWord){
                  formStore.changeIndex(Math.floor(Math.random() * 12) + 1)
                  formStore.changeStep(3)
                  formStore.changeIndexText("")
                }
              }}
          />}
        />
    }
      if(formStore.step === 3){
          return <BLNListItem 
                  leading={<PrevButton onPress={()=>{
                    formStore.reset()
                    formStore.changeStep(1)
                  }} />}
                  trailing={<DoneButton name={i18n.t(LANGUAGE_KEYS.CONFIRM)} 
                  onPress={formStore.verifyWord ? ()=>{
                    // Clipboard.setString(accountStore.currentMnemonic)
                    // showToast(i18n.t(LANGUAGE_KEYS.COPIED_MNEMONIC))
                    // formStore.reset()
                    // formStore.changeStep(1)
                    formStore.changeVisible(true)
                  } : undefined}
              />}
          />
      }
      return null
    }

    private renderStep1(){
      const { styles } = obStyles
      const words = accountStore.currentMnemonicWords
      return <View style={{marginTop: 20}}>
          <LabelListTitle
              leading={<Text style={{color: colors.textDefault}}>{i18n.t(LANGUAGE_KEYS.MNEMONIC)}</Text>}
          />
          <View style={{borderWidth: 1, borderColor: colors.border, borderRadius: 5, flexDirection: "row", marginTop: 0}}>
            <View style={{flex: 1, paddingBottom: 10}}>
              {words.slice(0, 6).map((text, index)=>{
                return <View style={{flexDirection: "row", marginTop: 10}} key={text}>
                  <Text style={{marginLeft: 15}}>{index+1}</Text>
                  <Text style={{marginLeft: 15}}>{text}</Text>  
                </View>
              })}
            </View>
            <View style={{borderWidth: 0.5, borderColor: colors.border, width: 0}}></View>
            <View style={{flex: 1, paddingBottom: 10}}>
              {words.slice(6).map((text, index)=>{
                return <View style={{flexDirection: "row", marginTop: 10}} key={text}>
                  <Text style={{marginLeft: 15}}>{index+7}</Text>
                  <Text style={{marginLeft: 15}}>{text}</Text>  
                </View>
              })}
            </View>
        </View>
        <Text style={{marginTop: 15}}>{i18n.t(LANGUAGE_KEYS.BACKUP_DESC)}</Text>
      </View>
    }

    private renderStep2(){
      const { styles } = obStyles
      return <View style={{marginTop: 20}}>
        <LabelListTitle
            leading={<View style={{flexDirection: "row", alignItems: "center"}}>
              <Text style={{color: colors.textDefault, marginRight: 6}}>{i18n.t(LANGUAGE_KEYS.CONFIRM_YOUR_MNEMONIC)}</Text>
              <Text style={{color: colors.textMuted}}>{`${formStore.index} #${i18n.t(LANGUAGE_KEYS.WORDS)}`}</Text>
            </View>}
        />
        <View style={styles.inputView}
        >
          <BLNTextInput style={{paddingLeft: 10}} 
            onFocus={()=>{formStore.changeFocus(true)}} 
            onBlur={()=>{formStore.changeFocus(false)}}
            onChangeText={(text: string)=>{
              formStore.changeIndexText(text)
            }}
          />
          {formStore.onFocus ? 
            <TextButton style={{paddingLeft: 10}} text={i18n.t(LANGUAGE_KEYS.CANCEL)} />
          : null}
        </View>
        <Text style={{color: colors.textMuted, marginTop: 15}}>{`${i18n.t(LANGUAGE_KEYS.CONFIRM_YOUR_MNEMONIC_DESC)} ${formStore.index}#${i18n.t(LANGUAGE_KEYS.WORDS)}`}</Text>
      </View>
    }

    private renderStep3(){
      const { styles } = obStyles
      return <View style={{marginTop: 20}}>
        <LabelListTitle
            leading={<View style={{flexDirection: "row", alignItems: "center"}}>
              <Text style={{color: colors.textDefault, marginRight: 6}}>{i18n.t(LANGUAGE_KEYS.CONFIRM_YOUR_MNEMONIC)}</Text>
              <Text style={{color: colors.textMuted}}>{`${formStore.index} #${i18n.t(LANGUAGE_KEYS.WORDS)}`}</Text>
            </View>}
        />
        <View style={styles.inputView}
        >
          <BLNTextInput style={{paddingLeft: 10}} 
            onFocus={()=>{formStore.changeFocus(true)}} 
            onBlur={()=>{formStore.changeFocus(false)}}
            onChangeText={(text: string)=>{
              formStore.changeIndexText(text)
            }}
          />
          {formStore.onFocus ? 
            <TextButton style={{paddingLeft: 10}} text={i18n.t(LANGUAGE_KEYS.CANCEL)} />
          : null}
        </View>
        <Text style={{color: colors.textMuted, marginTop: 15}}>{`${i18n.t(LANGUAGE_KEYS.CONFIRM_YOUR_MNEMONIC_DESC)} ${formStore.index}#${i18n.t(LANGUAGE_KEYS.WORDS)}`}</Text>
      </View>
    }

    renderConfirmDialog() {
      return (
        <Dialog.Container visible={formStore.visible}>
          <Dialog.Title>{i18n.t(LANGUAGE_KEYS.ARE_YOU_SURE)}</Dialog.Title>
          <Dialog.Description>{i18n.t(LANGUAGE_KEYS.NOT_BE_VIEW_AGAIN_MNEMONIC)}</Dialog.Description>
          <Dialog.Button
            label={i18n.t(LANGUAGE_KEYS.YES)}
            onPress={()=>{
              accountStore.updateBackup(true)
              formStore.changeVisible(false)
              this.props.navigation.goBack()
            }}
          />
          <Dialog.Button
            label={i18n.t(LANGUAGE_KEYS.NO)}
            onPress={()=>{
              formStore.changeVisible(false)
            }}
          />
        </Dialog.Container>
      );
    }
}

const obStyles = observable({
    get styles() {
      return StyleSheet.create({
        inputView: {
          flexDirection: "row",
          alignItems: "center"
        },
        input: {
            height: 40,
            borderColor: colors.border, 
            borderWidth: 1,
            color: colors.textDefault
        },
        item: {
            backgroundColor: colors.border,
            marginBottom: 20,
            borderRadius: 2,
            padding: 10
        },
        bootomBar: {
            height: 50,
            flexDirection: "row",
            alignItems: "center",
            justifyContent: "center",
            borderTopColor: colors.border,
            borderTopWidth: 1,
            paddingLeft: 10,
            paddingRight: 10
        },
        lists: {
          flex: 1,
          paddingLeft: 20,
          paddingRight: 20
        },
        container: {
          flex: 1
        }
      })
    }
})
