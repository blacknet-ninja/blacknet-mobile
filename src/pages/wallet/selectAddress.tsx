/**
 * Send
 * @file Send
 * @module pages/wallet/send
 * @author Pony Ma <https://gitlab.com/blacknet-ninja>
 */

import React, { Component } from 'react'
import { StyleSheet, View, Text, SafeAreaView, ScrollView } from 'react-native'
import { boundMethod } from 'autobind-decorator'
import { observable, action, computed } from 'mobx'
import { Observer, observer } from 'mobx-react'
import { IPageProps } from '@app/types/props'
import colors from '@app/style/colors'
import { IBlnScanerURL } from '@app/types/bln';
import { HomeRoutes } from '@app/routes'
import i18n from '@app/services/i18n'
import { LANGUAGE_KEYS } from '@app/constants/language'
import TextInput from '@app/components/common/text-input'
import { DoneButton, TextButton } from '@app/components/ui/button'
import BLNListItem from '@app/components/ui/BLNListItem'
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons'
import { LabelListTitle } from '@app/components/ui/list-title'
import { HeaderButton, HeaderButtons, Item } from 'react-navigation-header-buttons'
import { verifyAccount } from '@app/utils/bln'
import Clipboard from '@react-native-community/clipboard'
import { isFunction } from 'lodash'

class FormStore {
    @observable address?: string

    @action.bound
    changeAddress(v: string) {
        this.address = v
    }
    @action.bound
    reset(){
        this.address = undefined
    }
    @computed
    get verify(): boolean {
        return this.address != undefined && this.address != '' && verifyAccount(this.address)
    }
}
export const formStore = new FormStore()

export interface IIndexProps extends IPageProps { }
@observer export class SelectAddressScreen extends Component<IIndexProps> {

    constructor(props: IIndexProps) {
        super(props)
    }

    componentDidMount(){
        formStore.reset()
        let params = this.props.route.params;
        if(params){
            if (params.address) {
                formStore.changeAddress(params.address)
            }
        } 
    }

    static getPageScreenOptions = ({ navigation }: any) => {
        return {
            title:  i18n.t(LANGUAGE_KEYS.TX_TO),
            lazy : false,
            headerRight: (props: any) => (
                <Observer
                    render={() => {
                        return (
                            <HeaderButtons HeaderButtonComponent={HeaderButton}>
                                <Item
                                    {...props}
                                    iconName={"qrcode-scan"}
                                    iconSize={18}
                                    color={colors.textDefault}
                                    IconComponent={MaterialCommunityIcons}
                                    onPress={() => {
                                        navigation.navigate(HomeRoutes.QrScanner, {
                                            callback: (uri: IBlnScanerURL)=>{
                                                if (uri.address) {
                                                    formStore.changeAddress(uri.address)
                                                }
                                            }
                                        })
                                
                                    }}
                                />
                            </HeaderButtons>
                        );
                    }}
                />
            )
        }
    }

    @computed
    get showNext() {
        return true
    }

    render() {
        const { styles } = obStyles
        return (
            <SafeAreaView style={styles.container}>
                <ScrollView style={styles.lists}>
                    <View style={styles.inputView}>
                        <LabelListTitle
                            leading={<Text style={styles.subtitle}>{i18n.t(LANGUAGE_KEYS.ADDRESS)}</Text>}
                            trailing={<TextButton text={i18n.t(LANGUAGE_KEYS.PASTE)} onPress={()=>{
                                Clipboard.getString().then((text: string)=>{
                                    formStore.changeAddress(text)
                                })
                            }}/>}
                        />
                        <TextInput
                            style={[styles.input, { height: 100 }]}
                            onChangeText={text => formStore.changeAddress(text)}
                            value={formStore.address}
                            multiline={true}
                            numberOfLines={4}
                            autoFocus={true}
                            placeholder={`blacknet....`}
                        />
                    </View>
                </ScrollView>
                <View style={styles.bootomBar}>
                    {this.renderBottom()}
                </View>
            </SafeAreaView>
        )
    }

    private renderBottom(){
        return <BLNListItem 
                trailing={<DoneButton  onPress={formStore.verify ? this.onPressBootom : undefined}/>}
            />
    }

    @boundMethod
    private onPressBootom() {
        this.props.navigation.goBack()
        const { callback } = this.props.route.params
        if (isFunction(callback)) {
            callback(formStore.address)
        }
    }

}

const obStyles = observable({
    get styles() {
        return StyleSheet.create({
            inputView: {
                padding: 10
            },
            input: {
                textAlignVertical: 'top',
                color: colors.textDefault
            },
            subtitle: {
                color: colors.textTitle
            },
            bootomBar: {
                height: 50,
                flexDirection: "row",
                alignItems: "center",
                justifyContent: "center",
                borderTopColor: colors.border,
                borderTopWidth: 1,
                paddingLeft: 10,
                paddingRight: 10
            },
            lists: {
                flex: 1
            },
            container: {
                flex: 1,
                backgroundColor: colors.background
            }
        })
    }
})
