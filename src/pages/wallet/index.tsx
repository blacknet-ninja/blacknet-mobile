/**
 * Wallet
 * @file Wallet
 * @module pages/wallet/index
 * @author Pony Ma <https://gitlab.com/blacknet-ninja>
 */

import React, { Component, RefObject } from 'react'
import { StyleSheet, View, Text, SafeAreaView, FlatList, Modal } from 'react-native'
import { boundMethod } from 'autobind-decorator'
import { observable, action, computed } from 'mobx'
import { Observer, observer } from 'mobx-react'
import { IPageProps } from '@app/types/props'
import colors from '@app/style/colors'
import ListTitle from '@app/components/common/list-title'
import { HeaderBlnBalance2 } from '@app/components/ui/BLNBalance'
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5'
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons'
import Entypo from 'react-native-vector-icons/Entypo'
import { ChatRoutes, DiscoverRoutes, HomeRoutes, WalletRoutes } from '@app/routes'
import i18n from '@app/services/i18n'
import { LANGUAGE_KEYS } from '@app/constants/language'
import { accountStore } from '@app/stores/account'
import { IBlnScanerURL, ICoinCap, IPortfolioData } from '@app/types/bln'
import { userStore } from '@app/stores/users'
import { isFunction } from 'lodash'
import { optionStore } from '@app/stores/option'
import coinmarketcapStore from '../coinmarketcap/stores/coinmarketcap'
import BigNumber from 'bignumber.js'
import Numeral from '@app/utils/numeral'
import { IconButton } from '@app/components/ui/button'
import { HeaderButton, HeaderButtons, Item } from 'react-navigation-header-buttons'
import { BLNText } from '@app/components/common/text'
import { LabelListTitle } from '@app/components/ui/list-title'
import portfolioStore from '../portfolio/stores'
import { ButtonResult } from '@app/components/ui/NoResult'
import { MyFlatList } from '@app/components/common/FlatList'
import SearchPortfolio from '../portfolio/search'
import { SortTouch } from '@app/components/ui/sort'
import sizes from '@app/style/sizes'
import { CoinList } from '../portfolio'
import BottomSheet from '@app/components/common/bottom-sheet'
import { TouchableView } from '@app/components/common/touchable-view'
import { chatGroupStore } from '../chat/stores/group'

class FormStore {
  @observable isVisible: boolean = false

  @action.bound
  changeVisiable(isVisible: boolean = false) {
      this.isVisible = isVisible
  }
  @action.bound
  reset() {
      this.isVisible = false
      this.sortType = undefined
      this.sortFiled = undefined
  }

  @observable sortType?: 'up' | 'down' = undefined
  @observable sortFiled?: 'price' | 'hold' = undefined
  @action.bound
  changeSort(filed: 'price' | 'hold', sort?: 'up' | 'down') {
      this.sortFiled = filed
      this.sortType = sort
  }
}

export const formStore = new FormStore()

type CoinsListElement = RefObject<FlatList<IPortfolioData>>

export interface IIndexProps extends IPageProps { }
@observer export class Wallet extends Component<IIndexProps> {

  static getPageScreenOptions = ({ navigation }: any) => {
    return {
        title:  "",
        headerStyle: {
            backgroundColor: colors.background,
            borderBottomWidth: 0,
            shadowOffset: {width: 0, height: 0},
            elevation: 0
        },
        lazy : false,
        headerRight: (props: any) => (
            <Observer
                render={() => {
                    return (
                        <HeaderButtons HeaderButtonComponent={HeaderButton}>
                            <Item
                                {...props}
                                iconName={"dots-three-horizontal"}
                                iconSize={18}
                                color={colors.textDefault}
                                IconComponent={Entypo}
                                onPress={() => {
                                  BottomSheet.walletMenu((type: number)=>{
                                    switch (type) {
                                        case 0:
                                            formStore.changeVisiable(true)
                                            break;
                                        case 1:
                                            navigation.navigate(HomeRoutes.Transfer, {type: 2})
                                            break;
                                        case 2:
                                            navigation.navigate(WalletRoutes.CancelLease)
                                            break;
                                        case 3:
                                            navigation.navigate(WalletRoutes.SignMessage)
                                            break;
                                        case 4:
                                            navigation.navigate(WalletRoutes.VerifyMessage)
                                            break;
                                        default:
                                            break;
                                    }
                                  })
                                }}
                            />
                        </HeaderButtons>
                    );
                }}
            />
        ),
        headerLeft: (props: any) => (
          <Observer
              render={() => {
                  return (
                      <HeaderButtons HeaderButtonComponent={HeaderButton}>
                          <Item
                              {...props}
                              iconName={"qrcode-scan"}
                              iconSize={18}
                              color={colors.textDefault}
                              IconComponent={MaterialCommunityIcons}
                              onPress={() => {
                                  navigation.navigate(HomeRoutes.QrScanner, {
                                      callback: (uri: IBlnScanerURL)=>{
                                        switch (uri.screen) {
                                          case HomeRoutes.AddContact:
                                            navigation.navigate(HomeRoutes.AddContact, uri)
                                            break;
                                          default: //HomeRoutes.Transfer
                                            navigation.navigate(HomeRoutes.Transfer, uri)
                                            break;
                                        }
                                      }
                                  })
                              }}
                          />
                      </HeaderButtons>
                  );
              }}
          />
      )
    }
}

  @computed
  get balance(){
    let t = new BigNumber(0)
    const ticker = coinmarketcapStore.get(optionStore.blnName)
    if(ticker?.quote[this.currency]){
        t = t.plus(new BigNumber(accountStore.currencybalance).multipliedBy(ticker?.quote[this.currency].price || "0"))
    }
    return t
  }

  private listElement: CoinsListElement = React.createRef()

  @boundMethod
  scrollToListTop() {
      const listElement = this.listElement.current
      if (this.ListData.length) {
          listElement && listElement.scrollToIndex({ index: 0, viewOffset: 0 })
      }
  }

  @observable private isLoading: boolean = false

  @computed
  get currency() {
      return optionStore.currency
  }

  @computed
  private get ListData(): IPortfolioData[] {
      const datas = portfolioStore.getData()
      if (formStore.sortFiled && formStore.sortType) {
          datas.sort((a, b) => {
              const tickerA = coinmarketcapStore.get(a.name)
              const tickerB = coinmarketcapStore.get(b.name)
              if (formStore.sortFiled === 'hold') {
                  if (formStore.sortType === 'up') {
                      return new BigNumber(a.amount).multipliedBy(tickerA?.currentQuoteData?.price || "0").toNumber() - new BigNumber(b.amount).multipliedBy(tickerB?.currentQuoteData?.price || "0").toNumber()
                  } else {
                      return new BigNumber(b.amount).multipliedBy(tickerB?.currentQuoteData?.price || "0").toNumber() - new BigNumber(a.amount).multipliedBy(tickerA?.currentQuoteData?.price || "0").toNumber()
                  }
              } else {
                  if (formStore.sortType === 'up') {
                      return new BigNumber(tickerA?.currentQuoteData?.percent_change_24h || '0').toNumber() - new BigNumber(tickerB?.currentQuoteData?.percent_change_24h || '0').toNumber()
                  } else {
                      return new BigNumber(tickerB?.currentQuoteData?.percent_change_24h || '0').toNumber() - new BigNumber(tickerA?.currentQuoteData?.percent_change_24h || '0').toNumber()
                  }
              }
          })
      }
      return datas;
  }

  @boundMethod
  totalValue(): string {
      let t = new BigNumber(0)
      this.ListData.forEach((p) => {
          const ticker = coinmarketcapStore.get(p.name)
          if (ticker?.quote[this.currency]) {
              t = t.plus(new BigNumber(p.amount).multipliedBy(ticker?.quote[this.currency].price || "0"))
          }
      })
      t = t.plus(this.balance)
      return Numeral.total(t)
  }

  private showLoading: boolean = false
  private unsubscribe: any;

  componentDidMount() {
      this.showLoading = true
      // Listen for screen focus event
      this.unsubscribe = this.props.navigation.addListener('focus', this.onScreenFocus)

      chatGroupStore.refresh()
  }

  componentWillUnmount() {
      // unsubscribe
      isFunction(this.unsubscribe) && this.unsubscribe()
  }

  onScreenFocus = () => {
      this.showLoading = false
      coinmarketcapStore.refresh().finally(() => {
          this.showLoading = true
      })
  }

  @action
  private updateLoadingState(loading: boolean) {
      if (!this.showLoading) {
          return
      }
      this.isLoading = loading
  }

  @boundMethod
  private fetchLists(page: number = 1): Promise<any> {
    this.updateLoadingState(true)
    portfolioStore.initAccount();
    return Promise.all([
      accountStore.refreshBalance(), 
      coinmarketcapStore.refreshBLN(), 
      coinmarketcapStore.refresh]
    ).finally(() => this.updateLoadingState(false))
  }

  private getIdKey(tx: IPortfolioData, index?: number): string {
      return `index:${index}:sep:${tx.symbol}`
  }

  @boundMethod
  private renderListEmptyView(): JSX.Element | null {
      const { styles } = obStyles
      return (
          <Observer
              render={() => (
                  <View style={styles.buttonResult}>
                      <ButtonResult
                          style={styles.buttonResult}
                          title={i18n.t(LANGUAGE_KEYS.PORTFOLIO_NODATA)}
                          button={i18n.t(LANGUAGE_KEYS.PORTFOLIOADD)}
                          onPress={() => {
                              formStore.changeVisiable(true)
                          }}
                      />
                  </View>
              )}
          />
      )
  }

  render() {
      const { styles } = obStyles
      return (
          <View style={styles.listWarp}>
              {this.renderTotalHeader()}
              {this.renderBlnBalance()}
              <MyFlatList
                  style={obStyles.styles.listView}
                  data={this.ListData}
                  ref={this.listElement}
                  // 列表为空时渲染
                  ListEmptyComponent={this.renderListEmptyView}
                  // 当前列表 loading 状态
                  refreshing={this.isLoading}
                  // 刷新
                  onRefresh={this.fetchLists}
                  // 唯一 ID
                  keyExtractor={this.getIdKey}
                  // 单个主体
                  renderItem={({ item: coin, index }) => {
                      return (
                          <Observer
                              render={() => (
                                  <View style={styles.seq}>
                                      <CoinList
                                          data={coin}
                                          onPress={() => {
                                              const ticker = coinmarketcapStore.get(coin.name)
                                              this.onPressNavigation(HomeRoutes.PortfolioAdd, { data: ticker, amount: coin.amount })
                                          }}
                                      />
                                  </View>
                              )}
                          />
                      )
                  }}
                  ListHeaderComponent={this.renderListHeaderComponent()}
              />
              <Modal
                  visible={formStore.isVisible}
                  hardwareAccelerated={true}
                  presentationStyle={"overFullScreen"}
                  animationType={'fade'}
              >
                  <SafeAreaView style={{ flex: 1 }}>
                      <SearchPortfolio
                          onPress={(data: ICoinCap, coin: IPortfolioData) => {
                              formStore.changeVisiable(false)
                              this.onPressNavigation(HomeRoutes.PortfolioAdd, { data: data, amount: coin?.amount })
                          }}
                          onCancelPress={() => {
                              formStore.changeVisiable(false)
                          }}
                          showCancel={true}
                      />
                  </SafeAreaView>
              </Modal>
              <IconButton style={styles.sendButton} size={20} name={"send"}
                  onPress={() => {
                      this.onPressNavigation(HomeRoutes.Transfer)
                  }}
              />
          </View>
      )
  }

  renderListHeaderComponent() {
      if (this.ListData.length < 1) {
          return null
      }
      return <ListTitle
          contentsStyle={{ borderTopWidth: 0, height: 'auto', alignItems: 'flex-end', flex: 2 }}
          leadingStyle={{ width: 'auto', height: 'auto', flex: 1 }}
          leading={<BLNText>{i18n.t(LANGUAGE_KEYS.ASSET)}</BLNText>}
          title={<SortTouch name={i18n.t(LANGUAGE_KEYS.PRICE)}
              direction={formStore.sortFiled === 'price' ? formStore.sortType : undefined}
              onChange={(d) => {
                  formStore.changeSort('price', d)
              }}
          />}
          trailing={<SortTouch name={i18n.t(LANGUAGE_KEYS.HOLD)}
              direction={formStore.sortFiled === 'hold' ? formStore.sortType : undefined}
              onChange={(d) => {
                  formStore.changeSort('hold', d)
              }}
          />}
          trailingStyle={{ borderTopWidth: 0, height: 'auto', alignItems: 'flex-end', flex: 2 }}
          style={{ borderBottomColor: colors.border, borderBottomWidth: 1, marginLeft: 0, marginRight: 0, padding: 10 }}
      />
  }

  @boundMethod
  private onPressNavigation(routeName: WalletRoutes | ChatRoutes | DiscoverRoutes | HomeRoutes, params?: any) {
      this.props.navigation.navigate(routeName, params)
  }

  renderTotalHeader() {
      const { styles } = obStyles
      return (<View style={{margin: 10, borderRadius: 10}}>
          <BLNText style={{ color: colors.textDefault, fontSize: 24, marginBottom: 10 }}>{this.totalValue()}</BLNText>
          <BLNText style={{ color: colors.textDefault }}>{i18n.t(LANGUAGE_KEYS.TOTAL)}</BLNText>
      </View>)
  }

  renderBlnBalance() {
    const { styles } = obStyles
    return (<>
        <View style={{margin: 10, backgroundColor: colors.primary, borderRadius: 10, padding: 10, borderBottomLeftRadius: 0, borderBottomRightRadius: 0, marginBottom: 0}}>
            <LabelListTitle
                leading={<Text style={{color: colors.white, fontSize: 16}} numberOfLines={1} >{userStore.getUserName(accountStore.currentAddress)}</Text>}
            />
            <HeaderBlnBalance2
            onPress={() => {
                this.onPressNavigation(HomeRoutes.ExplorerDetail, {
                type: "account",
                id: accountStore.currentAddress
                })
            }}
            balance={accountStore.currentShowBalance}
            currency={Numeral.total(this.balance)}
            nickname={userStore.getName(accountStore.currentAddress)}
            />
        </View>
        <View style={{margin: 10, marginTop:0,flexDirection: "row", backgroundColor: colors.primary, justifyContent: 'space-around', alignItems: 'center', height: 40, borderBottomLeftRadius: 10, borderBottomRightRadius: 10}}>
            <TouchableView style={{flexDirection: "row", alignItems: "center"}}
                onPress={() => this.onPressNavigation(WalletRoutes.Received)}
            >
                <FontAwesome5 name={"qrcode"} size={16} color={colors.white}/>
                <BLNText style={{color: colors.white, fontSize: 16, marginLeft: 10}}>{i18n.t(LANGUAGE_KEYS.TX_RECEIVED)}</BLNText>
            </TouchableView>
            <TouchableView style={{flexDirection: "row", alignItems: "center"}}
                onPress={() => this.onPressNavigation(WalletRoutes.Transactions)}
            >
                <FontAwesome5 name={"history"} size={16} color={colors.white}/>
                <BLNText style={{color: colors.white, fontSize: 16, marginLeft: 10}}>{i18n.t(LANGUAGE_KEYS.TX_TRANSACTIONS)}</BLNText>
            </TouchableView>
        </View>
    </>)
  }
}

const obStyles = observable({
  get styles() {
    return StyleSheet.create({
      container: {
        flex: 1,
        backgroundColor: colors.cardBackground
      },
      content: {
        flex: 1
      },
      sendButton: {
        position: "absolute",
        width: 36,
        height: 36,
        backgroundColor: colors.primary,
        borderRadius: 18,
        shadowColor: colors.secondary,
        shadowOpacity: 0.3,
        shadowOffset: { width: 0, height: 0 },
        bottom: 20,
        left: "50%",
        marginLeft: -18,
        transform: [{ rotate: '-90deg'}]
      },
      listItem: {
        // paddingLeft: 10,
        // paddingRight: 10,
        // paddingTop: 8,
        // paddingBottom: 8,
        // backgroundColor: colors.cardBackground
      },
      menus: {
        borderTopWidth: 1,
        borderTopColor: colors.border,
        borderBottomColor: colors.border,
        borderBottomWidth: 1
      },
      balance: {
        height: 150
      },
      balanceText: {
        color: colors.yellow,
        fontSize: 30
      },
      balanceCNY: {
        fontSize: 14
      },
      listTitleLeading: {
        
      },
      listTitleIcon: {
        color: colors.textDefault,
        fontSize: 16
      },
      listTitleText: {
        color: colors.textDefault
      },
      buttonResult: {
        height: sizes.screen.height / 2,
        justifyContent: "center"
      },
      seq: {
          margin: 0
      },
      listView: {
          width: sizes.screen.width
      },
      listWarp: {
          flex: 1,
          paddingTop: 5,
          backgroundColor: colors.cardBackground
      }
    })
  }
})
