/**
 * Transactions
 * @file Transactions
 * @module pages/wallet/transactions
 * @author Pony Ma <https://gitlab.com/blacknet-ninja>
 */

import React, { Component, RefObject } from 'react'
import { StyleSheet, View, FlatList } from 'react-native'
import { boundMethod } from 'autobind-decorator'
import { observable, action, computed, reaction } from 'mobx'
import { Observer, observer } from 'mobx-react'
import { IPageProps } from '@app/types/props'
import colors from '@app/style/colors'
import sizes from '@app/style/sizes'
import fonts from '@app/style/fonts'
import mixins from '@app/style/mixins'
import API from '@app/services/api'
import { IRequestParams, ITxResultPaginate, ITxPaginate } from '@app/types/http'
import { IBlnTransaction } from '@app/types/bln'
import { accountStore } from '@app/stores/account'
import i18n from '@app/services/i18n'
import { LANGUAGE_KEYS } from '@app/constants/language'
import transactionStore from './stores/transaction'
import { WalletRoutes } from '@app/routes'
import { NoResult } from '@app/components/ui/NoResult'
import { MyFlatList, MyFlatListFooter_LOADING, MyFlatListFooter_LOADMORE, MyFlatListFooter_NO_MORE } from '@app/components/common/FlatList'
import { TabView, SceneMap, NavigationState, SceneRendererProps, TabBar } from 'react-native-tab-view';
import { BLNTransactionItem } from '@app/components/ui/BLNListItem'

type State = NavigationState<{
    key: string;
    title: string;
  }>;
  
class FilterStore {
    @observable type: number = -1
    @boundMethod changeType(type: number) {
        this.type = type
    }
}
export const filterStore = new FilterStore()

type ITxResultPaginateLists = ITxResultPaginate<IBlnTransaction[]>
export type TransactionListElement = RefObject<FlatList<IBlnTransaction>>
export interface IIndexProps extends IPageProps { }

@observer export class TransactionsScreen extends Component<IIndexProps> {

    constructor(props: IIndexProps) {
        super(props)
        this.initList()
        this.fetchLists()
        reaction(
            () => [
                filterStore.type
            ],
            ([type]: any) => this.handleFilterChanged(type)
        )
    }

    static getPageScreenOptions = ({ navigation }: any) => {
        return {
            title: i18n.t(LANGUAGE_KEYS.TX_TRANSACTIONS),
            headerStyle: {
                backgroundColor: colors.background,
                borderBottomWidth: 0,
                shadowOffset: {width: 0, height: 0},
                elevation: 0
            },
            lazy : false
        }
    }

    private listElement: TransactionListElement = React.createRef()

    @boundMethod
    scrollToListTop() {
        const listElement = this.listElement.current
        if (this.ListData.length) {
            listElement && listElement.scrollToIndex({ index: 0, viewOffset: 0 })
        }
    }

    @boundMethod
    private handleFilterChanged(type: number) {
        if (this.pagination && this.lists.length > 0) {
            this.scrollToListTop()
        }
        this.initList()
        this.fetchLists()
    }

    @observable private isLoading: boolean = false
    @observable.shallow private lists: IBlnTransaction[] = []
    @observable.ref private pagination: ITxPaginate | null = null

    @observable private tabIndex: number = 0

    @action.bound
    private changeTabIndex(i: number) {
        filterStore.changeType(parseInt(this.tabRoutes[i].key))
        this.tabIndex = i
    }

    @observable private tabRoutes: any = [
        { key: '-1', title: i18n.t(LANGUAGE_KEYS.TX_ALL) },
        { key: '0', title: i18n.t(LANGUAGE_KEYS.TX_TRANSFER) },
        { key: '2', title: i18n.t(LANGUAGE_KEYS.TX_LEASE) },
        { key: '3', title: i18n.t(LANGUAGE_KEYS.TX_CANCEL_LEASE) },
        { key: '254', title: i18n.t(LANGUAGE_KEYS.TX_POS_GENERATED) }
    ]

    @computed
    private get ListData(): IBlnTransaction[] {
        return this.lists.slice()
    }

    @computed 
    private get params(): IRequestParams{
        return {
            type: filterStore.type === -1 ? "all" : filterStore.type
        }
    }

    @computed
    private get isNoMoreData(): boolean {
        return (
            !!this.pagination &&
            this.pagination.txns_len < 100
        )
    }

    @action
    private updateLoadingState(loading: boolean) {
        this.isLoading = loading
    }

    @action
    private updateResultData(result: ITxResultPaginateLists) {
        const { txns, pagination } = result
        this.pagination = pagination
        if (pagination.current_page > 1) {
            this.lists.push(...txns)
        } else {
            this.lists = txns
        }
    }

    @boundMethod
    private initList(): Promise<any> {
        return transactionStore.get(accountStore.currentAddress, filterStore.type).then((json) => {
            if (json.length > 0) {
                action(() => {
                    this.lists = json
                })()
            }
            return json
        })
    }

    @boundMethod
    private fetchLists(page: number = 1): Promise<any> {
        this.updateLoadingState(true)
        const address = accountStore.currentAddress
        return API.fetchTransactions(address, { ...this.params, page })
            .then((json) => {
                this.updateResultData(json)
                if (page === 1) {
                    transactionStore.upserts(address, filterStore.type, json.txns)
                }
                return json
            })
            .catch(error => {
                console.warn('Fetch tx list error:', error)
                return Promise.reject(error)
            })
            .finally(() => this.updateLoadingState(false))
    }

    @boundMethod
    private handleLoadmoreList() {
        if (!this.isNoMoreData && !this.isLoading && this.pagination) {
            this.fetchLists(this.pagination.current_page + 1)
        }
    }

    private getArticleIdKey(tx: IBlnTransaction, index?: number): string {
        return `index:${index}:sep:${tx.hash}:${tx.seq}`
    }

    @boundMethod
    private renderListEmptyView(): JSX.Element | null {
        return (
            <Observer
                render={() => (
                    <NoResult />
                )}
            />
        )
    }

    // 渲染脚部的三种状态：空、加载中、无更多、上拉加载
    @boundMethod
    private renderListFooterView(): JSX.Element | null {
        const { styles } = obStyles
        if (!this.lists.length) {
            return null
        }
        if (this.lists.length < 100 || this.isNoMoreData) {
            return (
                <MyFlatListFooter_NO_MORE />
            )
        }
        if (this.isLoading) {
            return (
                <MyFlatListFooter_LOADING />
            )
        }
        return (
            <MyFlatListFooter_LOADMORE />
        )
    }

    @boundMethod
    private onPressNavigation(routeName: WalletRoutes, tx: IBlnTransaction) {
        this.props.navigation.navigate(routeName, {
            txid: tx.txid,
            tx: tx
        })
    }

    private tabRouteScenes(){
        const FirstRoute = () => (
            <View style={{ height: 100, backgroundColor: '#ff4081' }} />
        );
        const SecondRoute = () => (
            <View style={{ height: 100, backgroundColor: '#673ab7' }} />
        );
        return SceneMap({
            "-1": FirstRoute,
            "0": SecondRoute,
            '2': FirstRoute,
            '3': SecondRoute,
            '254': FirstRoute
        })
    }

    @boundMethod
    private renderTabBar(props: SceneRendererProps & { navigationState: State }){
        const { styles } = obStyles
        return <TabBar
            {...props}
            scrollEnabled
            indicatorStyle={styles.indicator}
            style={styles.tabbar}
            tabStyle={styles.tab}
            labelStyle={styles.label}
        />
    }
    
    render() {
        return (
            <View style={obStyles.styles.listViewContainer}>
                <View  style={{height: 45}}>
                    {this.renderListHeaderComponent()}
                </View>
                <View style={{flex: 1}}>
                    <MyFlatList
                        style={obStyles.styles.listView}
                        data={this.ListData}
                        ref={this.listElement}
                        // 列表为空时渲染
                        ListEmptyComponent={this.renderListEmptyView}
                        // 加载更多时渲染
                        ListFooterComponent={this.renderListFooterView}
                        // 当前列表 loading 状态
                        refreshing={this.isLoading}
                        // 刷新
                        onRefresh={this.fetchLists}
                        // 加载更多
                        onEndReached={this.handleLoadmoreList}
                        // 唯一 ID
                        keyExtractor={this.getArticleIdKey}
                        // 单个主体
                        renderItem={({ item: tx }) => {
                            return (
                                <Observer
                                    render={() => (
                                        <View style={obStyles.styles.listViewItem}>
                                            <BLNTransactionItem
                                                tx={tx}
                                                symbol={accountStore.symbol}
                                                address={accountStore.currentAddress}
                                                onPress={() => {
                                                    this.onPressNavigation(WalletRoutes.WalletDetail, tx)
                                                }}
                                            />
                                        </View>
                                    )}
                                />
                            )
                        }}
                    />
                </View>
            </View>
        )
    }

    @boundMethod
    renderListHeaderComponent(){
        return <TabView
            navigationState={{ index: this.tabIndex, routes: this.tabRoutes }}
            lazy
            renderScene={this.tabRouteScenes()}
            onIndexChange={this.changeTabIndex}
            renderTabBar={this.renderTabBar}
            scrollEnabled={true}
            initialLayout={{ width: sizes.screen.width }}
            renderScene={({ route, jumpTo }) => {
                return null
            }}
        />
    }
}

const obStyles = observable({
    get styles() {
        return StyleSheet.create({
            listViewContainer: {
                position: 'relative',
                flex: 1,
                height: "100%"
            },
            listView: {
                width: sizes.screen.width
            },
            centerContainer: {
                justifyContent: 'center',
                alignItems: 'center',
                padding: sizes.gap
            },
            loadmoreViewContainer: {
                ...mixins.rowCenter,
                padding: sizes.goldenRatioGap
            },
            normalTitle: {
                ...fonts.base,
                color: colors.textSecondary
            },
            smallTitle: {
                ...fonts.small,
                color: colors.textSecondary
            },
            headerCheckedIcon: {
                position: 'absolute',
                right: sizes.gap - 4,
                bottom: -1
            },
            tabbar: {
                backgroundColor: colors.primary,
            },
            tab: {
                width: "auto",
            },
            indicator: {
                backgroundColor: colors.white,
            },
            label: {
                fontWeight: '400',
            },
        })
    }
})
