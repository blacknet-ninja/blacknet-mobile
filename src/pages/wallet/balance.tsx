/**
 * Balance
 * @file Balance
 * @module pages/wallet/balance
 * @author Pony Ma <https://gitlab.com/blacknet-ninja>
 */

import React, { Component } from 'react'
import { StyleSheet, View, Text, ScrollView, RefreshControl } from 'react-native'
import { boundMethod } from 'autobind-decorator'
import { observable, action, } from 'mobx'
import { observer } from 'mobx-react'
import { IPageProps } from '@app/types/props'
import { RowListItem } from '@app/components/ui/list-title'
import i18n from '@app/services/i18n'
import { LANGUAGE_KEYS } from '@app/constants/language'
import { accountStore } from '@app/stores/account'
import { showBalance } from '@app/utils/bln'

export interface IIndexProps extends IPageProps { }
@observer export class BalanceScreen extends Component<IIndexProps> {

    static getPageScreenOptions = ({ navigation }: any) => {
        return {
            title: i18n.t(LANGUAGE_KEYS.ACCOUNT_INFO)
        }
    }

    @observable private isRefreshing: boolean = false

    constructor(props: IIndexProps) {
        super(props)
        this.onRefresh()
    }

    @action
    private updateLoadingState(loading: boolean) {
        this.isRefreshing = loading
    }

    @boundMethod
    private onRefresh() {
        this.updateLoadingState(true)
        accountStore.refreshMoreBalance().finally(() => this.updateLoadingState(false))
    }

    render() {
        const { styles } = obStyles
        const balance = accountStore.currentBalance
        return (
            <ScrollView style={styles.container}
                refreshControl={
                    <RefreshControl
                        refreshing={this.isRefreshing}
                        onRefresh={this.onRefresh}
                    />
                }
            >
                <View style={styles.rows}>
                    {this._renderRow(i18n.t(LANGUAGE_KEYS.ADDRESS), accountStore.currentAddress)}
                    {this._ownerRow()}
                    {this._renderRow(i18n.t(LANGUAGE_KEYS.ACCOUNT_SEQ), balance.seq)}
                    {this._renderRow(i18n.t(LANGUAGE_KEYS.ACCOUNT_BALANCE), accountStore.currentShowBalance + " " + accountStore.symbol)}
                    {this._renderRow(i18n.t(LANGUAGE_KEYS.ACCOUNT_CONFIRMED_BALANCE), showBalance(balance.confirmedBalance || 0))}
                    {this._renderRow(i18n.t(LANGUAGE_KEYS.ACCOUNT_STAKING_BALANCE), showBalance(balance.stakingBalance || 0))}
                    {this._renderRow(i18n.t(LANGUAGE_KEYS.ACCOUNT_IN_LEASES_BALANCE), showBalance(balance.inLeasesBalance || 0))}
                    {this._renderRow(i18n.t(LANGUAGE_KEYS.ACCOUNT_OUT_LEASES_BALANCE), showBalance(balance.outLeasesBalance || 0))}
                    {this._renderRow(i18n.t(LANGUAGE_KEYS.ACCOUNT_TX_AMOUNT), balance.txamount || 0)}
                    {this._renderRow(i18n.t(LANGUAGE_KEYS.ACCOUNT_POS_BLOCKS), balance.blocks || 0)}
                    {this._renderRow(i18n.t(LANGUAGE_KEYS.ACCOUNT_REAL_BALANCE), showBalance(balance.realBalance || 0))}
                </View>
            </ScrollView>
        )
    }

    _ownerRow() {
        if (!accountStore.currentBalance.displayName) {
            return null
        }
        return <RowListItem
            leading={<Text style={obStyles.styles.leading}>{i18n.t(LANGUAGE_KEYS.ACCOUNT_OWNER)}</Text>}
            trailing={<Text style={obStyles.styles.trailing}>{accountStore.currentBalance.displayName}</Text>}
        />
    }

    _renderRow(leading: any, trailing: any) {
        return <RowListItem
            leading={<Text style={obStyles.styles.leading}>{leading}</Text>}
            trailing={<Text style={obStyles.styles.trailing}>{trailing}</Text>}
        />
    }
}

const obStyles = observable({
    get styles() {
        return StyleSheet.create({
            leading: {
                paddingRight: 15
            },
            trailing: {
                textAlign: "right"
            },
            container: {
                flex: 1
            },
            rows: {
                paddingLeft: 20,
                paddingRight: 20,
                paddingTop: 20
            },
            statusView: {
                height: 60,
                marginTop: 30,
                marginBottom: 10,
                justifyContent: 'space-between',
                alignItems: 'center',
            }
        })
    }
})