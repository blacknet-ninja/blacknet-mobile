/**
 * Send
 * @file Send
 * @module pages/wallet/send
 * @author Pony Ma <https://gitlab.com/blacknet-ninja>
 */

import React, { Component } from 'react'
import { StyleSheet, View, Text, SafeAreaView, ScrollView, Keyboard, Clipboard, ActivityIndicator } from 'react-native'
import { boundMethod } from 'autobind-decorator'
import { observable, action, computed } from 'mobx'
import { observer } from 'mobx-react'
import { IPageProps } from '@app/types/props'
import colors from '@app/style/colors'
import Ionicons from 'react-native-vector-icons/Ionicons';
import { LabelListTitle } from '@app/components/ui/list-title'
import API from '@app/services/api'
import { accountStore } from '@app/stores/account'
import showToast, {showTopToast} from '@app/services/toast';
import { IBlnTransaction } from '@app/types/bln';
import { GlobalRoutes, WalletRoutes } from '@app/routes'
import i18n from '@app/services/i18n'
import { LANGUAGE_KEYS } from '@app/constants/language'
import BottomSheet from '@app/components/common/bottom-sheet'
import { FilterType } from '@app/components/ui/filter'
import BLNBalance from '@app/components/ui/BLNBalance'
import * as RootNavigation from '@app/rootNavigation';
import { TextInput } from 'react-native-gesture-handler'
import sizes from '@app/style/sizes'
import { IconButton, LabelButton, NextButton, PrevButton, DoneButton, TextButton } from '@app/components/ui/button'
import TextInputWarp from '@app/components/common/text-input'
import BLNListItem from '@app/components/ui/BLNListItem'
import Image from '@app/components/common/image'
import { userStore } from '@app/stores/users'
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5'
import BigNumber from 'bignumber.js'
import Numeral from '@app/utils/numeral'
import coinmarketcapStore from '../coinmarketcap/stores/coinmarketcap'
import { optionStore } from '@app/stores/option'
import { verifyAccount } from '@app/utils/bln'

class FilterStore {
    @observable type: number = 0
    @boundMethod changeType(type: number) {
        this.type = type
    }
}
export const filterStore = new FilterStore()

class FormStore {
    @observable amount?: string
    @observable address?: string
    @observable message?: string
    @observable result?: string

    @action.bound
    changeAmount(v: string) {
        this.amount = v
    }
    @action.bound
    changeAddress(v: string) {
        this.address = v
    }
    @action.bound
    changeMessage(v: string) {
        this.message = v
    }
    @action.bound
    changeResult(v?: string) {
        this.amount = undefined
        this.address = undefined
        this.message = undefined
        this.result = v
    }
    @action.bound
    reset(){
        this.amount = undefined
        this.address = undefined
        this.message = undefined
        this.result = undefined
    }
    @computed
    get verify(): boolean {
        return this.address != undefined && this.address != '' && this.amount != undefined && this.amount != ''
    }

    @computed 
    get totalValue(): string{
        let t = new BigNumber(this.amount || "0")
        const ticker = coinmarketcapStore.get("Blacknet")
        if (ticker?.quote[optionStore.currency]) {
            t = t.multipliedBy(ticker?.quote[optionStore.currency].price || "0")
        }
        return Numeral.total(t)
    }

    @computed
    get verifyAmount(){
        const t = new BigNumber(this.amount || "0")
        if(t.comparedTo(this.maxAmount) > 0){
            showTopToast("资金不足")
        }
        return t.comparedTo(this.maxAmount) <= 0 && t.comparedTo(0) === 1
    }

    @computed
    get verifyAddress(){
        return verifyAccount(formStore.address)
    }

    @computed 
    get maxAmount(){
        return new BigNumber(accountStore.currencybalance).minus(0.001).toString()
    }
}
export const formStore = new FormStore()

export interface IIndexProps extends IPageProps { }
@observer export class SendScreen extends Component<IIndexProps> {

    constructor(props: IIndexProps) {
        super(props)
    }

    componentDidMount(){
        formStore.reset()
        let params = this.props.route.params;

        if(params){
            if (params.address) {
                formStore.changeAddress(params.address)
            }
            if (params.type || params.type === 0) {
                filterStore.changeType(params.type)
            }
            if (params.amount) {
                formStore.changeAmount(params.amount)
            }
            if (params.message) {
                formStore.changeMessage(params.message)
            }
        }else{
            filterStore.changeType(0)
        }
        
    }

    static getPageScreenOptions = ({ navigation }: any) => {
        return {
            title: i18n.t(LANGUAGE_KEYS.CREATE_TRANSACTION),
            lazy : false
        }
    }

    @observable private step: number = 1

    @action
    private updateStep(step: number){
        this.step = step
    }

    @computed
    get showNext() {
        if(this.step === 1){
            return formStore.verifyAmount && formStore.verifyAddress
        }
        if(this.step === 2){
            return formStore.verifyAmount && formStore.verifyAddress
        }
    }

    render() {
        const { styles } = obStyles
        let stepItem
        if(this.step === 1){
            stepItem = <SafeAreaView style={{flex: 1}}>
                <ScrollView style={styles.lists}>
                    {this.renderStepOne()}
                </ScrollView>
                <View style={styles.bootomBar}>
                    {this.renderBottom()}
                </View>
            </SafeAreaView>
        }
        if(this.step === 2){
            stepItem = <SafeAreaView style={{flex: 1}}>
                <ScrollView style={styles.lists}>
                    {this.renderStepTwo()}
                </ScrollView>
                <View style={styles.bootomBar}>
                    {this.renderBottom()}
                </View>
            </SafeAreaView>
        }
        if(this.step === 3){
            stepItem = this.renderLoading()
        }
        if(this.step === 4){
            stepItem = this.renderResult()
        }
        return (
            <View style={styles.container}>
                {stepItem}
            </View>
        )
    }

    private renderStepOne(){
        const { styles } = obStyles
        return <View>
            <View style={styles.inputArea}>
                <View style={styles.inputView}>
                    <TextInput
                        onChangeText={text => formStore.changeAmount(text)}
                        value={formStore.amount}
                        placeholder={"0.0"}
                        style={[styles.inputViewInput, {color: formStore.amount && formStore.verifyAmount ? colors.textTitle : colors.transferOut}]}
                        keyboardType={'numeric'}
                        placeholderTextColor={colors.border}
                        autoFocus={true}
                    />
                    <FilterType onPress={this.onPressSelectTxType} type={filterStore.type} />
                </View>
                <View style={styles.inputLabelView}>
                    <Text style={styles.inputLabelViewText}>{formStore.totalValue}</Text>
                </View>
                <View style={styles.inputOpView}>
                    <LabelButton text={i18n.t(LANGUAGE_KEYS.MAX)} onPress={()=>{
                        formStore.changeAmount(new BigNumber(accountStore.currencybalance).minus(0.001).toString())
                    }}/>
                </View>
            </View>
            <View style={styles.addressArea}>
                <Text style={[styles.subtitle, {marginBottom: 10}]}>{i18n.t(LANGUAGE_KEYS.TX_FROM)}</Text>
                <BLNListItem 
                    leading={<Image style={styles.avatar} source={{uri: userStore.getUserImage(accountStore.currentAddress)}}/>}
                    trailing={<FontAwesome5
                        name="chevron-right"
                        color={colors.border}
                        size={16}
                        />}
                    title={<Text>{userStore.getUserName(accountStore.currentAddress)}</Text>}
                    subtitle={<BLNBalance style={{textAlign: "left"}}>{accountStore.currentShowBalance}</BLNBalance>}
                />
                <Text style={[styles.subtitle, {marginBottom: 10, marginTop: 10}]}>{i18n.t(LANGUAGE_KEYS.TX_TO)}</Text>
                {this.renderTo()}
            </View>
        </View>
    }

    private renderStepTwo(){
        const { styles } = obStyles
        return <View style={{padding: 10}}>
            <LabelListTitle
                leading={<Text style={styles.subtitle}>{i18n.t(LANGUAGE_KEYS.TX_MESSAGE)}</Text>}
                trailing={<TextButton text={i18n.t(LANGUAGE_KEYS.PASTE)} onPress={()=>{
                    Clipboard.getString().then((text: string)=>{
                        formStore.changeMessage(text)
                    })
                }}/>}
            />
            <TextInputWarp
                style={[styles.input, { height: 120 }]}
                onChangeText={text => formStore.changeMessage(text)}
                value={formStore.message}
                multiline={true}
                numberOfLines={4}
                autoFocus={true}
                placeholder={`(${i18n.t(LANGUAGE_KEYS.OPTION)})`}
            />
        </View>
    }

    private renderLoading(){
        const { styles } = obStyles
        return <View style={{justifyContent: "center", alignItems: "center", flexDirection: "column", flex: 1}}>
            <ActivityIndicator color={colors.primary} size={'large'} />
            <Text style={{marginTop: 10, color: colors.primary}}>{i18n.t(LANGUAGE_KEYS.TRANSFERING)}</Text>
        </View>
    }

    private renderResult(){
        const { styles } = obStyles
        return <View style={{justifyContent: "center", alignItems: "center", flexDirection: "column", flex: 1}}>
            <Ionicons name="checkmark-circle" size={42} color={colors.transferIn}/>
            <Text style={{marginTop: 10, color: colors.textDefault}}>{i18n.t(LANGUAGE_KEYS.TRANSFER_SUCCESS)}</Text>
        </View>
    }

    private renderTo(){
        const { styles } = obStyles
        if(formStore.verifyAddress){
            return <BLNListItem 
                leading={<Image style={styles.avatar} source={{uri: userStore.getUserImage(formStore.address)}}/>}
                trailing={<FontAwesome5
                    name="chevron-right"
                    color={colors.border}
                    size={16}
                    />}
                title={<Text style={{color: colors.textTitle}}>{userStore.getUserName(formStore.address)}</Text>}
                onPress={()=>{
                    this.props.navigation.navigate(GlobalRoutes.SelectAddress, {
                        callback: this.onSuccessSelectAddress
                    })
                }}
            />
        }
        return <BLNListItem 
            leading={<IconButton name={"plus"} style={styles.icon} size={24}/>}
            trailing={<FontAwesome5
                name="chevron-right"
                color={colors.border}
                size={16}
                />}
            title={<Text style={{color: colors.primary}}>{i18n.t(LANGUAGE_KEYS.SELECT_ADDRESS)}</Text>}
            onPress={()=>{
                this.props.navigation.navigate(GlobalRoutes.SelectAddress, {
                    callback: this.onSuccessSelectAddress
                })
            }}
        />
    }

    private renderBottom(){
        const { styles } = obStyles
        if(this.step === 2){
            return <BLNListItem 
                leading={<PrevButton onPress={()=>{
                    this.updateStep(1)
                }} />}
                trailing={<DoneButton  onPress={this.showNext ? this.onPressBootom : undefined}/>}
            />
        }
        if(this.step === 1){
            return <BLNListItem
                trailing={<NextButton 
                    onPress={this.showNext ? ()=>{
                        this.updateStep(2)
                    } : undefined}
                />}
            />
        }
    }

    @boundMethod
    private onPressBootom() {
        accountStore.authenticate().then(() => {
            if (filterStore.type === 0) {
                this.pressTransfer()
            }
            if (filterStore.type === 2) {
                this.pressLease()
            }
        }).catch((error) => {
            showToast(`${error}`)
        })
    }

    @boundMethod
    private onSuccessSelectAddress(address: string) {
        formStore.changeAddress(address)
    }

    @boundMethod
    private onPressSelectTxType() {
        return BottomSheet.selectReceivedType((type: number,) => filterStore.changeType(type), filterStore.type)
    }

    @boundMethod
    private pressLease() {
        // const ld = Loading.show()
        this.updateStep(3)
        API.lease(
            accountStore.currentMnemonic,
            formStore.amount || "",
            accountStore.currentAddress,
            formStore.address || ""
        ).then((tx: IBlnTransaction) => {
            this.updateStep(4)
            showToast(i18n.t(LANGUAGE_KEYS.LEASE_SUCCESS))
            // formStore.changeResult(tx.txid)
            formStore.reset()
            // go to detail page
            RootNavigation.navigate(WalletRoutes.WalletDetail, {
                tx: tx
            })
        })
        .catch((err) => {
            showToast(`${err}`)
            console.log("Lease failed.", err);
        })
        .finally(() => {
            // Loading.hide(ld)
            accountStore.refreshBalance()
        })
    }
    @boundMethod
    private pressTransfer() {
        // const ld = Loading.show()
        this.updateStep(3)
        API.transfer(
            accountStore.currentMnemonic || "",
            formStore.amount || "",
            accountStore.currentAddress,
            formStore.address || "",
            formStore.message
        ).then((tx: IBlnTransaction) => {
            this.updateStep(4)
            showToast(i18n.t(LANGUAGE_KEYS.TRANSFER_SUCCESS))
            // formStore.changeResult(tx.txid)
            formStore.reset()
            // go to detail page
            RootNavigation.navigate(WalletRoutes.WalletDetail, {
                tx: tx
            })
        })
        .catch((err) => {
            showToast(`${err}`)
            console.log("Transfer failed.", err);
        })
        .finally(() => {
            // Loading.hide(ld)
            accountStore.refreshBalance()
        })
    }
}

const obStyles = observable({
    get styles() {
        return StyleSheet.create({
            input: {
                textAlignVertical: 'top',
                color: colors.textDefault
            },
            inputOpView: {
                flexDirection: "row"
            },
            icon: {
                backgroundColor: colors.primary,
                width: 40,
                height: 40,
                borderRadius: 40,
                marginRight: 15
            },
            avatar: {
                marginRight: 15,
                width: 40,
                height: 40,
                borderRadius: 40
            },
            inputArea: {
                padding: 10,
                borderBottomColor: colors.border,
                borderBottomWidth: 1
            },
            inputView:{
                flexDirection: "row",
                alignItems: "center"
            },
            inputViewInput:{
                maxWidth: sizes.screen.widthHalf,
                marginRight: 20,
                fontSize: 24
            },
            inputLabelView: {
                marginBottom: 10
            },
            inputLabelViewText: {
                color: colors.border
            },
            addressArea: {
                padding: 10
            },
            subtitle: {
                color: colors.textTitle
            },
            bootomBar: {
                height: 50,
                flexDirection: "row",
                alignItems: "center",
                justifyContent: "center",
                borderTopColor: colors.border,
                borderTopWidth: 1,
                paddingLeft: 10,
                paddingRight: 10
            },
            lists: {
                flex: 1
            },
            container: {
                flex: 1,
                backgroundColor: colors.background
            }
        })
    }
})
