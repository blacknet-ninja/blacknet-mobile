/**
 * Import
 * @file Import
 * @module pages/home/import
 * @author Pony Ma <https://gitlab.com/blacknet-ninja>
 */

import React, { Component } from 'react'
import { StyleSheet, View, Text, SafeAreaView, TextInput, ScrollView } from 'react-native'
import { boundMethod } from 'autobind-decorator'
import { action, computed, observable } from 'mobx'
import { observer } from 'mobx-react'
import { IPageProps } from '@app/types/props'
import colors from '@app/style/colors'
import BottomButton from '@app/components/ui/bottom-button'
import showToast from '@app/services/toast';
import blacknetjs from 'blacknetjs';
import Loading from '@app/components/common/loading'
import { accountStore } from '@app/stores/account'
import { createIBln } from '@app/types/bln'
import i18n from '@app/services/i18n'
import { LANGUAGE_KEYS } from '@app/constants/language'
import { checkVersion, verifyAccount } from '@app/utils/bln'
import TouchID from '@app/services/touchid'
import BLNListItem from '@app/components/ui/BLNListItem'
import { DoneButton, TextButton } from '@app/components/ui/button'
import { LabelListTitle } from '@app/components/ui/list-title'
import Clipboard from '@react-native-community/clipboard'

class FormStore {

    @observable text: string = ""

    @action.bound
    changeText(v: string) {
        this.text = v
    }
    @action.bound
    reset(){
        this.text = ""
    }
    @computed
    get verify(): boolean {
        return this.text != undefined && this.text != '' && checkVersion(this.text)
    }
}
export const formStore = new FormStore()

export interface IIndexProps extends IPageProps { }

@observer export class Import extends Component<IIndexProps> {

    static getPageScreenOptions = ({ navigation }: any) => {
        return {
          title: i18n.t(LANGUAGE_KEYS.IMPORT)
        }
    }

    render() {
        const { styles } = obStyles
        return (
            <SafeAreaView style={styles.container}>
                <ScrollView style={styles.lists}>
                    <Text style={styles.h1}>{i18n.t(LANGUAGE_KEYS.INPUT_YOUR_MNEMONIC)}</Text>
                    <View style={styles.inputBox}>
                        <LabelListTitle
                            leading={<Text style={styles.subtitle}>{i18n.t(LANGUAGE_KEYS.MNEMONIC)}</Text>}
                            trailing={<TextButton text={i18n.t(LANGUAGE_KEYS.PASTE)} onPress={()=>{
                                Clipboard.getString().then((text: string)=>{
                                    formStore.changeText(text)
                                })
                            }}/>}
                        />
                        <TextInput
                            style={styles.input}
                            onChangeText={text => formStore.changeText(text)}
                            value={formStore.text}
                            multiline={true}
                            numberOfLines={4}
                            autoFocus={true}
                            onBlur={()=>{
                                if(!formStore.verify && formStore.text !== ''){
                                    showToast(i18n.t(LANGUAGE_KEYS.INPUT_CORRECT_MNEMONIC))
                                }
                            }}
                        />
                    </View>
                    <Text style={styles.desc}>{i18n.t(LANGUAGE_KEYS.INPUT_YOUR_MNEMONIC_DESC)}</Text>
                </ScrollView>
                <View style={styles.bootomBar}>
                    {this.renderBottom()}
                </View>
            </SafeAreaView>
        )
    }

    private renderBottom(){
        return <BLNListItem 
                trailing={<DoneButton name={i18n.t(LANGUAGE_KEYS.CONFIRM)} onPress={formStore.verify ? this.onPressBootom : undefined}/>}
            />
    }

    @boundMethod
    private onPressBootom() {
        if (!formStore.verify || formStore.text.trim().split(" ").length != 12) {
            showToast(i18n.t(LANGUAGE_KEYS.INPUT_CORRECT_MNEMONIC))
        } else {
            const ld = Loading.show()
            const mnemonic = formStore.text.trim()
            const address = blacknetjs.Address(mnemonic);
            if (!verifyAccount(address)) {
                Loading.hide(ld)
                return showToast(i18n.t(LANGUAGE_KEYS.INPUT_CORRECT_MNEMONIC))
            }
            TouchID.isSupported()
                .then((enable: boolean) => {
                    if (enable) {
                        return TouchID.authenticate()
                    }
                    // 不支持touchid/faceid直接通过
                    return false
                })
                .then((enable) => {
                    // enable true表示支持touchid  false表示不支持
                    accountStore.addAccount(createIBln(mnemonic, address))
                    if (enable) {
                        accountStore.updateTouchid(true, address)
                    }
                    setTimeout(() => {
                        Loading.hide(ld)
                    }, 100)
                    setTimeout(() => {
                        this.props.navigation.goBack()
                    }, 150)
                }).catch((err) => {
                    showToast(`${err}`)
                }).finally(() => {
                    Loading.hide(ld)
                })
        }
    }
}

const obStyles = observable({
    get styles() {
        return StyleSheet.create({
            h1: {
                fontSize: 24,
                fontWeight: "bold",
                textAlign: "center",
                lineHeight: 50,
                marginTop: 20,
                marginBottom: 20,
                color: colors.textDefault
            },
            desc: {
                fontSize: 14,
                fontWeight: "bold",
                lineHeight: 25,
                textAlign: "center",
                marginTop: 10,
                marginBottom: 10,
                color: colors.textDefault
            },
            input: {
                height: 150,
                borderColor: colors.border,
                borderWidth: 1,
                color: colors.textDefault
            },
            subtitle: {
                color: colors.textTitle
            },
            inputBox: {
                paddingLeft: 20,
                paddingRight: 20
            },
            bootomBar: {
                height: 50,
                flexDirection: "row",
                alignItems: "center",
                justifyContent: "center",
                borderTopColor: colors.border,
                borderTopWidth: 1,
                paddingLeft: 10,
                paddingRight: 10
            },
            lists: {
                flex: 1
            },
            container: {
                flex: 1
            }
        })
    }
})


@observer export class Import2 extends Component<IIndexProps> {

    state = {
        text: ""
    }

    onChangeText(text: string) {
        this.setState({
            text: text
        })
    }

    render() {
        const { styles } = obStyles
        return (
            <SafeAreaView style={styles.container}>
                <ScrollView style={styles.lists}>
                    <Text style={styles.h1}>{i18n.t(LANGUAGE_KEYS.INPUT_YOUR_MNEMONIC)}</Text>
                    <View style={styles.inputBox}>
                        <TextInput
                            style={styles.input}
                            onChangeText={text => this.onChangeText(text)}
                            value={this.state.text}
                        />
                    </View>
                    <Text style={styles.desc}>{i18n.t(LANGUAGE_KEYS.INPUT_YOUR_MNEMONIC_DESC)}</Text>
                </ScrollView>
                <View style={styles.bootomBar}>
                    <BottomButton text={i18n.t(LANGUAGE_KEYS.CONFIRM)} onPress={() => { this.onPressBootom() }} />
                </View>
            </SafeAreaView>
        )
    }

    @boundMethod
    private onPressBootom() {
        if (!this.state.text || this.state.text.trim().split(" ").length != 12) {
            showToast(i18n.t(LANGUAGE_KEYS.INPUT_CORRECT_MNEMONIC))
        } else {
            const ld = Loading.show()
            const mnemonic = this.state.text.trim()
            const address = blacknetjs.Address(mnemonic);
            if (!verifyAccount(address)) {
                Loading.hide(ld)
                return showToast(i18n.t(LANGUAGE_KEYS.INPUT_CORRECT_MNEMONIC))
            }
            TouchID.isSupported()
                .then((enable: boolean) => {
                    if (enable) {
                        return TouchID.authenticate()
                    }
                    // 不支持touchid/faceid直接通过
                    return false
                })
                .then((enable) => {
                    // enable true表示支持touchid  false表示不支持
                    accountStore.addAccount(createIBln(mnemonic, address))
                    if (enable) {
                        accountStore.updateTouchid(true, address)
                    }
                    setTimeout(() => {
                        Loading.hide(ld)
                    }, 100)
                    setTimeout(() => {
                        this.props.navigation.goBack()
                    }, 150)
                }).catch((err) => {
                    showToast(`${err}`)
                }).finally(() => {
                    Loading.hide(ld)
                })
        }
    }
}

@observer export class Import3 extends Component<IIndexProps> {

    state = {
        text: ""
    }

    onChangeText(text: string) {
        this.setState({
            text: text
        })
    }

    render() {
        const { styles } = obStyles
        return (
            <SafeAreaView style={styles.container}>
                <ScrollView style={styles.lists}>
                    <Text style={styles.h1}>{i18n.t(LANGUAGE_KEYS.INPUT_YOUR_MNEMONIC)}</Text>
                    <View style={styles.inputBox}>
                        <TextInput
                            style={styles.input}
                            onChangeText={text => this.onChangeText(text)}
                            value={this.state.text}
                        />
                    </View>
                    <Text style={styles.desc}>{i18n.t(LANGUAGE_KEYS.INPUT_YOUR_MNEMONIC_DESC)}</Text>
                </ScrollView>
                <View style={styles.bootomBar}>
                    <BottomButton text={i18n.t(LANGUAGE_KEYS.CONFIRM)} onPress={() => { this.onPressBootom() }} />
                </View>
            </SafeAreaView>
        )
    }

    @boundMethod
    private onPressBootom() {
        if (!this.state.text || this.state.text.trim().split(" ").length != 12) {
            showToast(i18n.t(LANGUAGE_KEYS.INPUT_CORRECT_MNEMONIC))
        } else {
            const ld = Loading.show()
            const mnemonic = this.state.text.trim()
            const address = blacknetjs.Address(mnemonic);
            if (!verifyAccount(address)) {
                Loading.hide(ld)
                return showToast(i18n.t(LANGUAGE_KEYS.INPUT_CORRECT_MNEMONIC))
            }
            TouchID.isSupported()
                .then((enable: boolean) => {
                    if (enable) {
                        return TouchID.authenticate()
                    }
                    // 不支持touchid/faceid直接通过
                    return false
                })
                .then((enable) => {
                    // enable true表示支持touchid  false表示不支持
                    accountStore.addAccount(createIBln(mnemonic, address))
                    if (enable) {
                        accountStore.updateTouchid(true, address)
                    }
                    setTimeout(() => {
                        Loading.hide(ld)
                    }, 100)
                    setTimeout(() => {
                        this.props.navigation.goBack()
                    }, 150)
                }).catch((err) => {
                    showToast(`${err}`)
                }).finally(() => {
                    Loading.hide(ld)
                })
        }
    }
}