import React, { Component } from 'react';
import { WebView } from 'react-native-webview';
import { IPageProps } from '@app/types/props'
import { observer } from 'mobx-react';

@observer export class WebviewScreen extends Component<IPageProps> {
  render() {
    return <WebView source={{ uri: this.props.route.params.uri }} />;
  }
}