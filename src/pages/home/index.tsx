/**
 * Index
 * @file Index
 * @module pages/home/index
 * @author Pony Ma <https://gitlab.com/blacknet-ninja>
 */

import React, { Component, RefObject } from 'react'
import { StyleSheet, View, FlatList, SafeAreaView, Text } from 'react-native'
import { observable, computed } from 'mobx'
import { Observer, observer } from 'mobx-react'
import { IPageProps } from '@app/types/props'
import colors from '@app/style/colors'
import { AccountListItem } from '@app/components/ui/list-title'
import Loading from '@app/components/common/loading'
import { ChatRoutes, GlobalRoutes, HomeRoutes, WalletRoutes } from '@app/routes'
import { accountStore } from '@app/stores/account'
import blacknetjs from 'blacknetjs';
import { StackActions } from '@react-navigation/native'
import { createIBln, IBln } from '@app/types/bln'
import i18n from '@app/services/i18n'
import sizes from '@app/style/sizes'
import { LANGUAGE_KEYS } from '@app/constants/language'
import { boundMethod } from 'autobind-decorator'
import { Result } from '@app/components/ui/NoResult'
import { userStore } from '@app/stores/users'
import { MyFlatList } from '@app/components/common/FlatList'
import portfolioStore from '../portfolio/stores'
import { HeaderButton, HeaderButtons, Item } from 'react-navigation-header-buttons'
import Entypo from 'react-native-vector-icons/Entypo'
import BottomSheet from '@app/components/common/bottom-sheet'
import { TextButton } from '@app/components/ui/button'
export interface IIndexProps extends IPageProps { }
export type ListElement = RefObject<FlatList<IBln>>
@observer export class Home extends Component<IIndexProps> {

  static getPageScreenOptions = ({ navigation }: any) => {
      return {
        title: i18n.t(LANGUAGE_KEYS.YOUR_ACCOUNTS),
        headerStyle: {
            backgroundColor: colors.background,
            borderBottomWidth: 0,
            shadowOffset: {width: 0, height: 0},
            elevation: 0
        },
        lazy : false,
        headerRight: (props: any) => (
            <Observer
                render={() => {
                    return (
                        <HeaderButtons HeaderButtonComponent={HeaderButton}>
                            <Item
                                {...props}
                                iconName={"dots-three-horizontal"}
                                iconSize={18}
                                color={colors.textDefault}
                                IconComponent={Entypo}
                                onPress={() => {
                                  BottomSheet.createAccount(()=>{
                                    const mnemonic = blacknetjs.Mnemonic();
                                    const address = blacknetjs.Address(mnemonic);
                                    const bln = createIBln(mnemonic, address)
                                    accountStore.addAccount(bln)
                                    accountStore.switchAccount(bln);
                                    portfolioStore.initAccount();
                                    setTimeout(() => {
                                      navigation.dispatch(StackActions.replace(HomeRoutes.Main, { screen: WalletRoutes.Wallet }));
                                    }, 150)
                                  })
                                }}
                            />
                        </HeaderButtons>
                    );
                }}
            />
        )
      }
  }

  private listElement: ListElement = React.createRef()

  @boundMethod
  scrollToListTop() {
    const listElement = this.listElement.current
    if (this.ListData.length) {
      listElement && listElement.scrollToIndex({ index: 0, viewOffset: 0 })
    }
  }

  @computed
  private get ListData(): IBln[] {
    return accountStore.lists
  }

  private getIdKey(tx: IBln, index?: number): string {
    return `index:${index}:${tx.address}`
  }

  @boundMethod
  private renderListEmptyView(): JSX.Element | null {
    const { styles } = obStyles
    return (
      <Observer
        render={() => (
          <View style={styles.noResult}>
            <Result
              title={<Text style={styles.noResultTitle}>{i18n.t(LANGUAGE_KEYS.ACCOUNT_NO_RESULT)}</Text>}
            />
          </View>
        )}
      />
    )
  }

  render() {
    const { styles } = obStyles
    return (
      <SafeAreaView style={styles.container}>
        <View style={styles.lists}>
          <MyFlatList
            style={obStyles.styles.listView}
            data={this.ListData}
            ref={this.listElement}
            // 列表为空时渲染
            ListEmptyComponent={this.renderListEmptyView}
            // 唯一 ID
            keyExtractor={this.getIdKey}
            // 单个主体
            renderItem={({ item: account, index }) => {
              return (
                <Observer
                  render={() => (
                    <AccountListItem key={index} onPress={() => { this.onPressNavigation(account) }}
                      title={account.address}
                      balance={account.balance}
                      name={userStore.getUserName(account.address)}
                    />
                  )}
                />
              )
            }}
          />
        </View>
        <View style={styles.bootomBar}>
          <TextButton text={i18n.t(LANGUAGE_KEYS.IMPORT)} onPress={() => { this.onPressBottom() }} />
        </View>
      </SafeAreaView>
    )
  }

  @boundMethod
  private onPressNavigation(account: IBln) {
    accountStore.authenticate(account).then(() => {
      this.switchAccount(account);
      const resetAction = StackActions.replace(HomeRoutes.Main, { screen: ChatRoutes.Chat })
      this.props.navigation.dispatch(resetAction);
    })
  }

  @boundMethod
  private switchAccount(account: IBln) {
    accountStore.switchAccount(account);
    portfolioStore.initAccount();
  }

  @boundMethod
  private onPressBottom() {
    this.props.navigation.navigate(GlobalRoutes.Import)
  }

  @boundMethod
  private onPressCreate() {
    const ld = Loading.show()
    const mnemonic = blacknetjs.Mnemonic();
    const address = blacknetjs.Address(mnemonic);
    const bln = createIBln(mnemonic, address)
    accountStore.addAccount(bln)
    this.switchAccount(bln);
    setTimeout(() => {
      Loading.hide(ld)
    }, 100)
    setTimeout(() => {
      this.props.navigation.dispatch(StackActions.replace(HomeRoutes.Main, { screen: WalletRoutes.Wallet }));
    }, 150)
  }
}

const obStyles = observable({
  get styles() {
    return StyleSheet.create({
      noResult: {
        height: sizes.screen.height - 275,
        justifyContent: "center"
      },
      noResultTitle: {
        paddingLeft: 30,
        paddingRight: 30,
        textAlign: "center"
      },
      listView: {
        width: sizes.screen.width
      },
      bootomBar: {
          height: 50,
          flexDirection: "row",
          alignItems: "center",
          justifyContent: "center",
          borderTopColor: colors.border,
          borderTopWidth: 1,
          paddingLeft: 10,
          paddingRight: 10
      },
      bootomButton: {
        flex: undefined,
        height: 40,
        minWidth: 150,
        paddingLeft: 15,
        paddingRight: 15,
        borderRadius: 10
      },
      lists: {
        flex: 1,
        flexDirection: "column"
      },
      container: {
        flex: 1
      }
    })
  }
})
