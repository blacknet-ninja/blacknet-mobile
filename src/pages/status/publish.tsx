/**
 * publish
 * @file publish
 * @module pages/status/publish
 * @author Pony Ma <https://gitlab.com/blacknet-ninja>
 */

 import React, { Component } from 'react'
 import { StyleSheet, View, Text, ScrollView } from 'react-native'
 import { action, computed, observable } from 'mobx'
 import { observer } from 'mobx-react'
 import colors from '@app/style/colors'
 import { accountStore } from '@app/stores/account'
 import { LabelListTitle } from '@app/components/ui/list-title'
 import i18n from '@app/services/i18n'
 import { LANGUAGE_KEYS } from '@app/constants/language'
 import { showToast } from '@app/services/toast'
 import API from '@app/services/api'
 import Loading from '@app/components/common/loading'
 import { boundMethod } from 'autobind-decorator'
 import { SafeAreaView } from 'react-native-safe-area-context'
 import { IPageProps } from '@app/types/props'
 import { SendButton } from '@app/components/ui/button'
import { TextInput } from 'react-native-gesture-handler'
import BLNListItem from '@app/components/ui/BLNListItem'
 
 class FormStore {
     @observable content: string = ""
     
     @action.bound
     changeContent(content: string) {
         if(this.contentLen >= 300){
            return 
         }
         this.content = content
     }
 
     @computed
     get verify(): boolean{
         return this.content != undefined && this.content != ''
     }
 
     @computed
     get data(): any{
         return {
             content: this.content
         }
     }

     @computed
     get contentLen(): number{
         return this.content.length
     }
 
     @action.bound
     reset(){
         this.content = ""
     }
 }
 export const formStore = new FormStore()
 export interface IIndexProps extends IPageProps { }
 
 const obStyles = observable({
     get styles() {
       return StyleSheet.create({
         bootomBar: {
             height: 50,
             flexDirection: "row",
             alignItems: "center",
             justifyContent: "center",
             borderTopColor: colors.border,
             borderTopWidth: 1,
             paddingLeft: 10,
             paddingRight: 10
         },
         inputTitle: {
             padding: 4,
             color: colors.textDefault
         },
         inputContent: {
             height: 200,
             padding: 4,
             textAlignVertical: 'top',
             color: colors.textDefault
         },
         container: {
             flex: 1
         },
         content: {
             flex: 1,
             paddingLeft: 20,
             paddingRight: 20,
             paddingTop: 20
         },
          sendIcon: {
            transform: [{ rotate: '-90deg'}]
          }
       })
     }
 })
 
 @observer export class StatusPublishScreen extends Component<IIndexProps> {
 
     static getPageScreenOptions = ({ navigation }: any) => {
         return {
           title: i18n.t(LANGUAGE_KEYS.STATUS_MY_STATUS)
         }
     }
 
     componentDidMount(){
         formStore.reset()
     }
 
     @boundMethod
     private onPressSubmit(){
         if(!formStore.verify){
             return
         }
         const ld = Loading.show()
             API.addPost(
                 accountStore.currentMnemonic, 
                 accountStore.currentAddress, 
                 accountStore.currentAddress, 
                 formStore.data
             ).then((res:any)=>{
                 showToast(i18n.t(LANGUAGE_KEYS.FORUM_ADD_SUCCESS));
                 // goto detail or goto list
                 setTimeout(()=>{
                     this.props.navigation.goBack()
                 }, 500)
             })
             .catch((err)=>{
                 showToast(`${err}`)
                 console.log("Add Post failed.", err);
             })
             .finally(()=>{
                 Loading.hide(ld)
                 accountStore.refreshBalance()
             })
     }
 
     render() {
         const { styles } = obStyles
         return (
             <View style={styles.container}>
                 <ScrollView style={styles.content}>
                     <TextInput
                         style={styles.inputContent}
                         multiline={true}
                         autoFocus={true}
                         onChangeText={text => formStore.changeContent(text)}
                         value={formStore.content}
                         placeholder={i18n.t(LANGUAGE_KEYS.STATUS_PUBLISH_PLACEHOLDER)}
                         placeholderTextColor={colors.border}
                         numberOfLines={15}
                     />
                 </ScrollView>
                 <SafeAreaView>
                     <View style={styles.bootomBar}>
                         <BLNListItem 
                            trailing={<SendButton onPress={formStore.verify ? this.onPressSubmit : undefined}/>}
                            title={<Text style={{alignContent: "center", color: colors.border}}>{`${formStore.contentLen}/300`}</Text>}
                        />
                     </View>
                 </SafeAreaView>
             </View>
         )
     }
 }