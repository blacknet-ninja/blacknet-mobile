/**
 * Discover
 * @file Discover
 * @module pages/discover/index
 * @author Pony Ma <https://gitlab.com/blacknet-ninja>
 */

import FontAwesome5 from "react-native-vector-icons/FontAwesome5";
import { LANGUAGE_KEYS } from "@app/constants/language";
import { DiscoverRoutes, GlobalRoutes } from "@app/routes";
import i18n from "@app/services/i18n";
import colors from "@app/style/colors";
import { IPageProps } from "@app/types/props";
import { action, observable } from "mobx";
import { Observer, observer } from "mobx-react";
import React, { Component } from "react";
import { NativeSyntheticEvent, SafeAreaView, StyleSheet, Text, TextInputEndEditingEventData, View } from "react-native";
import { ScrollView } from "react-native-gesture-handler";

export interface IIndexProps extends IPageProps { }
import * as RootNavigation from "@app/rootNavigation";
import { Header } from '@react-navigation/stack'
import sizes from "@app/style/sizes";
import { BLNTextInput } from "@app/components/common/searchInput";
import { TextButton } from "@app/components/ui/button";
import { BLNDappButton } from "@app/components/ui/BLNListItem";
import { verifyAccount, verifyBlockNumber, verifyHash, verifyURL } from "@app/utils/bln";
import { boundMethod } from "autobind-decorator";

const obStyles = observable({
  get styles() {
    return StyleSheet.create({
      versionView: {
        position: "relative",
      },
      versionViewPoint: {
        width: 8,
        height: 8,
        borderRadius: 8,
        backgroundColor: colors.transferOut,
        position: "absolute",
        right: 0,
      },
      container: {
        flex: 1,
      },
      content: {
        borderBottomColor: colors.border,
        borderBottomWidth: 1,
        flex: 1,
      },
      list: {
      },
      balance: {
        height: 200,
        alignItems: "center",
        justifyContent: "center",
      },
      balanceText: {
        color: colors.yellow,
        fontSize: 30,
      },
      listTitleLeading: {
      },
      listTitleIcon: {
        color: colors.textDefault,
        fontSize: 16
      },
      listTitleText: {
        color: colors.textDefault,
        marginRight: 10,
      },
      listLanguage: {
        flexDirection: "row",
        alignItems: "center",
      },
      dapps: {
        margin: 15,
        borderWidth: 1,
        borderRadius: 6,
        borderColor: colors.border,
        paddingTop: 15,
        paddingBottom: 15
      },
      dappsContent: {
        flexDirection: "row",
        justifyContent: "space-evenly"
      }
    });
  },
});


class FormStore {
  @observable headerInputCancelVisible: boolean = false

  @action.bound
  changeHeaderInputCancelVisible(checked: boolean) {
      this.headerInputCancelVisible = checked
  }

  @observable headerInputText: string = ""

  @action.bound
  changeHeaderInputText(text: string) {
      this.headerInputText = text
  }

  @boundMethod
  onEndEditing(e: NativeSyntheticEvent<TextInputEndEditingEventData>){
    const text = this.headerInputText
    if (verifyAccount(text)) {
      return RootNavigation.navigate(DiscoverRoutes.ExplorerDetail, {
        type: "account",
        id: text
      })
    }
    if (verifyBlockNumber(text)) {
      return RootNavigation.navigate(DiscoverRoutes.ExplorerDetail, {
        type: "block",
        id: text
      })
    }
    if (verifyHash(text)) {
      return RootNavigation.navigate(DiscoverRoutes.ExplorerDetail, {
        type: "transaction",
        id: text
      })
    }
    if (verifyURL(text)) {
      return RootNavigation.navigate(GlobalRoutes.Browser, {
        uri: text
      })
    }
  }

}
export const formStore = new FormStore()


@observer
export class DiscoverScreen extends Component<IIndexProps> {


  static getPageScreenOptions = ({ navigation }: any) => {
    return {
      // title: i18n.t(LANGUAGE_KEYS.DISCOVER),
      title: null,
      headerStyle: {
          borderBottomWidth: 1,
          shadowOffset: {width: 0, height: 0},
          elevation: 0
      },
      header: (props: any) => {
          return <Observer render={()=>
            <View>
              <Header
                  {...props} 
              />
              <View style={{position: "absolute", top: sizes.safeAreaViewTop, right: 15, left: 15, flexDirection: "row", alignItems: 'center'}}>
                <BLNTextInput placeholder={i18n.t(LANGUAGE_KEYS.ENTER_RUL)} 
                  value={formStore.headerInputText}
                  onBlur={(v)=>{
                    formStore.changeHeaderInputCancelVisible(false)
                  }}
                  onFocus={(v)=>{
                    formStore.changeHeaderInputCancelVisible(true)
                  }}
                  onChangeText={formStore.changeHeaderInputText}
                  onEndEditing={formStore.onEndEditing}
                />
                {formStore.headerInputCancelVisible ? 
                  <TextButton text={i18n.t(LANGUAGE_KEYS.CANCEL)} style={{marginLeft: 10}} onPress={()=>{
                    formStore.changeHeaderInputText("")
                    formStore.changeHeaderInputCancelVisible(false)
                  }}/>
                : null}
              </View>
          </View>
        } />
      }
    }
  }

  render() {
    const { styles } = obStyles;
    return (
      <SafeAreaView style={styles.container}>
        <ScrollView>
          <View style={styles.dapps}>
            {this.renderDapps()}
          </View>
        </ScrollView>
      </SafeAreaView>
    );
  }

  renderDapps(){
    const { styles } = obStyles;
    return (
      <View style={styles.dappsContent}>
        <BLNDappButton IconName={"search"} IconComponent={FontAwesome5} text={"Explorer"} onPress={()=>{
            RootNavigation.navigate(DiscoverRoutes.Explorer, {});
        }}/>
        <BLNDappButton IconName={"bitcoin"} IconComponent={FontAwesome5} text={"CoinMarketCap"} onPress={()=>{
            RootNavigation.navigate(DiscoverRoutes.Coinmarketcap, {});
        }}/>
        <BLNDappButton IconName={"truck-monster"} IconComponent={FontAwesome5} text={"Pool"} onPress={()=>{
            RootNavigation.navigate(DiscoverRoutes.Poollist, {});
        }}/>
      </View>
    );
  }
}