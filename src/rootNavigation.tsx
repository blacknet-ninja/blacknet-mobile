// RootNavigation.js

import { NavigationContainerRef } from '@react-navigation/native';
import * as React from 'react';

export const navigationRef = React.createRef<NavigationContainerRef>();

export function navigate(name: any, params: any) {
  navigationRef.current?.navigate(name, params);
}

export function getCurrentRoute(){
  return navigationRef.current?.getCurrentRoute();
}
// add other navigation functions that you need and export them