
mobile_version=$(node -e "process.stdout.write((require('./package.json').version));")

cd android

./gradlew assembleRelease

scp -r app/build/outputs/apk/release/app-release.apk root@blnserver:~/blacknet-explorer/static/blacknet-mobile-$mobile_version.beta.apk


echo "https://blnscan.loqunbai.com/blacknet-mobile-${mobile_version}.beta.apk"