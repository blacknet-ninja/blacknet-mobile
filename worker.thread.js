import Blacknetjs from 'blacknetjs';
const blacknetjs = new Blacknetjs()
blacknetjs.jsonrpc.endpoint = 'https://blnapi.loqunbai.com';


import { self } from 'react-native-threads';

// listen for messages
self.onmessage = (message) => {
    try {
        const data = JSON.parse(message)
        switch (data.method) {
            case 'blacknetjs.decrypt':
                self.postMessage(Blacknetjs.Decrypt(data.sk, data.pk, data.text));
                break;
            default:
                self.postMessage("");
                break;
        }
    } catch (error) {
        console.log("thread worker", error)
        self.postMessage("");
    }
}